import { storiesOf } from '@storybook/react-native';
import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import {
  BarChart as C,
  PieChart as P,
  StackChart as S,
  colorList,
} from 'react-native-vulpes';

const buttonContainer = { margin: 10 };

const BarChart = (props) => {
  return <C style={buttonContainer} {...props} />;
};
const PieChart = (props) => {
  return <P style={buttonContainer} {...props} />;
};
const StackChart = (props) => {
  return <S style={buttonContainer} {...props} />;
};
class TemplateButton extends Component {
  render() {
    const dataB = {
      title: 'Titulo do gráfico de barras',
      data: [
        { label: 'Jan', value: 50 },
        { label: 'Fev', value: 80 },
        { label: 'Mar', value: 90 },
        { label: 'Abr', value: 70 },
      ],
    };
    const dataP = {
      title: 'Titulo do gráfico de pizza 2',
      helper: 'Mensagem de ajuda',
      data: [
        { label: 'Plano Básico I', value: 1 },
        { label: 'Plano Básico II', value: 2 },
        { label: 'Plano Básico III', value: 1 },
        { label: 'Plano Standard I', value: 2 },
        { label: 'Plano Standard II', value: 1 },
        { label: 'Plano Premium I', value: 9 },
        { label: 'Plano Premium II', value: 2 },
        { label: 'Plano Elite I', value: 13 },
        { label: 'Plano Elite II', value: 11 },
      ],
    };

    const dataS = {
      title: 'Titulo do gráfico Stack',
      helper: 'Mensagem de ajuda',
      data: [
        {
          label: 'Plano Básico I',
          stacks: [
            { value: 10 },
            { value: 30, color: '#4ABFF4', marginBottom: 2 },
          ],
        },
        {
          label: 'Plano Básico II',
          stacks: [
            { value: 10, color: 'orange' },
            { value: 40, color: '#4ABFF4', marginBottom: 2 },
          ],
        },
        {
          label: 'Plano Básico III',
          stacks: [
            { value: 10, color: 'orange' },
            { value: 50, color: '#4ABFF4', marginBottom: 2 },
          ],
        },
        {
          label: 'Plano Básico IV',
          stacks: [
            { value: 5, color: 'orange' },
            { value: 20, color: '#4ABFF4', marginBottom: 2 },
          ],
        },
      ],
    };

    const s = { flexDirection: 'row' };
    return (
      <View style={s}>
        <PieChart data={dataP} color={this.props.color} />
        <BarChart data={dataB} color={this.props.color} />
        <StackChart data={dataS} color={this.props.color} />
      </View>
    );
  }
}

const TemplateButtonWrap = (props) => <TemplateButton {...props} />;

export const Example = TemplateButtonWrap;

export default {
  title: 'Example/BarChart',
  component: BarChart,
  argTypes: {
    color: {
      description: 'color for the chart',
      control: {
        type: 'select',
        options: colorList(),
      },
    },
    data: {
      description: 'array of data to be shown',
      control: {
        type: 'array',
      },
    },
  },
};

Example.argTypes = {
  color: {
    description: 'color for the text',
    control: {
      type: 'select',
      options: colorList().filter((color) => !color.includes('gradient')),
    },
  },
  data: {
    description: 'array of data to be shown',
    control: {
      type: 'array',
    },
  },
};

Example.args = {
  color: undefined,
  data: undefined,
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('Button', TemplateButton, Example.args);
}
