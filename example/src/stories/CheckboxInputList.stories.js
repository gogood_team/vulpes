import { storiesOf } from '@storybook/react-native';
import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import { CheckboxInputList, H4 } from 'react-native-vulpes';

const mobileStyleWidget = { maxWidth: 320 };

export default {
  title: 'Example/CheckboxInputList',
  component: CheckboxInputList,
  argTypes: {
    values: {
      type: 'array',
    },
    onChange: {
      description: 'function that handle the state to be send to the component',
      control: 'fn',
    },
    title: {
      description: 'title of the multiple checkbox',
      control: 'text',
    },
  },
};

class TemplateCheckbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: this.props.values,
    };
  }

  onChange(newState) {
    this.setState({
      values: newState,
    });
  }

  render() {
    const { title } = this.props;
    return (
      <View>
        {title ? <H4>{title}</H4> : null}
        <CheckboxInputList
          title={this.props.title}
          values={this.state.values}
          onChange={this.onChange.bind(this)}
          {...this.props}
        />
      </View>
    );
  }
}

const TemplateCheck = (props) => <TemplateCheckbox {...props} />;
export const Example = TemplateCheck;
Example.args = {
  style: mobileStyleWidget,
  title: 'Atendimento',
  values: [
    { text: 'Presencial', checked: false },
    { text: 'Remoto', checked: false },
    { text: 'Hibrido', checked: false },
  ],
};

if (Platform.OS === 'android') {
  const paddingContainer = { padding: 10 };
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View style={paddingContainer}>
      <Story />
    </View>
  ));

  fillStories.add('CheckboxList', TemplateCheckbox, Example.args);
}
