import { storiesOf } from '@storybook/react-native';
import React, { useState } from 'react';
import { Platform, View } from 'react-native';
import { Form } from 'react-native-vulpes';

const form = [
  {
    title: 'Informações obrigatórias',
    hasRequired: true,
    description:
      'Para seguir com a contratação do plano, vamos precisar fazer seu cadastro pessoal.',
    innerDescription: 'Preencha os dados corretamente',
    questions: [
      {
        field: 'name',
        label: 'Nome completo',
        placeholder: 'Digite seu nome completo',
        required: true,
        type: 'fullname',
        invalid:
          'Informe o nome completo, como no seu documento de identificação',
      },
      {
        field: 'CPF',
        label: 'CPF',
        placeholder: 'Digite apenas os números',
        required: true,
        type: 'cpf',
      },
    ],
  },
  {
    title: 'Informações de contato',
    hasRequired: true,
    description: 'Insira os dados de contato.',
    questions: [
      {
        field: 'E-mail',
        label: 'E-mail pessoal',
        placeholder: 'Digite seu e-mail',
        required: true,
        type: 'email',
      },
      {
        field: 'Telefone',
        label: 'Telefone',
        placeholder: 'Apenas números, não esqueça o DDD',
        required: true,
        type: 'phone',
      },
      {
        field: 'date',
        label: 'Data',
        placeholder: 'Data de nascimento',
        required: true,
        type: 'date',
      },
    ],
  },
  {
    title: 'Informações da relação com a empresa',
    hasRequired: true,
    // description: 'Informações da empresa',
    innerDescription: 'Preencha os dados corretamente',
    questions: [
      // {
      //   'field': 'birthday',
      //   'label': 'Data de nascimento',
      //   'placeholder': 'Secione uma data',
      //   'required': true,
      //   'type': 'date'
      // },
      {
        field: 'biz_relation',
        label: 'Relação com a empresa',
        placeholder: 'Selecione uma opção',
        required: true,
        type: 'option',
        options: [
          {
            value: 'colaborador',
            label: 'Sou colaborador em regime CLT',
          },
          {
            value: 'outro',
            label: 'Outro',
          },
        ],
      },
      {
        field: 'biz_relation_other',
        label: 'Relação',
        placeholder: 'Digite qual a sua relação com a empresa',
        required: false,
        type: 'text',
        showIf: {
          field: 'biz_relation',
          value: ['outro'],
        },
      },
      {
        field: 'registration_id',
        label: 'Matrícula',
        placeholder: 'Digite sua matrícula',
        required: true,
        type: 'text',
      },
    ],
  },
  {
    title: 'Informações de endereço',
    hasRequired: true,
    description: 'Informações sobre endereço de contado do usuário',
    innerDescription: 'Preencha os dados corretamente',
    questions: [
      {
        field: 'CEP',
        label: 'CEP',
        placeholder: 'Digite apenas os números',
        required: true,
        type: 'cep',
      },
      {
        field: 'address_street',
        label: 'Rua',
        placeholder: 'Digite a sua rua',
        required: true,
        type: 'text',
      },
      {
        row: [
          {
            field: 'address_number',
            label: 'Número',
            placeholder: 'Digite o nº do seu endereço',
            required: true,
            type: 'text',
          },
          {
            field: 'address_complemento',
            label: 'Complemento',
            placeholder: 'Digite o complemento',
            required: false,
            type: 'text',
          },
        ],
      },

      {
        field: 'address_district',
        label: 'Bairro',
        placeholder: 'Digite o seu bairro',
        required: true,
        type: 'text',
      },
      {
        field: 'address_city',
        label: 'Cidade',
        placeholder: 'Digite a sua cidade',
        required: true,
        type: 'text',
      },
      {
        field: 'address_state',
        label: 'UF',
        placeholder: 'Escolha seu estado',
        required: true,
        type: 'option',
        options: [
          {
            value: 'RS',
            label: 'RS',
          },
          {
            value: 'SC',
            label: 'SC',
          },
        ],
      },
    ],
  },
];
const answers = {
  name: 'João da Silva',
  CPF: '99793857072',
  address_number: 999,
};

export default {
  title: 'Example/Form',
  component: Form,
  argTypes: {
    form: Object,
    answers: Object,
    onAnswer: Function,
    messages: Object,
  },
};

const TemplateForm = ({ ...rest }) => {
  const [answer, setAnswers] = useState(answers);

  function onAnswer(v, c, f) {
    // console.log('\n000000000000000000000');
    // console.log('aa', answer);
    console.log('Answer', v, c, f);
    // console.log('mmm', answers[f], v[f]);
    // // console.log("GGGGGG",setAnswers(answers))

    if (f) {
      console.log('VVVVVVVVVVVVVVVVV');
      console.log(v);
      setAnswers(v);
      // ...answers,
      // f: v[f]
    }
  }
  return <Form answers={answer} onAnswer={onAnswer} {...rest} />;
};

export const Example = TemplateForm;
Example.args = {
  form: form,
  messages: [
    { type: 'error', text: 'Hoje o dia está nublado' },
    { type: 'success', text: 'Mas amanhã vai ter sol' },
    'Depois, quem sabe?',
  ],

  // error: undefined,
  // label: 'Field label',
  // placeholder: 'Placeholder',
  // labelStyle: undefined,
  // inputStyle: undefined,
  // value: undefined,
  // onChangeText: undefined,
};

if (Platform.OS === 'android') {
  const paddingContainer = { padding: 10 };
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View style={paddingContainer}>
      <Story />
    </View>
  ));

  fillStories.add('FormSession', TemplateForm, Example.args);
}
