import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { HeaderSeparator } from 'react-native-vulpes';
const exampleImage = require('../images/transparentLogo.png');

const imageList = () => {
  return [
    'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg',
    exampleImage,
    'static/media/thumb.5ebfbd91.png',
    undefined,
  ];
};

const mobileStyleWidget = { maxWidth: 320 };
export default {
  title: 'Example/HeaderSeparator',
  component: HeaderSeparator,
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  argTypes: {
    source: {
      description: 'source for profile image',
      control: {
        type: 'select',
        options: imageList(),
      },
    },
  },
};

const TemplateGradientView = ({ ...rest }) => {
  return <HeaderSeparator {...rest} />;
};

export const Example = TemplateGradientView;
Example.argTypes = {
  source: {
    description: 'source for profile image',
    control: {
      type: 'select',
      options: imageList(),
    },
  },
};
Example.args = {
  color: 'primary.80',
  source: imageList()[0],
  cover: undefined,
  tagIcon: 'unlock',
  tagColor: 'dark_gray',
  tagText: 'Tag text',
  tagTextColor: undefined,
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('HeaderSeparator', TemplateGradientView, Example.args);
}
