import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { PartnersAgenda, PartnersCard } from 'react-native-vulpes';

const mobileStyleWidget = { maxWidth: 280, flex: 1 };
export default {
  title: 'Partners/PartnersAgenda',
  component: PartnersAgenda,
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  argTypes: {},
};

const Template = ({ dayMonth, dayName, timeArray, ...rest }) => {
  return (
    <PartnersCard {...rest}>
      <PartnersAgenda
        dayMonth={dayMonth}
        dayName={dayName}
        timeArray={timeArray}
      />
    </PartnersCard>
  );
};

// -- example 1-----
export const MiniCardAgenda = Template;
MiniCardAgenda.argTypes = {};

MiniCardAgenda.args = {
  dayMonth: '14/09',
  dayName: 'Quarta',
  timeArray: [
    {
      id: '123123',
      time: '11:00 - 12:00',
      description: 'description1',
      onPress: () => console.log('press'),
    },
    {
      id: '123123123',
      time: '12:00 - 12:30',
      description: 'description2dasdasdasdasdasdasdsadasdasd',
    },
    { id: '1231gsd23', time: '13:00 - 14:00', description: 'description3' },
  ],
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PartnersAgenda', Template, MiniCardAgenda.args);
}
