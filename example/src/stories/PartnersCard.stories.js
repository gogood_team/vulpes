import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { PartnersCard } from 'react-native-vulpes';
import { Regular, RegularBold } from '../../../src/components/typos';

const mobileStyleWidget = { maxWidth: 320 };
export default {
  title: 'Partners/PartnersCard',
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  component: PartnersCard,
  argTypes: {},
};

const TemplateGradientView = ({ ...rest }) => {
  return (
    <PartnersCard {...rest}>
      <Regular>Regular title</Regular>
      <RegularBold>Regular Bold Text</RegularBold>
    </PartnersCard>
  );
};

export const Example = TemplateGradientView;
Example.argTypes = {};
Example.args = {};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PartnersCard', TemplateGradientView, Example.args);
}
