import { storiesOf } from '@storybook/react-native';
import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import { PartnersLineChart, PartnersBarChart } from 'react-native-vulpes';

const mobileStyleWidget = {};
const styleSizeComplete = { width: 288, height: 146 };
class TemplateButton extends Component {
  render() {
    const dataL = [3410, 2387, 3751, 3751, 6479, 5115, 6138, 5797, 4092];
    const dataB = [
      { value: 2000, label: 'março' },
      { value: 3000, label: 'abril' },
      { value: 5000, label: 'maio' },
      { value: 4000, label: 'junho' },
      { value: 3000, label: 'julho' },
    ];
    const s = { flexDirection: 'row', display: 'flex' };
    return (
      <View style={s}>
        <View style={styleSizeComplete}>
          <PartnersLineChart
            data={dataL}
            description={'Valores a receber'}
            height={116}
            title={'PartnersLineChart'}
            helper={'helper'}
          />
        </View>
        <View style={styleSizeComplete}>
          <PartnersBarChart
            data={dataB}
            height={116}
            title={'PartnersBarChart'}
            helper={'helper'}
          />
        </View>
      </View>
    );
  }
}

const TemplateButtonWrap = (props) => <TemplateButton {...props} />;

export const Example = TemplateButtonWrap;

export default {
  title: 'Partners/PartnersCharts',
  component: PartnersLineChart,
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  argTypes: {
    data: {
      description: 'array of data to be shown',
      control: {
        type: 'array',
      },
    },
  },
};

Example.argTypes = {
  data: {
    description: 'array of data to be shown',
    control: {
      type: 'array',
    },
  },
};

Example.args = {
  data: undefined,
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PartnersCharts', TemplateButton, Example.args);
}
