import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { PartnersChatCard } from 'react-native-vulpes';

const imageList = () => {
  return [
    'static/media/thumb.5ebfbd91.png',
    'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg',
  ];
};

const mobileStyleWidget = { maxWidth: 320 };
export default {
  title: 'Partners/PartnersChatCard',
  component: PartnersChatCard,
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  argTypes: {
    source: {
      description: 'source for profile image',
      control: {
        type: 'select',
        options: imageList(),
      },
    },
  },
};

const Template = ({ source, titleName, datetime, lastMessage, ...rest }) => {
  return (
    <PartnersChatCard
      {...rest}
      source={source}
      titleName={titleName}
      datetime={datetime}
      lastMessage={lastMessage}
    />
  );
};

// -- example 1-----
export const ChatCard = Template;
ChatCard.argTypes = {
  source: {
    description: 'source for profile image',
    control: {
      type: 'select',
      options: imageList(),
    },
  },
};

ChatCard.args = {
  source:
    'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg',
  titleName: 'Titulo',
  datetime: '16/12/2022',
  lastMessage:
    'Regular Regular Regular Regular Regular Regular Regular Regular Regular ',
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PartnersChatCard', Template, ChatCard.args);
}
