import React, { useState } from 'react';
import { NotificationIcon, Menu, MenuItem, Text } from 'react-native-vulpes';

export default {
  title: 'Partners/PartnersMenu',
  component: MenuItem,
};

const TemplateMenuItem = ({
  color = undefined,
  outline = undefined,
  light = undefined,
  icon = 'like_empty',
  ...rest
}) => {
  const [current, setMenuItem] = useState('home');
  const defineMenuItem = (newLevel) => setMenuItem(() => newLevel);
  return (
    <Menu>
      <MenuItem
        selected={current === 'dashboard'}
        onPress={() => defineMenuItem('dashboard')}
      >
        <NotificationIcon name="dashboard" />
        <Text>Painel</Text>
      </MenuItem>
      <MenuItem
        selected={current === 'scan'}
        onPress={() => defineMenuItem('scan')}
        itemFocus={true}
      >
        <NotificationIcon name="scan" />
        <Text>QR Code</Text>
      </MenuItem>
      <MenuItem
        selected={current === 'profile'}
        onPress={() => defineMenuItem('profile')}
      >
        <NotificationIcon name="user" />
        <Text>Conta</Text>
      </MenuItem>
    </Menu>
  );
};

export const Example = TemplateMenuItem;
