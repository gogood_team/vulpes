import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import {
  PartnersAgenda,
  PartnersChatCard,
  PartnersStatsMiniCard,
  PartnersFixedNotificationCard,
  PartnersMiniGymCard,
  PartnersVisitorInfoCard,
  PartnersMiniReportCard,
  PartnersMiniRecentVisitorsCard,
  colorList,
} from 'react-native-vulpes';
import { Regular, RegularBold } from '../../../src/components/typos';
import { listOfIcons } from '../../../src/components/icon';
import { PartnersCard } from '../../../src/components/partnersCards';

const mobileStyleWidget = { maxWidth: 320 };

const imageList = () => {
  return [
    'static/media/thumb.5ebfbd91.png',
    'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg',
  ];
};

export default {
  title: 'Partners/PartnersMiniCards',
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  component: PartnersStatsMiniCard,
  argTypes: {
    tagColor: {
      description: 'color for the icon',
      type: 'select',
      options: colorList(),
    },
    color: {
      description: 'color for the icon',
      control: {
        type: 'select',
        options: colorList(),
      },
    },
    tagIcon: {
      description: 'icon to be used',
      control: {
        type: 'select',
        options: [null, ...listOfIcons()],
      },
    },
    tagIcon2: {
      description: 'icon to be used',
      control: {
        type: 'select',
        options: [null, ...listOfIcons()],
      },
    },
    source: {
      description: 'source for profile image',
      control: {
        type: 'select',
        options: imageList(),
      },
    },
  },
};

const TemplateGradientView = ({
  rest,
  data1,
  data2,
  color = 'primary.100',
  tagColor = 'primary.100',
  tagText = null,
  tagIcon = null,
  tagText2 = null,
  tagIcon2 = null,
  dayMonth,
  dayName,
  timeArray,
  datetime,
  titleName,
  source,
  visitorName,
  ...props
}) => {
  const componentsView = { gap: 20, maxHeight: 280, flexWrap: 'wrap' };
  return (
    <View style={componentsView}>
      <RegularBold>PartnersChatCard</RegularBold>
      <PartnersChatCard
        {...props}
        titleName={titleName}
        source={source}
        datetime={datetime}
        onPress={() => console.log('press')}
      />
      <View {...props}>
        <RegularBold>PartnersAgenda</RegularBold>
        <PartnersCard>
          <PartnersAgenda
            dayMonth={dayMonth}
            dayName={dayName}
            timeArray={timeArray}
          />
        </PartnersCard>
      </View>
      <RegularBold>PartnersStatsMiniCard</RegularBold>
      <PartnersStatsMiniCard
        {...props}
        data1={data1}
        data2={data2}
        color={color}
        onPress={() => console.log('press')}
      />
      <RegularBold>PartnersFixedNotificationCard</RegularBold>
      <PartnersFixedNotificationCard
        {...props}
        tagColor={tagColor}
        tagText={tagText}
        tagIcon={tagIcon}
        tagText2={tagText2}
        tagIcon2={tagIcon2}
        onPress={() => console.log('press')}
      >
        <RegularBold>Titulo Bold</RegularBold>
        <Regular>
          Regular Regular Regular Regular Regular Regular Regular
          RegularRegularRegularRegular
        </Regular>
      </PartnersFixedNotificationCard>
      <RegularBold>PartnersMiniGymCard</RegularBold>
      <PartnersMiniGymCard
        source={source}
        {...props}
        tagText={'tagText'}
        tagColor={'success.100'}
        tagTextColor={'gray.100'}
        onPress={() => console.log('press')}
      >
        <RegularBold>RegularBold children</RegularBold>
      </PartnersMiniGymCard>
      <RegularBold>PartnersVisitorInfoCard</RegularBold>
      <PartnersVisitorInfoCard
        source={source}
        value={20}
        titleName={titleName}
        datetime={datetime}
        tagText={tagText}
        tagText2={tagText2}
        onPress={() => console.log('press')}
        {...props}
      />
      <RegularBold>PartnersMiniReportCard</RegularBold>
      <PartnersMiniReportCard
        {...props}
        source={source}
        value={20}
        titleName={titleName}
        datetime={datetime}
        tagText={tagText}
        onPress={() => console.log('press')}
      />
      <RegularBold>PartnersMiniRecentVisitorsCard</RegularBold>
      <PartnersMiniRecentVisitorsCard
        {...props}
        onPress={() => console.log('press')}
        data={[
          { source, visitorName },
          { source, visitorName },
          { source, visitorName },
          { source, visitorName },
        ]}
      />
    </View>
  );
};

export const Example = TemplateGradientView;
Example.argTypes = {};
Example.args = {
  data1: { title: 'Visitas', value: 1523, percentage: -15 },
  data2: { title: 'Valor a receber', value: 23, percentage: -25 },
  source:
    'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg',
  tagText: 'TagText',
  tagIcon: null,
  tagText2: 'TagText2',
  tagIcon2: null,
  titleName: 'Title Name',
  value: 20,
  datetime: '30/03/22 - 12:30',
  dayMonth: '14/09',
  dayName: 'Quarta',
  timeArray: [
    {
      time: '11:00 - 12:00',
      description: 'description1',
      onPress: () => console.log('press'),
    },
    {
      time: '12:00 - 12:30',
      description: 'description2dasdasdasdasdasdasdsadasdasd',
    },
    { time: '13:00 - 14:00', description: 'description3' },
  ],
  visitorName: 'Visitor',
  lastMessage:
    'Regular Regular Regular Regular Regular Regular Regular Regular Regular ',
  props: { style: { height: 220 } },
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PartnersMiniCards', TemplateGradientView, Example.args);
}
