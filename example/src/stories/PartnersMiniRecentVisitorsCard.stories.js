import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { PartnersMiniRecentVisitorsCard } from 'react-native-vulpes';

const imageList = () => {
  return [
    undefined,
    'static/media/thumb.5ebfbd91.png',
    'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg',
  ];
};

// const source =
//   'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg';
// const visitorName = 'visitor';

const mobileStyleWidget = { maxWidth: 320 };
export default {
  title: 'Partners/PartnersMiniRecentVisitorsCard',
  component: PartnersMiniRecentVisitorsCard,
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  argTypes: {
    source: {
      description: 'source for profile image',
      control: {
        type: 'select',
        options: imageList(),
      },
    },
    visitorName: {
      description: 'Name to be displayed beneath the profile picture',
      control: {
        type: 'text',
      },
    },
  },
};

const Template = ({ source, visitorName, ...rest }) => {
  return (
    <PartnersMiniRecentVisitorsCard
      data={[
        { source, visitorName },
        { source, visitorName },
        { source, visitorName },
        { source, visitorName },
      ]}
      {...rest}
    />
  );
};

// -- example 1-----
export const MiniCard = Template;
MiniCard.argTypes = {};

MiniCard.args = {
  source:
    'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg',
  visitorName: 'Visitor',
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PartnersMiniGymCard', Template, MiniCard.args);
}
