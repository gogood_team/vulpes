import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { colorList, PartnersQrCodeCard } from 'react-native-vulpes';

const mobileStyleWidget = { maxWidth: 320 };

const imageList = () => {
  return [
    undefined,
    null,
    require('../../../assets/images/unchecked.png'),
    require('../../../assets/images/checked.png'),
    require('../../../assets/images/qr_code_partners.png'),
  ];
};

export default {
  title: 'Partners/PartnersQrCodeCard',
  component: PartnersQrCodeCard,
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  argTypes: {
    title: {
      description: 'Title',
      control: {
        type: 'text',
      },
    },
    color: {
      description: 'color for the card',
      control: {
        type: 'select',
        options: colorList().filter((color) => !color.includes('gradient')),
      },
    },
    onPress: {
      description: 'Function to be executed when the banner is pressed',
      control: {
        type: 'fn',
      },
    },
    source: {
      description: 'source for profile image',
      control: {
        type: 'select',
        options: imageList(),
      },
    },
  },
};

const Template = ({ ...rest }) => {
  return <PartnersQrCodeCard {...rest} />;
};

// example1
export const Example = Template;
Example.args = {
  color: 'primary.100',
  title: 'Escaneie o QR Code agora mesmo!',
  onPress: () => {
    console.log('teste');
  },
  source: require('../../../assets/images/qr_code_partners.png'),
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PartnersQrCodeCard', Template, Example.args);
}
