import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { PartnersStatsMiniCard, colorList } from 'react-native-vulpes';
import { listOfIcons } from '../../../src/components/icon';

const mobileStyleWidget = { maxWidth: 320 };

export default {
  title: 'Partners/PartnersStatsMiniCard',
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  component: PartnersStatsMiniCard,
  argTypes: {
    tagColor: {
      description: 'color for the icon',
      type: 'select',
      options: colorList(),
    },
    color: {
      description: 'color for the icon',
      control: {
        type: 'select',
        options: colorList(),
      },
    },
    tagIcon: {
      description: 'icon to be used',
      control: {
        type: 'select',
        options: [null, ...listOfIcons()],
      },
    },
    tagIcon2: {
      description: 'icon to be used',
      control: {
        type: 'select',
        options: [null, ...listOfIcons()],
      },
    },
  },
};

const TemplateGradientView = ({
  rest,
  data1,
  data2,
  color = 'primary.100',
}) => {
  return (
    <PartnersStatsMiniCard
      props={rest}
      data1={data1}
      data2={data2}
      color={color}
    />
  );
};

export const Example = TemplateGradientView;
Example.argTypes = {};
Example.args = {
  data1: { title: 'Visitas', value: 1523, percentage: -15 },
  data2: { title: 'Valor a receber', value: 23, percentage: -25 },
  tagText: 'TagText',
  tagIcon: null,
  tagText2: 'TagText2',
  tagIcon2: null,
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PartnersStatsMiniCard', TemplateGradientView, Example.args);
}
