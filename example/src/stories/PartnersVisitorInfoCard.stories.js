import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { colorList, PartnersVisitorInfoCard } from 'react-native-vulpes';
import { listOfIcons } from '../../../src/components/icon';

const imageList = () => {
  return [
    'static/media/thumb.5ebfbd91.png',
    'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg',
  ];
};

const mobileStyleWidget = { maxWidth: 320 };
export default {
  title: 'Partners/PartnersVisitorInfoCard',
  component: PartnersVisitorInfoCard,
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  argTypes: {
    tagColor: {
      description: 'color for the card',
      control: {
        type: 'select',
        options: colorList(),
      },
    },
    source: {
      description: 'source for profile image',
      control: {
        type: 'select',
        options: imageList(),
      },
    },
    tagIcon: {
      description: 'icon to be used',
      control: {
        type: 'select',
        options: [null, ...listOfIcons()],
      },
    },
    tagIcon2: {
      description: 'icon to be used',
      control: {
        type: 'select',
        options: [null, ...listOfIcons()],
      },
    },
  },
};

const Template = ({ ...rest }) => {
  return <PartnersVisitorInfoCard {...rest} />;
};

// -- example 1-----
export const MiniCardWithTag = Template;
MiniCardWithTag.argTypes = {
  source: {
    description: 'source for profile image',
    control: {
      type: 'select',
      options: imageList(),
    },
  },
};

MiniCardWithTag.args = {
  value: 20,
  datetime: '30/03/22 - 12:30',
  titleName: 'Nome do Visitante',
  source:
    'https://is2-ssl.mzstatic.com/image/thumb/Purple114/v4/26/44/72/2644724f-d58e-3097-9f6c-da427946c99e/source/60x60bb.jpg',
  tagText: 'hello',
  tagText2: 'hello2',
  tagColor: 'primary.100',
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PartnersMiniGymCard', Template, MiniCardWithTag.args);
}
