import { storiesOf } from '@storybook/react-native';
import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import { PinCode } from 'react-native-vulpes';
import { Regular } from '../../../src';

class Template extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: {
        value: null,
        complete: false,
      },
    };
  }

  onCodeChange(value, complete) {
    return this.setState({
      code: {
        complete,
        value,
      },
    });
  }

  render() {
    const s1 = { width: 350 };
    const s2 = { marginTop: 16 };
    return (
      <View style={s1}>
        <PinCode onChange={this.onCodeChange.bind(this)} />
        <Regular style={s2}>{`value: ${this.state.code.value}`}</Regular>
        <Regular style={s2}>{`complete: ${this.state.code.complete}`}</Regular>
      </View>
    );
  }
}

const TemplateWrap = (props) => <Template {...props} />;

export const Example = TemplateWrap;

export default {
  title: 'Example/PinCode',
  component: PinCode,
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('PinCode', Template, Example.args);
}
