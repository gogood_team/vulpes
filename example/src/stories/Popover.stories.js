import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { Popover, Regular } from 'react-native-vulpes';

export default {
  title: 'Example/Popover',
  component: Popover,
  // decorators: [(story) => <View>{story()}</View>],
  argTypes: {
    canShow: {
      control: 'boolean',
    },
  },
};

const Template = ({ ...rest }) => {
  const containerStyle = { flexDirection: 'row' };
  const style = { margin: 10, zIndex: -1 };
  return (
    <View style={containerStyle}>
      <View style={style}>
        <Popover {...rest} label={'Clique aqui'}>
          <Regular>Foo</Regular>
          <Regular>Bar</Regular>
        </Popover>
      </View>
      <View style={style}>
        <Popover {...rest} label={'Clique aqui'}>
          <Regular>Foo</Regular>
          <Regular>Bar</Regular>
        </Popover>
      </View>
      <View style={style}>
        <Popover {...rest} label={'Clique aqui'}>
          <Regular>Foo</Regular>
          <Regular>Bar</Regular>
        </Popover>
      </View>
      <View style={style}>
        <Popover {...rest} label={'Clique aqui'}>
          <Regular>Foo</Regular>
          <Regular>Bar</Regular>
        </Popover>
      </View>
    </View>
  );
};

export const Example = Template;
Example.args = {
  canShow: true,
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('Popover', Template, Example.args);
}
