import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { SearchHistoryItems } from 'react-native-vulpes';

const mobileStyleWidget = { maxWidth: 320 };

export default {
  title: 'Example/SearchHistoryItems',
  component: SearchHistoryItems,
  argTypes: {
    title: {
      type: 'string',
    },
    searchItems: {
      type: 'array',
    },
  },
};

const TemplateSearchInput = ({ ...rest }) => <SearchHistoryItems {...rest} />;

export const Example = TemplateSearchInput.bind({});
Example.args = {
  searchItems: ['items1', 'items2', 'items3', 'items4', 'items5'],
  title: 'Buscas recentes',
  cleanHistoryFunction: () => {
    console.log('eraseHistoryFunction called to erase search history');
  },
  style: mobileStyleWidget,
  onPress: (item) => {
    console.log('onPress ', item);
  },
};

if (Platform.OS === 'android') {
  const paddingContainer = { padding: 10 };
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View style={paddingContainer}>
      <Story />
    </View>
  ));

  fillStories.add('SearchHistoryItems', TemplateSearchInput, Example.args);
}
