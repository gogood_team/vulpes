import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { SearchInput } from 'react-native-vulpes';
import { listOfIcons } from '../../../src/components/icon';

export default {
  title: 'Example/SearchInput',
  component: SearchInput,
  argTypes: {
    error: {
      description:
        'error message to be shown for the input, Null or empty if there is no error',
      control: 'text',
    },
    placeholder: {
      description: 'placeholder text shown while no input is given',
      control: 'text',
    },
    inputStyle: {
      description: 'style for the input field',
      control: 'object',
    },
    allowSend: {
      description: 'if it is allowed to send the query',
      control: 'boolean',
    },
    onChangeText: {
      description:
        'function that will be called when the field value is changed. It passes the resulting text as a parameter.',
      control: 'fn',
    },
    iconName: {
      description: 'icon to be used',
      options: listOfIcons(),
      control: {
        type: 'select',
      },
    },
    disableIcon: {
      description: 'disable touchOpacity on the icon',
      control: 'boolean',
    },
  },
};

const TemplateSearchInput = ({ ...rest }) => (
  <SearchInput label={'Field label'} placeholder={'Placeholder'} {...rest} />
);

export const Example = TemplateSearchInput.bind({});
export const LocationSearch = TemplateSearchInput.bind({});
Example.args = {
  error: undefined,
  placeholder: 'Pesquisar um serviço',
  inputStyle: undefined,
  onChangeText: undefined,
  allowSend: true,
  disableIcon: true,
  onFinish: () => console.log('onFinish '),
};
LocationSearch.args = {
  ...Example.args,
  placeholder: 'Bairro, cidade, estado ou CEP',
  iconName: 'checkin',
  disableIcon: false,
};

if (Platform.OS === 'android') {
  const paddingContainer = { padding: 10 };
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View style={paddingContainer}>
      <Story />
    </View>
  ));

  fillStories.add('SearchInput', TemplateSearchInput, Example.args);
}
