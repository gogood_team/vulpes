import { storiesOf } from '@storybook/react-native';
import React, { Component } from 'react';
import { Platform, View } from 'react-native';
// import Slider from 'react-native-slider';

import { Slider } from 'react-native-vulpes';
import { Regular } from '../../../src';

export default {
  title: 'Example/Slider',
  component: Slider,
  argTypes: {
    minimumValue: {
      control: {
        type: 'number',
      },
      description: 'Minimum range value',
    },
    maximumValue: {
      control: {
        type: 'number',
      },
      description: 'Maximum range value',
    },
    step: {
      control: {
        type: 'number',
      },
      description: 'Slider step value',
    },
  },
};

class TemplateSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    };
  }

  render() {
    return (
      <View>
        <Slider
          {...this.props}
          value={this.state.value}
          onValueChange={(value) => this.setState({ value })}
        />
        <Regular>{this.state.value}</Regular>
      </View>
    );
  }
}

const TemplateCheck = (props) => <TemplateSlider {...props} />;
export const Example = TemplateCheck;
Example.args = {
  minimumValue: 0,
  maximumValue: 100,
  step: 1,
};

if (Platform.OS === 'android') {
  const paddingContainer = { padding: 10 };
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View style={paddingContainer}>
      <Story />
    </View>
  ));

  fillStories.add('Slider', TemplateSlider, Example.args);
}
