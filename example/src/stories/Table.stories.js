import { storiesOf } from '@storybook/react-native';
import React from 'react';
import { Platform, View } from 'react-native';
import { Colors, Table } from 'react-native-vulpes';
import { Regular } from '../../../src/components/typos';

const colorList = () => {
  var keys = [undefined];
  for (var k in Colors) {
    if (k.substring(0, 8) !== 'gradient') {
      keys.push(k);
    }
  }
  return keys;
};

export default {
  title: 'Example/Table',
  component: Table,
  argTypes: {
    color: {
      description: 'color for the button',
      control: {
        type: 'select',
        options: colorList(),
      },
    },
  },
};

const serverData = [
  { username: 'Fernando', email: 'fernando@email.com.br', status: 'ativo' },
  { username: 'maria', email: 'maria@@email.com.br', status: 'inativo' },
  { username: 'João', email: 'joao@@email.com.br', status: 'inativo' },
  { username: 'Carlos', email: 'carlos@@email.com.br', status: 'ativo' },
];

const tableJsonData = serverData.map((data) => ({
  user: {
    type: 'composite',
    value: { title: data.username, description: data.status },
  },
  email: {
    type: 'composite',
    value: { title: 'E-mail', description: data.email },
    responsiveParent: true,
  },
  title: {
    type: 'composite_popover',
    value: {
      title: 'Popover content',
      description: 'Open popover',
      content: (
        <Regular color={'singleton.black'}>
          Popover do(a) {data.username} {'\n'} Lorem ipsum {'\n\n\n'} Dolor sit
          amet
        </Regular>
      ),
    },
  },
  sla: { type: 'text', value: 'ha' },
}));

const tableArrayData = [
  ['Borba', 'borba@gg.com', 6],
  ['Fernando', 'fefo@gg.com', 5],
  ['David', 'costa@gg.com', 1],
  ['Águilis', 'águilis@gg.com', 1],
  ['Marcelo', 'marcelo@gg.com', 1],
  ['Bruno', 'bruno@gg.com', 1],
];

const TemplateList = ({ ...rest }) => {
  return (
    <>
      <Table
        header={['Nome', 'E-mail', 'Popover', 'sla']}
        {...rest}
        title={'Tabela com dados JSON'}
        data={tableJsonData}
      />
      <Table
        header={['Nome', 'E-mail', 'Experiência']}
        {...rest}
        title={'Tabela com dados De array e tema definido pelo usuário'}
        data={tableArrayData}
        headerColor={'gray.100'}
        evenColor={'gray.40'}
        oddColor={'gray.20'}
        mobileEvenColor={'gray.40'}
        mobileOddColor={'gray.20'}
        childColor={'gray.3'}
      />
      <Table
        header={['Foo']}
        {...rest}
        emptyState={<Regular>...Nada por aqui</Regular>}
        title={'Tabela sem dados'}
      />
    </>
  );
};

export const Example = TemplateList;

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('Table', TemplateList, Example.args);
}
