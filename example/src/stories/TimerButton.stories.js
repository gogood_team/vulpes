import { storiesOf } from '@storybook/react-native';
import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import { TimerButton } from 'react-native-vulpes';

class TemplateButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: false,
    };
  }

  handlePress() {
    this.setState({
      value: !this.state.value,
    });
  }

  render() {
    const s1 = { width: '100%', justifyContent: 'center', marginBottom: 16 };
    return (
      <>
        <TimerButton
          timeInSeconds={5}
          text={'Reenviar código '}
          style={s1}
          ghost
          onPress={null}
        />
      </>
    );
  }
}

const TemplateButtonWrap = (props) => <TemplateButton {...props} />;

export const Example = TemplateButtonWrap;

export default {
  title: 'Example/TimerButton',
  component: TimerButton,
};

if (Platform.OS === 'android') {
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View>
      <Story />
    </View>
  ));

  fillStories.add('Button', TemplateButton, Example.args);
}
