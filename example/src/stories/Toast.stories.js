import React from 'react';
import { View } from 'react-native';
import { Button, Toast } from 'react-native-vulpes';

const mobileStyleWidget = { maxWidth: 320 };
export default {
  title: 'Example/Toast',
  component: Toast,
  decorators: [(story) => <View style={mobileStyleWidget}>{story()}</View>],
  argTypes: {
    type: {
      description: 'color for the button',
      control: {
        type: 'select',
        options: ['success', 'warning', 'error'],
      },
    },
  },
};

const TemplateGradientView = ({ ...rest }) => {
  const style = { height: '100vh' };
  return (
    <View style={style}>
      <Toast />
      <Button
        text="Mostrar toast"
        onPress={() => {
          Toast.show({
            title: 'Hello',
            text: 'world...',
            duration: 2000,
            position: 16,
            ...rest,
          });
        }}
      />
    </View>
  );
};

export const Example = TemplateGradientView;
Example.args = {
  type: 'success',
};
