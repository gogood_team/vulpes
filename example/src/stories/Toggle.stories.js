import { storiesOf } from '@storybook/react-native';
import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import { Toggle } from 'react-native-vulpes';

export default {
  title: 'Example/Toggle',
  component: Toggle,
  argTypes: {
    state: {
      description: 'Toggle active state',
      control: 'boolean',
    },
  },
};

class TemplateToggle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
  }

  onChange(active) {
    this.setState({
      active: active,
    });
  }

  render() {
    return (
      <View>
        <Toggle
          onChange={this.onChange.bind(this)}
          active={this.state.active}
        />
      </View>
    );
  }
}

const TemplateCheck = (props) => <TemplateToggle {...props} />;
export const Example = TemplateCheck;
Example.args = {
  state: false,
};

if (Platform.OS === 'android') {
  const paddingContainer = { padding: 10 };
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View style={paddingContainer}>
      <Story />
    </View>
  ));

  fillStories.add('Toggle', TemplateToggle, Example.args);
}
