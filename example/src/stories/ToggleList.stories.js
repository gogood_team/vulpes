import { storiesOf } from '@storybook/react-native';
import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import { ToggleList, H4 } from 'react-native-vulpes';

const mobileStyleWidget = { maxWidth: 320 };

export default {
  title: 'Example/ToggleList',
  component: ToggleList,
  argTypes: {
    values: {
      type: 'array',
    },
    onChange: {
      description: 'function that handle the state to be send to the component',
      control: 'fn',
    },
    // title: {
    //   description: 'title of the multiple checkbox',
    //   control: 'text',
    // },
    floatRight: {
      description: 'box to be shown on the left or right side',
      type: 'boolean',
      control: 'boolean',
    },
  },
};

class TemplateToggle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: this.props.values,
    };
  }

  onChange(newState) {
    this.setState({
      values: newState,
    });
  }

  render() {
    const { title } = this.props;
    return (
      <View>
        {title ? <H4>{title}</H4> : null}
        <ToggleList
          title={this.props.title}
          values={this.state.values}
          onChange={this.onChange.bind(this)}
          floatRight={this.props.floatRight}
          {...this.props}
        />
      </View>
    );
  }
}

const TemplateCheck = (props) => <TemplateToggle {...props} />;
export const Example = TemplateCheck;
Example.args = {
  style: mobileStyleWidget,
  title: 'Atendimento',
  values: [
    { text: 'Favoritos', active: false },
    { text: 'Aberto agora', active: false },
    { text: 'Últimos checkins', active: false },
  ],
};

if (Platform.OS === 'android') {
  const paddingContainer = { padding: 10 };
  const fillStories = storiesOf('Color', module).addDecorator((Story) => (
    <View style={paddingContainer}>
      <Story />
    </View>
  ));

  fillStories.add('ToggleList', TemplateToggle, Example.args);
}
