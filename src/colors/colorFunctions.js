import { getSafe } from '../utils/errorHandler';
import semanticTokens from './tokens.json';
import mappedColors from './colors.json';

const DEFAULT_THEME = 'gogood';

export const colorList = (theme) => {
  const colors = mappedColors[theme ?? DEFAULT_THEME];
  return Object.keys(colors).flatMap((family) =>
    Object.keys(colors[family]).map((grade) => `${family}.${grade}`)
  );
};

export const getColors = (theme = DEFAULT_THEME) => {
  return (token) => {
    const colors = mappedColors[theme ?? DEFAULT_THEME];
    const defaultColor = colors.singleton.default;

    if (!token) return defaultColor;
    const [family, grade] = token.split('.');
    if (!family || !grade) return defaultColor;

    try {
      const color = colors[family][grade];
      return color;
    } catch {
      const semanticToken = getSafe(
        () => semanticTokens[theme ?? DEFAULT_THEME][family][grade],
        'primary.100'
      );
      const [mappedFamily, mappedGrade] = semanticToken.split('.');
      const color = getSafe(() => colors[mappedFamily][mappedGrade], '#0067e1');
      return color || defaultColor;
    }
  };
};
