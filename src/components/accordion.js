import React, { useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { BodyLargeBold } from '..';
import { getColors } from '../colors';
import useVulpes from '../hooks/useVulpes';
import { titleStyle } from '../styles/list';
import { HTMLRender } from './html_render';
import { Icon } from './icon';
import { RegularBold } from './typos';

const Title = (props) => {
  if (!props.title) return null;
  return (
    <BodyLargeBold style={titleStyle} color={'primary.80'}>
      {props.title}
    </BodyLargeBold>
  );
};

export const Accordion = (props) => {
  const cItens = React.Children.toArray(props.children).filter(
    (c) => c.props.visible !== false
  );

  const itemCount = cItens.length;
  const accStyle = { flex: 1, marginTop: 8, marginBottom: 16, ...props.style };
  return (
    <View style={accStyle}>
      <Title title={props.title} />
      {cItens.map((child, i) => {
        const last = i === itemCount - 1;
        return React.cloneElement(child, {
          last: last,
        });
      })}
    </View>
  );
};

const NavIcon = ({ showing }) => {
  const navIconCont = {
    justifyContent: 'center',
    textAlign: 'center',
    verticalAlign: 'center',
    paddingRight: 5,
    paddingLeft: 5,
  };
  return (
    <View style={navIconCont}>
      <Icon size={12} name={showing ? 'chevron_down' : 'chevron_right'} />
    </View>
  );
};

const Children = ({ children }) => {
  let item =
    typeof children === 'string' ? (
      <RegularBold>{children}</RegularBold>
    ) : (
      children
    );
  const style = { flex: 1, minHeight: 24, justifyContent: 'center' };
  return <View style={style}>{item}</View>;
};

function itemStyle(props, theme) {
  const colors = getColors(theme);
  let style = {
    paddingTop: 16,
    paddingBottom: 16,
    paddingLeft: 0,
    paddingRight: 0,
    borderBottomColor: colors('gray.40'),
    borderBottomWidth: 1,
    flexDirection: 'column',
    ...props.style,
  };
  if (props.last) style.borderBottomWidth = 0;

  return style;
}

export const AccordionItem = (props) => {
  const [showing, setShowing] = useState(props.showing || false);
  const toggleShowing = () => setShowing(!showing);
  const touchStyle = { flexDirection: 'row', alignItems: 'center' };
  const { theme } = useVulpes();
  return (
    <View style={itemStyle(props, theme)}>
      <TouchableOpacity onPress={toggleShowing} style={touchStyle}>
        <Children children={props.children} />
        <NavIcon showing={showing} />
      </TouchableOpacity>
      {showing ? (
        <View>
          {props.long && <HTMLRender html={props.long} onLink={props.onLink} />}
          {props.content}
        </View>
      ) : null}
    </View>
  );
};
