import React, { Component } from 'react';
import { Keyboard, TouchableOpacity, View } from 'react-native';
import { getColors } from '../colors';
import { RegularBold } from '../components/typos';
import VulpesContext from '../contexts/VulpesContext';
import { getFonts } from '../fonts';
import styles from '../styles/buttons';
import { Icon } from './icon';

class Button extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pressed: false,
    };
  }

  renderField() {
    const { children, text, leftIcon, rightIcon, iconSize } = this.props;
    if (children) return this.renderChildren();

    const tx = text === undefined ? 'Enviar' : text;

    const t = tx && (
      <RegularBold numberOfLines={1} key={'t'} color={this.textColor()}>
        {tx}
      </RegularBold>
    );

    const l = leftIcon && (
      <Icon
        key={'l'}
        name={leftIcon}
        color={this.textColor()}
        size={iconSize}
      />
    );

    const r = rightIcon && (
      <Icon
        key={'r'}
        name={rightIcon}
        color={this.textColor()}
        size={iconSize}
      />
    );

    const els = [l, t, r].filter((i) => i);

    return els.map((el, i) => {
      if (!React.isValidElement(el)) return;
      if (i === 0) return el;
      return React.cloneElement(el, {
        style: {
          paddingLeft: 8,
          ...el.props.style,
        },
      });
    });
  }

  renderChildren() {
    const { theme } = this.context;
    const style = styles(theme);
    const fonts = getFonts(theme);

    const list = React.Children.toArray(this.props.children).filter((c) => !!c);
    const items = list.map((child, i) => {
      if (React.isValidElement(child)) {
        return React.cloneElement(child, {
          color: this.textColor(),
          fontStyle: fonts.regularBold,
          style: {
            ...this.textStyle({
              isFirst: i === 0,
              isIcon: child.type.displayName === 'Icon',
            }),
            ...child.props.style,
          },
          numberOfLines: 1,
        });
      }
      return child;
    });

    if (!this.props.block) return items;

    return (
      <View style={style.blockOuter}>
        <View style={style.blockInner}>{items}</View>
      </View>
    );
  }

  primaryColor() {
    const { color, disabled } = this.props;
    if (color) return color; // se não tem color não mantemos a opacidade lá no componente
    if (disabled) return 'btn.disabled';
    return this.state.pressed ? 'btn.pressed' : 'btn.regular';
  }

  textColor() {
    const { outline, ghost, textColor } = this.props;
    if (textColor) return textColor;
    if (outline || ghost) return this.primaryColor();
    return 'singleton.white';
  }

  textStyle({ isFirst, isIcon }) {
    const ret = {
      ...this.props.textStyle,
      marginLeft: isFirst ? undefined : 8,
    };
    if (isIcon) {
      ret.marginTop = 'auto';
      ret.marginBottom = 'auto';
    }
    return ret;
  }

  outlineGhostColor() {
    const { color } = this.props;
    const { theme } = this.context;
    if (color) return null;
    const colors = getColors(theme);
    return this.state.pressed ? colors('btn.pressed_ghost') : null; // gogood only?
  }

  backgroundColor() {
    const { outline, ghost } = this.props;
    const { theme } = this.context;
    const colors = getColors(theme);
    if (outline || ghost) {
      return this.outlineGhostColor();
    }
    return colors(this.primaryColor());
  }

  borderColor() {
    const { ghost } = this.props;
    const { theme } = this.context;
    const colors = getColors(theme);
    if (ghost) return 'transparent';
    return colors(this.primaryColor());
  }

  completeStyle() {
    const { theme } = this.context;
    const style = styles(theme);
    const { ghost, outline } = this.props;
    if (outline) return style.outlineStyle;
    if (ghost) return style.ghostStyle;
    return style.defaultStyle;
  }

  buttonStyle() {
    return {
      ...this.completeStyle(),
      ...(this.props.block ? { width: '100%' } : {}),
      ...this.props.style,
      backgroundColor: this.backgroundColor(),
      borderColor: this.borderColor(),
    };
  }

  onPress() {
    const { onPress } = this.props;
    if (!onPress) return;
    Keyboard.dismiss();
    onPress();
  }

  onPressIn() {
    return this.setState({ pressed: true });
  }

  onPressOut() {
    return this.setState({ pressed: false });
  }

  render() {
    const { disabled, color } = this.props;
    return (
      <TouchableOpacity
        onPress={this.onPress.bind(this)}
        onPressIn={this.onPressIn.bind(this)}
        onPressOut={this.onPressOut.bind(this)}
        style={this.buttonStyle()}
        disabled={disabled}
        activeOpacity={color ? 0.2 : 1}
      >
        {this.renderField()}
      </TouchableOpacity>
    );
  }
}
Button.contextType = VulpesContext;

class ToggleButton extends Component {
  handleClick() {
    return this.props.onPress && this.props.onPress();
  }

  color() {
    const { color } = this.props;
    if (!color) return 'gray.100';
    return color;
  }
  size() {
    return this.props.size || 18;
  }

  render() {
    const { value, onIcon, offIcon, style } = this.props;
    return (
      <TouchableOpacity onPress={this.handleClick.bind(this)} style={style}>
        {value ? (
          <Icon name={onIcon} size={this.size()} color={this.color()} />
        ) : (
          <Icon name={offIcon} size={this.size()} color={this.color()} />
        )}
      </TouchableOpacity>
    );
  }
}

export { Button, ToggleButton };
