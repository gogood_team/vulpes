import React, { Component } from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { getColors } from '../colors';
import VulpesContext from '../contexts/VulpesContext';
import useVulpes from '../hooks/useVulpes';
import getStyle from '../styles/card';
import { Button } from './button';
import { Icon } from './icon';
import { Text } from './text';
import { Thumbnail } from './thumbnail';
import { Regular, RegularBold } from './typos';
import {
  TicketCardSeparator,
  TicketCheckinCardSeparator,
  TicketProfileCardSeparator,
  ProfileCardSeparator,
  CardCover,
  CardTag,
  IllustrationOnCard,
} from '../utils/cardElements';
import { GradientView } from './gradient_view';

export class Card extends Component {
  changedColor() {
    const { color } = this.props;
    const data = {};
    if (color) {
      const { theme } = this.context;
      const colors = getColors(theme);
      data.backgroundColor = colors(color);
    }
    return data;
  }

  render() {
    const { theme } = this.context;
    const style = getStyle(theme);
    const zeroPadding = this.props.noPadding
      ? style.cardContainerZeroPadding
      : {};

    let cardContainer = {
      ...getStyle(theme).cardContainer,
      ...zeroPadding,
    };

    cardContainer = { ...cardContainer, ...this.props.cardContainer };

    const MainComponent = this.props.onPress ? TouchableOpacity : View;

    return (
      <MainComponent onPress={this.props.onPress} style={this.props.style}>
        <View style={cardContainer}>{this.props.children}</View>
        <View style={getStyle(theme).outerCardBorder}>
          <View
            style={{
              ...(!this.props.flat ? getStyle(theme).cardTopBorder : {}),
              ...this.changedColor(),
            }}
          />
        </View>
      </MainComponent>
    );
  }
}
Card.contextType = VulpesContext;

export class TicketCard extends Component {
  render() {
    return (
      <Card cardContainer={{ overflow: null }} {...this.props}>
        <TicketCardSeparator />
        {this.props.children}
      </Card>
    );
  }
}

export class TicketProfileCard extends Component {
  changedColor() {
    const { color } = this.props;
    const data = {};
    if (color) {
      const { theme } = this.context;
      const colors = getColors(theme);
      data.backgroundColor = colors(color);
    }
    return data;
  }
  render() {
    return (
      <Card {...this.props} cardContainer={{ overflow: null }}>
        <TicketProfileCardSeparator source={this.props.source} />
        {this.props.children}
      </Card>
    );
  }
}
TicketProfileCard.contextType = VulpesContext;

export class TicketCheckinCard extends Component {
  changedColor() {
    const { color } = this.props;
    const data = {};
    if (color) {
      const { theme } = this.context;
      const colors = getColors(theme);
      data.backgroundColor = colors(color);
    }
    return data;
  }
  render() {
    return (
      <Card {...this.props} cardContainer={{ overflow: null }}>
        <TicketCheckinCardSeparator checked={this.props.checked} />
        {this.props.children}
      </Card>
    );
  }
}
TicketCheckinCard.contextType = VulpesContext;

export class ProfileCard extends Component {
  render() {
    const { cover, color, source, children, ...rest } = this.props;

    return (
      <Card {...this.props} color={cover ? 'singleton.transparent' : color}>
        <CardCover source={cover} {...rest} />
        <View>
          <ProfileCardSeparator source={source} />
          {children}
        </View>
      </Card>
    );
  }
}
export class MiniProfileCard extends Component {
  changedColor() {
    const { color } = this.props;
    const data = {};
    if (color) {
      const { theme } = this.context;
      const colors = getColors(theme);
      data.backgroundColor = colors(color);
    }
    return data;
  }
  render() {
    const { tagTextColor, tagColor, tagText, tagIcon } = this.props;
    const { theme } = this.context;
    const style = getStyle(theme);
    return (
      <Card cardContainer={style.miniCardContainer} {...this.props}>
        {(tagText || tagIcon) && (
          <CardTag
            textColor={tagTextColor}
            color={tagColor}
            text={tagText}
            icon={tagIcon}
            marginBottom={true}
          />
        )}
        <View style={style.outerMiniCardStyle}>
          <Thumbnail source={this.props.source} size={'small'} />
          <View style={style.miniCardContentStyle}>{this.props.children}</View>
        </View>
      </Card>
    );
  }
}
MiniProfileCard.contextType = VulpesContext;

export class IllustrationMiniCard extends Component {
  render() {
    const { theme } = this.context;
    const style = getStyle(theme);
    return (
      <Card
        cardContainer={style.illustrationCardContainer}
        color={'singleton.transparent'}
        {...this.props}
      >
        <View style={style.outerMiniCardStyle}>
          <IllustrationOnCard source={this.props.source} />
          <View style={style.illustrationCardOuterStyle}>
            {this.props.children}
          </View>
        </View>
      </Card>
    );
  }
}
IllustrationMiniCard.contextType = VulpesContext;

export class UploadCard extends Component {
  handlePress() {
    const { onClose } = this.props;
    onClose && onClose();
  }

  render() {
    const { theme } = this.context;
    const { filename } = this.props;
    const style = getStyle(theme);
    return (
      <Card
        cardContainer={style.uploadCardContainer}
        color={'singleton.transparent'}
        {...this.props}
      >
        <View style={style.uploadCardIconContainer}>
          <Icon
            style={style.uploadCardIcon}
            name={'file'}
            size={24}
            color={'singleton.white'}
          />
        </View>
        <View style={style.uploadCardContentContainer}>
          <View style={style.uploadCardLabelContainer}>
            <Regular numberOfLines={1}>{filename}</Regular>
          </View>
          <Button
            color={'gray.100'}
            onPress={this.handlePress.bind(this)}
            ghost
          >
            <Icon size={16} name={'close'} />
          </Button>
        </View>
      </Card>
    );
  }
}
UploadCard.contextType = VulpesContext;

function computeSourceSize(source, cardHeight) {
  let width = 90;
  if (source?.uri) return width;

  if (Image.resolveAssetSource) {
    const { width: w, height: h } = Image.resolveAssetSource(source);
    const ratio = w / h;
    width = Math.round(cardHeight * ratio);
  }

  return width;
}
export const BannerCard = ({
  title,
  description,
  linkText,
  source,
  color,
  onPress,
  onPressLink,
  height,
  style: bStyle,
  imageContainerStyle,
}) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);

  const OuterComp = onPress ? TouchableOpacity : View;

  const cardHeight = height || 162;
  let width = computeSourceSize(source, cardHeight);

  let imageStyle = { height: '100%', width: '100%' };
  let imageContainer = {
    ...style.imageInBannerCard,
    height: '100%',
    width: width,
    ...imageContainerStyle,
  };
  let cardStyle = {
    ...style.bannerCard,
    minHeight: cardHeight,
    overflow: 'hidden',
  };
  if (height) cardStyle.height = height;

  return (
    <OuterComp onPress={onPress} style={bStyle}>
      <GradientView style={cardStyle} color={color}>
        <View style={style.outerViewBannerCard}>
          <View style={imageContainer}>
            <Image source={source} style={imageStyle} resizeMode={'contain'} />
          </View>

          <View style={style.textsViewBannerCard}>
            <RegularBold
              color="singleton.white"
              style={style.titleTextBannerCard}
            >
              {title}
            </RegularBold>
            <Regular color="singleton.white">{description}</Regular>
            {linkText && (
              <Button
                color="singleton.white"
                ghost
                style={style.buttonTextBannerCard}
                onPress={onPressLink}
              >
                <Text>{linkText}</Text>
                <Icon name="long_arrow_right" />
              </Button>
            )}
          </View>
        </View>
      </GradientView>
    </OuterComp>
  );
};
