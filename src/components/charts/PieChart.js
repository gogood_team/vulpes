import React, { Component } from 'react';
import { View } from 'react-native';
import { PieChart as PChart } from 'react-native-gifted-charts';
import { getColors } from '../../colors';
import VulpesContext from '../../contexts/VulpesContext';
import { Fonts } from '../../fonts';
import { SmallBold, Small } from './../typos';

function flexedOut(d) {
  return { flex: d };
}

const colors = [
  'primary.100',
  'secondary.100',
  'comp1.100',
  'comp2.100',
  'comp3.100',
  'comp4.100',
  'primary.80',
  'secondary.80',
  'comp1.80',
  'comp2.80',
  'comp3.80',
  'comp4.80',
  'primary.60',
  'secondary.60',
  'comp1.60',
  'comp2.60',
  'comp3.60',
  'comp4.60',
  'primary.40',
  'secondary.40',
  'comp1.40',
  'comp2.40',
  'comp3.40',
  'comp4.40',
  'primary.20',
  'secondary.20',
  'comp1.20',
  'comp2.20',
  'comp3.20',
  'comp4.20',
];

const legendOuterStyle = { flexDirection: 'row', marginBottom: 12 };
const legendInnerStyle = {
  height: 18,
  width: 18,
  marginRight: 10,
  borderRadius: 4,
};
const chartWrapperStyle = {
  flexDirection: 'row',
  width: '100%',
  paddingLeft: 32,
  paddingRight: 32,
};
export class PieChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      with: null,
    };

    this.nItems = 0;
    const { data } = props;
    if (data && data.data && data.data.length > 0) {
      this.nItems = data.data.length;
    }
  }

  topLabel(value) {
    return <SmallBold>{value}</SmallBold>;
  }

  xLabelStyle() {
    return {
      fontSize: 11,
      textAlign: 'center',
    };
  }
  // todo: resolver inversão de cores
  parseGraphGradient() {
    const { color } = this.props;
    if (!color) return { gradientColor: '#000000', frontColor: '#000333' };
    const { theme } = this.context;
    const [family, grade] = color.split('.');
    const colorsT = getColors(theme);
    const gradientColor = colorsT(color);
    const frontColor = isNaN(grade)
      ? colorsT(`${color}_dark`)
      : colorsT(`${family}.110`);
    return { gradientColor, frontColor };
  }

  renderGraph() {
    const { data } = this.props;
    if (!this.props.width) return null;

    const itens = this.nItems;
    const wa = this.props.width - 12 - 12;

    this.spacing = (wa * 0.2) / (itens - 1);
    this.barWidth = (wa * 0.8) / itens;

    if (!data || !data.data || data.data.length === 0) {
      console.error('BarChart:: No data defined');
      return null;
    }

    const sGraph = { flex: 1, paddingBottom: 16 };
    const { theme } = this.context;
    const colorsT = getColors(theme);
    const total = data.data.reduce((ac, cum) => cum.value + ac, 0);

    const pieData = data.data
      .map((d, index) => {
        // console.log(colors[index]);
        return {
          ...d,
          color: colorsT(colors[index]),
          shiftTextX: -10,
          text:
            d.value / total > 0.1
              ? (100 * (d.value / total)).toFixed(1) + '%'
              : undefined,
        };
      })
      .sort((a, b) => b.value - a.value);

    return (
      <View style={sGraph}>
        <PChart
          donut
          initialAngle={-1.5}
          innerCircleColor={'white'}
          font={Fonts.smallBold.fontFamily}
          fontWeight={Fonts.smallBold.fontWeight}
          textSize={Fonts.smallBold.fontSize}
          focusOnPress
          showText
          strokeWidth={5}
          innerRadius={35}
          textColor={'#333'}
          strokeColor={'white'}
          innerCircleBorderWidth={5}
          innerCircleBorderColor={'white'}
          data={pieData}
        />
      </View>
    );
  }

  renderLegend(text, index) {
    const { theme } = this.context;
    const colorsT = getColors(theme);
    const color = colors[index];
    return (
      <View style={legendOuterStyle} key={'l_' + index}>
        <View
          style={{
            ...legendInnerStyle,
            backgroundColor: colorsT(color || 'white'),
          }}
        />
        <Small>{text || ''}</Small>
      </View>
    );
  }

  render() {
    const { data } = this.props;
    if (!data || !data.length === 0) return null;
    return (
      <View style={chartWrapperStyle}>
        <View style={flexedOut(2)}>{this.renderGraph()}</View>
        <View style={flexedOut(1)}>
          {data.data.map((d, index) => this.renderLegend(d.label, index))}
        </View>
      </View>
    );
  }
}

PieChart.contextType = VulpesContext;
