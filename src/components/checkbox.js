import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { getColors } from '../colors';
import VulpesContext from '../contexts/VulpesContext';
import { getFonts } from '../fonts';
import { Icon } from './icon';
import { Regular, Small } from './typos';

const CheckboxLabel = ({ label, labelStyle }) => {
  if (!label) return null;
  return <Regular style={labelStyle}>{label}</Regular>;
};

export class Checkbox extends Component {
  constructor(props) {
    super(props);
  }

  async handleChange() {
    const { onChange, checked } = this.props;
    return onChange && onChange(!checked);
  }

  renderBoxText() {
    const { text, floatRight } = this.props;
    return (
      <View style={floatRight ? this.newBodyStyle() : this.bodyStyle()}>
        {floatRight ? <Regular style={this.fontStyle()}>{text}</Regular> : null}
        <View style={this.checkboxStyle()}>{this.renderChecked()}</View>
        {!floatRight ? (
          <Regular style={this.fontStyle()}>{text}</Regular>
        ) : null}
      </View>
    );
  }

  render() {
    const { error, style, label, labelStyle, testID } = this.props;
    return (
      <View testID={testID} style={style}>
        <CheckboxLabel {...{ label, labelStyle }} />
        <TouchableOpacity
          onPress={() => this.handleChange()}
          style={this.completeStyle()}
        >
          {this.renderBoxText()}
        </TouchableOpacity>
        {error && (
          <Small style={this.errorStyle()} color={'error.100'}>
            {error}
          </Small>
        )}
      </View>
    );
  }

  renderChecked() {
    const { checked } = this.props;
    if (!checked) return null;
    return (
      <View style={this.checkedBoxStyle()}>
        <Icon size={16} name={'check'} color={'singleton.white'} />
      </View>
    );
  }

  errorStyle() {
    return {
      marginTop: 8,
    };
  }

  completeStyle() {
    return {
      ...this.fontStyle(),
      height: 38,
      marginTop: 2,
      flexDirection: 'row',
      alignItems: 'center',
    };
  }

  bodyStyle() {
    return {
      alignItems: 'center',
      flexDirection: 'row',
    };
  }
  newBodyStyle() {
    return {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
    };
  }

  checkboxStyle() {
    return {
      borderWidth: 1,
      borderRadius: 4,
      height: 20,
      width: 20,
      alignItems: 'center',
      justifyContent: 'center',
      borderColor: this.colorOutline(),
      marginRight: 8,
    };
  }

  checkedBoxStyle() {
    return {
      borderRadius: 4,
      height: 20,
      width: 20,
      alignItems: 'center',
      justifyContent: 'center',
      borderColor: this.colorOutline(),
      backgroundColor: this.colorRegular(),
    };
  }

  fontStyle() {
    const { theme } = this.context;
    const fonts = getFonts(theme);
    const colors = getColors(theme);
    let font = fonts.regular;
    if (this.props.error) font = { ...font, color: colors('error.100') };
    return font;
  }

  colorRegular() {
    const { theme } = this.context;
    const colors = getColors(theme);
    if (this.props.error) return colors('error.100');
    return colors('checkbox.checked');
  }

  colorOutline() {
    const { theme } = this.context;
    const colors = getColors(theme);
    if (this.props.error) return colors('error.100');
    return colors('gray.100');
  }
}
Checkbox.defaultProps = {
  floatRight: false,
  onChange: null,
};
Checkbox.contextType = VulpesContext;
