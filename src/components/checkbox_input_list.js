import React from 'react';
import { CheckboxInput } from 'react-native-vulpes';
import { View, FlatList } from 'react-native';

export const CheckboxInputList = ({ values, onChange, ...props }) => {
  const _handleChange = (_values, _index, _onChange) => {
    const newState = _values.map((item, index) => {
      if (index === _index) item.checked = true;
      else item.checked = false;
      return item;
    });
    return onChange(newState);
  };

  return (
    <View {...props}>
      <FlatList
        data={values}
        keyExtractor={(_, index) => `cbil-${index}`}
        renderItem={({ item, index }) => (
          <CheckboxInput
            clean
            text={item.text}
            value={item.checked}
            onChange={() => _handleChange(values, index, onChange)}
          />
        )}
      />
    </View>
  );
};
