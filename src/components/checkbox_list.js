import { View, FlatList } from 'react-native';
import React from 'react';
import { Checkbox } from 'react-native-vulpes';

export const CheckboxList = ({
  values,
  onChange,
  floatRight = false,
  ...props
}) => {
  const _handleChange = async (_values, _index, _onChange) => {
    const newState = _values.map((item, index) => {
      if (index === _index) item.checked = !item.checked;
      return item;
    });
    onChange(newState);
  };

  return (
    <View {...props}>
      <FlatList
        data={values}
        keyExtractor={(_, index) => `cb-${index}`}
        renderItem={({ item, index }) => (
          <Checkbox
            floatRight={floatRight}
            text={item.text}
            checked={item.checked}
            onChange={() => _handleChange(values, index, onChange)}
          />
        )}
      />
    </View>
  );
};
