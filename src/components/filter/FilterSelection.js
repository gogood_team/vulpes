export class FilterSelection {
  constructor(itens) {
    this.itens = { ...itens };
  }

  setNewFilter(list, item) {
    this.item = item;
    this.list_alias = list.alias;
    this.item_alias = item.alias;
    this.multi = list.multi;
  }

  getGroup() {
    if (!this.list_alias) return {};
    if (!this.itens[this.list_alias]) {
      this.itens[this.list_alias] = {};
    }
    return this.itens[this.list_alias];
  }

  get isMulti() {
    return this.multi === true;
  }

  clearGroupKeys(group) {
    if (!group[this.item_alias]) {
      Object.keys(group).forEach((k) => delete group[k]);
    }
  }

  toggleItem(group) {
    if (group[this.item_alias]) {
      delete group[this.item_alias];
    } else {
      group[this.item_alias] = this.item;
    }
  }

  updatedItens() {
    const group = this.getGroup();

    if (group || Object.keys(group).length > 0) {
      if (!this.isMulti) {
        this.clearGroupKeys(group);
      }
      this.toggleItem(group);
    }

    Object.keys(this.itens).forEach((k) => {
      if (Object.keys(this.itens[k]).length === 0) {
        delete this.itens[k];
      }
    });

    return this.itens;
  }

  hasFilters() {
    const itens = Object.keys(this.updatedItens());
    const res =
      itens.length > 1 || (itens.length === 1 && !itens.includes('query'));
    return res;
  }
}
