import React from 'react';
import { Button, Text } from 'react-native-vulpes';

const buttonMargin = { marginRight: 5, marginBottom: 5 };
export const FilterTagItem = function ({ onPress, selected, item }) {
  return (
    <Button
      onPress={onPress}
      color={'cyan'}
      outline={!selected}
      style={buttonMargin}
    >
      <Text>{item.name}</Text>
    </Button>
  );
};
