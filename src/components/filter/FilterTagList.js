import React, { Component } from 'react';
import { View } from 'react-native';
import { Button, H4, Icon, Text } from 'react-native-vulpes';
import { FilterTagItem } from './FilterTagItem';

const LINES_TO_SHOW = 2;
const LINE_HEIGHT = 49;

const containerItens = {
  marginTop: 16,
  flexDirection: 'row',
  flexWrap: 'wrap',
  overflow: 'hidden',
};

const moreBtnStyle = {
  fontFamily: 'OpenSans-Regular',
  fontWeight: 'bold',
  fontStyle: 'normal',
  letterSpacing: 0,
  color: 'black',
  fontSize: 12,
  paddingTop: 10,
};

const ShowMore = ({ showMore }) => {
  const mainStyle = { alignSelf: 'center' };
  return (
    <View style={mainStyle}>
      <Button onPress={showMore} ghost>
        <Text>Mostrar Mais</Text>
        <Icon name={'chevron_down'} />
      </Button>
    </View>
  );
};

const MoreAction = ({ lines, showLines, showMore, showLess }) => {
  if (!lines) return null;
  if (lines <= LINES_TO_SHOW) return null;

  if (showLines < lines) {
    return <ShowMore showMore={showMore} />;
  }

  const mainStyle = { alignSelf: 'center' };
  return (
    <View style={mainStyle}>
      <Button ghost onPress={showLess}>
        <Text>Mostrar menos</Text>
        <Icon name={'chevron_up'} />
      </Button>
    </View>
  );
};

class FilterTagListWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMore: false,
    };
    this.lines = null;
  }

  showMore() {
    this.setState({ showLines: this.lines });
  }

  showLess() {
    this.setState({ showLines: LINES_TO_SHOW });
  }

  onLayout({
    nativeEvent: {
      layout: { x, y, width, height },
    },
  }) {
    if (this.lines) return;
    const lines = parseInt(height / LINE_HEIGHT, 10);

    this.lines = lines;

    if (this.lines > LINES_TO_SHOW) {
      this.setState({
        showMore: true,
        showLines: LINES_TO_SHOW,
      });
    }
  }

  moreAction() {
    if (!this.lines) return null;
    if (this.lines <= LINES_TO_SHOW) return null;

    if (this.state.showLines < this.lines) {
      return (
        <Text
          style={moreBtnStyle}
          onPress={() => this.setState({ showLines: this.lines })}
        >
          Mostrar Mais <Icon name="chevron-down" />
        </Text>
      );
    }

    return (
      <Text
        style={moreBtnStyle}
        onPress={() => this.setState({ showLines: LINES_TO_SHOW })}
      >
        Mostrar menos <Icon name="chevron-up" />
      </Text>
    );
  }

  mapItens(list) {
    const options = list.options;
    const groupAlias = list.alias;

    if (!options) return null;

    return options.map((option, index) => {
      const key = index;
      return (
        <FilterTagItem
          key={key}
          item={option}
          onPress={() => this.props.selectItem(list, option)}
          selected={this.props.isSelected(groupAlias, option)}
        />
      );
    });
  }

  render() {
    const list = this.props.data;
    if (!list || !list.options || list.options.length === 0) return null;

    const mainStyle = { width: '100%' };
    return (
      <View style={mainStyle}>
        <H4> {list.title} </H4>
        <View
          style={{ ...containerItens, ...{ height: this.height() } }}
          onLayout={this.onLayout.bind(this)}
        >
          {this.mapItens(list)}
        </View>

        <MoreAction
          lines={this.lines}
          showLines={this.state.showLines}
          showMore={this.showMore.bind(this)}
          showLess={this.showLess.bind(this)}
        />
      </View>
    );
  }

  height() {
    if (!this.state.showLines) return null;
    return this.state.showLines * LINE_HEIGHT;
  }
}

export const FilterTagList = FilterTagListWrapper;
