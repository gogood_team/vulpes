import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { List, ListItem } from 'react-native-vulpes';
import { FilterSelection } from './FilterSelection';
import { FilterTagList } from './FilterTagList';

class FiltersListWrapper extends Component {
  selectItem(list, item) {
    const { currentFilters, setFilter } = this.props;
    const fs = new FilterSelection(currentFilters);
    fs.setNewFilter(list, item);
    const itens = fs.updatedItens();
    setFilter(itens);
  }

  isSelected(group, item) {
    const itens = this.props.currentFilters;
    if (!itens) return false;
    if (!itens[group]) return false;
    if (itens[group][item.alias]) {
      return true;
    }
    return false;
  }

  listItens() {
    const { filters } = this.props;
    if (!filters) {
      return null;
    }
    return filters.map((l, index) => {
      return (
        <ListItem key={'fList' + index}>
          <FilterTagList
            data={l}
            isSelected={this.isSelected.bind(this)}
            selectItem={this.selectItem.bind(this)}
            withSeparator={index < filters.length - 1}
          />
        </ListItem>
      );
    });
  }

  render() {
    const st = { paddingLeft: 16, paddingRight: 16 };
    return (
      <ScrollView contentContainerStyle={st}>
        <List>{this.listItens()}</List>
      </ScrollView>
    );
  }
}

export const FiltersList = FiltersListWrapper;
