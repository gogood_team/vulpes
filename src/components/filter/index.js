import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Button,
  Colors,
  H3,
  Icon,
  Modal,
  ModalContainer,
  PaddedView,
} from 'react-native-vulpes';
import { FiltersList } from './FiltersList';
// import userFilters from './userFilters.json';

import { getColors, useVulpes } from 'react-native-vulpes';

const Marker = ({ markerSize, borderColor, fillColor, up, ...rest }) => {
  const { theme } = useVulpes();
  const colors = getColors(theme);
  const borderColorVal = borderColor
    ? colors(borderColor)
    : colors('singleton.white');
  const fillColorVal = fillColor ? colors(fillColor) : colors('error.100');

  const markerCompleteStyle = {
    width: markerSize,
    height: markerSize,
    borderRadius: markerSize / 2,
    borderWidth: 1,
    borderColor: borderColorVal,
    backgroundColor: fillColorVal,
    right: 0,
    bottom: up ? undefined : 0,
    top: up ? 0 : undefined,
    position: 'absolute',
  };
  return <View style={markerCompleteStyle} />;
};

class GeneralModal extends Component {
  render() {
    const { children, visible, onClose } = this.props;
    return (
      <ModalContainer visible={visible} onClose={onClose}>
        <Modal onClose={onClose} clearModal>
          {children}
        </Modal>
      </ModalContainer>
    );
  }
}

const FilterButtons = ({ setFilter, onSearch, searchText }) => {
  const mainStyle = {
    borderTopWidth: 1,
    borderTopColor: Colors.light_gray,
    flexDirection: 'row',
    marginBottom: 32,
    paddingTop: 16,
    paddingBottom: 16,
    gap: 16,
  };
  return (
    <PaddedView style={mainStyle}>
      <Button outline onPress={() => setFilter({})} text={'Limpar filtros'} />
      <Button color={'gray.100'} onPress={onSearch} text={searchText} />
    </PaddedView>
  );
};

class ModalFilter extends Component {
  render() {
    const { onClose, visible, title, ...rest } = this.props;
    const hStyle = { marginTop: 16 };
    return (
      <GeneralModal visible={visible} onClose={onClose}>
        <PaddedView>
          <H3 style={hStyle} color={'gray.100'}>
            {title}
          </H3>
        </PaddedView>

        <FiltersList
          filters={this.props.filters}
          currentFilters={rest.currentFilters}
          setFilter={rest.setFilter}
        />

        <FilterButtons
          onSearch={rest.onSearch}
          setFilter={rest.setFilter}
          searchText={rest.searchText}
        />
      </GeneralModal>
    );
  }
}

export class SearchFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };
  }

  componentDidMount() {
    const filters = this.props.filters || [];

    const defaultFilters = this.props.currentFilters;

    for (let f of filters) {
      for (let o of f?.options) {
        if (o.default)
          defaultFilters[f.alias] = {
            [o.alias]: o,
          };
      }
    }

    this.props.setFilter(defaultFilters);
  }

  modal(value) {
    this.setState({ modal: value });
  }

  onSearch() {
    this.modal(false);
    this.props.onSearch();
  }

  hasFilter() {
    const { currentFilters } = this.props;
    if (!currentFilters) return false;
    if (Object.keys(currentFilters).length > 0) return true;
    return false;
  }

  renderFilterBTN() {
    const sizeVal = 20;
    const markerSize = (sizeVal * 4) / 10;
    return (
      <Button
        outline
        color={'singleton.black'}
        onPress={() => this.modal(true)}
      >
        <View style={{ width: sizeVal, height: sizeVal }}>
          <Icon name="filter" />
          {this.hasFilter() && (
            <Marker
              borderColor={'singleton.black'}
              up={false}
              fillColor={'rh.users'}
              markerSize={markerSize}
            />
          )}
        </View>
      </Button>
    );
  }

  render() {
    const { title, filters, searchText } = this.props;

    const mainStyle = { flexDirection: 'row' };
    return (
      <View style={mainStyle}>
        {this.renderFilterBTN()}

        <ModalFilter
          title={title || 'Filtrar:'}
          searchText={searchText || 'Buscar'}
          filters={filters || []}
          visible={this.state.modal}
          onClose={() => this.modal(false)}
          currentFilters={this.props.currentFilters}
          setFilter={this.props.setFilter}
          onSearch={this.onSearch.bind(this)}
        />
      </View>
    );
  }
}
