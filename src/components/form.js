import React, { Component } from 'react';
import { Button } from 'react-native-vulpes';
import { FormSession } from './forms/FormSession';
import { View } from 'react-native';
import { Spinner } from './spinner';

const advBtnStyle = {
  flexDirection: 'row',
  gap: 16,
  flex: 1,
  justifyContent: 'center',
  marginTop: 16,
};

export class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 0,
      complete: false,
      loading: false,
    };
  }

  changeStep(s = 1) {
    this.setState({ loading: true });
    const { form } = this.props;
    const { step } = this.state;
    if (step < form.length) {
      const newStep = step + s;
      if (newStep === form.length) {
        return this.props.onSave?.();
      }
      this.setState({ step: newStep });
      this.props.onChangeStep?.(newStep);
    }
    setTimeout(() => {
      this.setState({ loading: false });
    }, 100);
  }

  renderAdvanceBtn() {
    const { form } = this.props;
    const { step } = this.state;
    if (form.length <= 1) return null;

    const adv = step < form.length;
    const ret = step > 0 && step < form.length;

    const stl = { minWidth: 120, justifyContent: 'center' };
    return (
      <View style={advBtnStyle}>
        {ret && (
          <Button
            text="Voltar"
            onPress={() => this.changeStep(-1)}
            style={stl}
          />
        )}
        {adv && (
          <Button
            text={step === form.length - 1 ? 'Salvar' : 'Avançar'}
            disabled={!this.state.complete}
            onPress={() => this.changeStep(1)}
            style={stl}
          />
        )}
      </View>
    );
  }
  onAnswer(answers, complete, field) {
    this.setState({ complete });
    this.props.onAnswer?.(answers, complete, field);
  }
  onEndSession() {
    if (!this.state.complete) return;
    this.changeStep(1);
  }

  render() {
    const { step, loading } = this.state;
    if (loading) return <Spinner />;
    const { form, style, hide, answers, errors } = this.props;
    const currentForm = form[step];
    return (
      <>
        <FormSession
          form={currentForm}
          answers={answers}
          errors={errors}
          onAnswer={this.onAnswer.bind(this)}
          style={style}
          onEndSession={this.onEndSession.bind(this)}
          hide={hide}
        />
        {this.renderAdvanceBtn()}
      </>
    );
  }
}
