import React, { Component } from 'react';
import { View } from 'react-native';
// import {FormItemCC} from './FormItemCC';
import { FormItemCEP } from './FormItemCEP';
import { FormItemCPF } from './FormItemCPF';
// import {FormItemCVV} from './FormItemCVV';
import { FormItemDate } from './FormItemDate';
import { FormItemEmail } from './FormItemEmail';
// import {FormItemExpiration} from './FormItemExpiration';
import { FormItemFullName } from './FormItemFullName';
import { FormItemOptions } from './FormItemOptions';
import { FormItemPassword } from './FormItemPassword';
import { FormItemPhone } from './FormItemPhone';
import { FormItemText } from './FormItemText';

export class FormItem extends Component {
  questionAdapted(question) {
    const newQuestion = { ...question };

    newQuestion.label = question.label + (question.required ? ' *' : '');

    return newQuestion;
  }

  fieldByType() {
    const { question: item, ...rest } = this.props;
    const question = this.questionAdapted(item);
    const type = question.type;

    const types = this.componentsFromQuestion(question, rest);

    const func = types[type];
    if (!func) return null;

    return func();
  }

  componentsFromQuestion(question, rest) {
    return {
      text: () => <FormItemText question={question} {...rest} />,
      email: () => <FormItemEmail question={question} {...rest} />,
      date: () => <FormItemDate question={question} {...rest} />,
      cpf: () => <FormItemCPF question={question} {...rest} />,
      cep: () => <FormItemCEP question={question} {...rest} />,
      phone: () => <FormItemPhone question={question} {...rest} />,
      option: () => <FormItemOptions question={question} {...rest} />,
      // 'card': () => <FormItemCC question={question} {...rest} />,
      // 'expiration': () => <FormItemExpiration question={question} {...rest} />,
      // 'cvv': () => <FormItemCVV question={question} {...rest} />,
      fullname: () => <FormItemFullName question={question} {...rest} />,
      password: () => <FormItemPassword question={question} {...rest} />,
    };
  }

  render() {
    if (!this.props.shouldShow) return null;
    const ss = { marginBottom: 24 };
    return (
      <View key={'qstin'} style={ss}>
        {this.fieldByType()}
      </View>
    );
  }
}
