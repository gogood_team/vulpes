import React, { Component } from 'react';
import { TextInput } from 'react-native-vulpes';

export class FormItemCEP extends Component {
  validateCep(field, value) {
    const CEP = this.normalizeCEP(value);
    this.props.onChangeAnswer(field, CEP).then(() => {
      if (this.validCep(CEP)) {
        this.props.onAnswer(field, CEP);
      } else {
        this.props.onAnswer(field, null);
      }
    });
  }

  normalizeCEP(value) {
    return value
      .replace(/[^0-9]/g, '')
      .replace(/^(\d{5})(\d)/, '$1-$2')
      .replace(/(-\d{3})\d+?$/, '$1');
  }

  validCep(cep) {
    const re = new RegExp(/\d{5}-\d{3}/);
    if (!re.test(cep)) {
      return false;
    }
    return true;
  }

  render() {
    const { question: item, errorMessage } = this.props;

    return (
      <TextInput
        {...this.props}
        error={errorMessage}
        label={item.label}
        key={item.field}
        ref={(input) => this.props.refField(item.field, input)}
        onSubmitEditing={() => this.props.nextField(item.field)}
        placeholder={item.placeholder}
        value={this.props.value}
        onChangeText={(value) => this.validateCep(item.field, value)}
        placeholderTextColor={{ color: '#d3d5da' }}
        keyboardType={'number-pad'}
        underlineColorAndroid="rgba(0,0,0,0)"
      />
    );
  }
}
