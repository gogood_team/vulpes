import React, { Component } from 'react';
import { TextInput } from 'react-native-vulpes';
import { isValidCPF } from '../../utils/CPFValidation';

export class FormItemCPF extends Component {
  validateCpf(field, value) {
    const cpf = this.normalizeCPF(value);
    this.props.onChangeAnswer(field, cpf).then(() => {
      const valid = isValidCPF(cpf);
      if (valid) {
        this.props.onAnswer(field, cpf);
      } else {
        this.props.onAnswer(field, null);
      }
    });
  }

  normalizeCPF(value) {
    return value
      .replace(/\D/g, '')
      .replace(/(\d{3})(\d)/, '$1.$2')
      .replace(/(\d{3})(\d)/, '$1.$2')
      .replace(/(\d{3})(\d{1,2})/, '$1-$2')
      .replace(/(-\d{2})\d+?$/, '$1');
  }

  get errorMessage() {
    const { question, value } = this.props;
    if (!value && !question.required) return undefined;
    if (isValidCPF(value)) return undefined;
    if ([null, undefined, ''].includes(value)) {
      return undefined;
    }
    return 'CPF inválido';
  }

  render() {
    const { question: item, errorMessage } = this.props;

    return (
      <TextInput
        {...this.props}
        error={errorMessage || this.errorMessage}
        key={item.field}
        ref={(input) => this.props.refField(item.field, input)}
        onSubmitEditing={() => this.props.nextField(item.field)}
        placeholder={item.placeholder}
        label={item.label}
        value={this.props.value}
        onChangeText={(value) => this.validateCpf(item.field, value)}
        placeholderTextColor={{ color: '#d3d5da' }}
        keyboardType={'number-pad'}
      />
    );
  }
}
