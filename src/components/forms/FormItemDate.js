import moment from 'moment';
import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import DatePickerModal from 'react-native-modal-datetime-picker';
import { TextInput } from 'react-native-vulpes';

const formInputStyle = {
  flex: 1,
  height: 44,
  paddingLeft: 8,
  justifyContent: 'center',
  alignItems: 'flex-start',
  opacity: 0.98,
  borderStyle: 'solid',
  borderWidth: 1,
  borderColor: '#d9d9d9',
  borderBottomWidth: 1,
  borderBottomColor: '#d9d9d9',
  borderTopWidth: 1,
  borderTopColor: '#d9d9d9',
  borderLeftWidth: 1,
  borderLeftColor: '#d9d9d9',
  borderRightWidth: 1,
  borderRightColor: '#d9d9d9',
  fontFamily: 'OpenSans-Regular',
  fontSize: 16,
  fontWeight: 'normal',
  fontStyle: 'normal',
  letterSpacing: 0,
  color: '#707070',
};

const DateTextInput = (props) => {
  const { item, value, errorMessage } = props;
  console.log('ITEM', item);
  if (!item) return null;

  return (
    <TextInput
      key={item.field}
      error={errorMessage}
      pointerEvents="none"
      ref={(input) => props.refField(item.field, input)}
      onSubmitEditing={() => props.nextField(item.field)}
      editable={false}
      placeholder={item.placeholder}
      value={value}
      onFocus={(event) => props.showPicker()}
      style={formInputStyle}
      placeholderTextColor={{ color: '#d3d5da' }}
      underlineColorAndroid="rgba(0,0,0,0)"
    />
  );
};

const DateSelector = (props) => {
  console.log(Object.keys(props));
  const { value } = props;
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={() => props.showPicker()}>
      <DateTextInput value={value} item={props.question} {...props} />
    </TouchableOpacity>
  );
};

class DateModal extends Component {
  get format() {
    const { item } = this.props;
    return item.format || 'DD/MM/YYYY';
  }

  get day() {
    const { value } = this.props;
    return moment(value || new Date(), this.format);
  }

  get valuePicker() {
    return new Date(this.day.valueOf() - this.day.utcOffset() * 60000);
  }

  handleConfirmation(value) {
    const { item } = this.props;
    this.setState({ showDatePicker: false });
    const val = moment(value).format(this.format);
    this.validateDate(item.field, val, this.format);
  }

  render() {
    const { showDatePicker } = this.props;

    return (
      <DatePickerModal
        isVisible={showDatePicker}
        date={this.valuePicker}
        mode={'date'}
        maximumDate={new Date()}
        onConfirm={this.handleConfirmation.bind(this)}
        onCancel={() => {
          this.setState({ showDatePicker: false });
        }}
        headerTextIOS={'Selecione uma data'}
        confirmTextIOS={'Confirmar'}
        cancelTextIOS={'Cancelar'}
      />
    );
  }
}

export class FormItemDate extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  validateDate(field, value, format) {
    const val = moment(value, format);
    if (val.isValid()) {
      this.props.onAnswer(field, val.format('YYYY-MM-DD'));
    } else {
      this.props.onAnswer(field, null);
    }
  }

  render() {
    const item = this.props.question;
    const ss = { borderWidth: 2, height: 50 };
    return (
      <View style={ss}>
        <DateSelector
          {...this.props}
          showPicker={() => this.setState({ showDatePicker: true })}
        />
        <DateModal
          showDatePicker={this.state.showDatePicker}
          item={item}
          value={this.props.value}
        />
      </View>
    );
  }
}
