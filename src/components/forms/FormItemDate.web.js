import moment from 'moment';
import React, { Component } from 'react';
import { TextInput } from 'react-native-vulpes';

export class FormItemDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mask: props.value,
    };
  }

  validateDate(field, value, format) {
    let newText = '';
    let mask = '99/99/9999';

    for (let i = 0, j = 0; i < Math.min(value.length, mask.length); i++) {
      if (mask[j] === '9') {
        if (/\d/.test(value[i])) {
          newText += value[i];
          j++;
        }
      } else {
        newText += mask[j];
        j++;
        if (/\d/.test(value[i])) {
          i--;
        }
      }
    }

    this.setState({ mask: newText });

    if (newText.length === 10) {
      const val = moment(value, 'DD/MM/YYYY');
      if (val.isValid()) {
        this.props.onAnswer(field, val.format('DD/MM/YYYY'));
      } else {
        this.props.onAnswer(field, null);
      }
    } else {
      this.props.onAnswer(field, null);
    }
  }

  get errorMessage() {
    const { question } = this.props;
    console.log(question);
    if (!question.required) {
      return undefined;
    }
    const val = moment(this.state.mask, 'DD/MM/YYYY');
    if (this.state.mask && this.state.mask.length === 10 && val.isValid()) {
      return undefined;
    }
    if ([null, undefined, ''].includes(this.state.mask)) {
      return undefined;
    }
    return 'Data inválida';
  }

  render() {
    const { question: item, errorMessage } = this.props;

    return (
      <TextInput
        {...this.props}
        type={'date'}
        error={errorMessage || this.errorMessage}
        key={item.field}
        ref={(input) => this.props.refField(item.field, input)}
        onSubmitEditing={() => this.props.nextField(item.field)}
        placeholder={item.placeholder}
        label={item.label}
        value={this.state.mask}
        onChangeText={(value) =>
          this.validateDate(item.field, value, 'DD/MM/YYYY')
        }
        placeholderTextColor={{ color: '#d3d5da' }}
        keyboardType={'number-pad'}
      />
    );
  }
}
