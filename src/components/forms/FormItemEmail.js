import React, { Component } from 'react';
import { TextInput } from 'react-native-vulpes';

export class FormItemEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editable: false,
    };
  }

  componentDidMount() {
    const { type } = this.props.question;
    if (type === 'email') {
      setTimeout(() => {
        this.setState({ editable: true });
      }, 100);
    }
  }

  validEmail(email) {
    const part11 = /([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)/;
    const part12 = /(".+")/;
    const part1 = '(' + part11.source + '|' + part12.source + ')@';
    const part21 = /(\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])/;
    const part22 = /(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})/;
    const part2 = '(' + part21.source + '|' + part22.source + ')';
    const re = new RegExp('^' + part1 + part2 + '$');
    return re.test(String(email).trim().toLowerCase());
  }

  validateEmail(field, value) {
    this.props.onChangeAnswer(field, value).then(() => {
      if (value === '' || !this.validEmail(value)) {
        this.props.onAnswer(field, null);
      } else {
        this.props.onAnswer(field, value.trim().toLowerCase());
      }
    });
  }

  get errorMessage() {
    const { question } = this.props;
    if (!question.required) return undefined;
    if (this.validEmail(this.props.value)) return undefined;
    if ([null, undefined, ''].includes(this.props.value)) return undefined;
    return 'E-mail inválido';
  }

  render() {
    const { question: item, errorMessage, editable } = this.props;

    return (
      <TextInput
        {...this.props}
        error={errorMessage || this.errorMessage}
        editable={editable && this.state.editable}
        key={item.field}
        label={item.label}
        ref={(input) => this.props.refField(item.field, input)}
        onSubmitEditing={() => this.props.nextField(item.field)}
        placeholder={item.placeholder}
        value={this.props.value}
        onChangeText={(value) => this.validateEmail(item.field, value)}
        placeholderTextColor={{ color: '#d3d5da' }}
        underlineColorAndroid="rgba(0,0,0,0)"
        autoComplete={'email'}
        textContentType={'emailAddress'}
        keyboardType={'email-address'}
        autoCapitalize={'none'}
      />
    );
  }
}
