import moment from 'moment';
import React, { Component } from 'react';
import { TextInput } from 'react-native-vulpes';

import { getSafe } from '../../../utils/errorHandler';

export class FormItemExpiration extends Component {
  validateExpiration(field, value) {
    const expiration = this.normalizeExpiration(value);
    this.props.onChangeAnswer(field, expiration).then(() => {
      if (this.validExpiration(expiration) > 0) {
        // this.loadInfosFromExpiration(value);
        this.props.onAnswer(field, expiration);
      } else {
        this.props.onAnswer(field, null);
      }
    });
  }

  normalizeExpiration(value) {
    return value
      .replace(/^(\d{2})(\d)/, '$1/$2')
      .replace(/(\/\d{2})\d+?$/, '$1');
  }

  validExpiration(expiration) {
    const re = new RegExp(/\d{2}\/\d{2}/);
    if (!re.test(expiration)) {
      return -1;
    }
    const exp = moment(expiration, 'MM/YY').endOf('month');
    const d = getSafe(() => exp.diff(moment()) > 0, -2);
    return d;
  }

  get errorMessage() {
    const { question, value } = this.props;
    if (!question.required) {
      return undefined;
    }
    if ([null, undefined, ''].includes(value)) {
      return undefined;
    }
    const valid = this.validExpiration(value);
    if (valid > 0) {
      return undefined;
    }
    if (valid === -1) return 'Formato inválido';
    return 'Data inválida';
  }

  render() {
    const { question: item, errorMessage } = this.props;

    return (
      <TextInput
        {...this.props}
        error={errorMessage || this.errorMessage}
        label={item.label}
        key={item.field}
        ref={(input) => this.props.refField(item.field, input)}
        onSubmitEditing={() => this.props.nextField(item.field)}
        placeholder={item.placeholder}
        value={this.props.value}
        onChangeText={(value) => this.validateExpiration(item.field, value)}
        placeholderTextColor={{ color: '#d3d5da' }}
        keyboardType={'number-pad'}
        underlineColorAndroid="rgba(0,0,0,0)"
      />
    );
  }
}
