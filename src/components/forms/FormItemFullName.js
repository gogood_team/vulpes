import React, { Component } from 'react';
import { TextInput } from 'react-native-vulpes';

export class FormItemFullName extends Component {
  constructor(props) {
    super(props);
  }

  get errorMessage() {
    if (this.isRequired()) return undefined;
    if (this.isEmpty()) return undefined;
    if (this.minimumSize()) return 'Nome deve ter pelo menos 1 caracteres';
    if (this.maximumSize()) return 'Nome deve ter no máximo 100 caracteres';
    if (!this.isFullName()) return this.errorMessageForFullName();
    return undefined;
  }

  errorMessageForFullName() {
    const { question } = this.props;
    return question.invalid || 'Campo deve conter nome completo';
  }

  maximumSize() {
    const { value } = this.props;
    return value.length > 100;
  }

  minimumSize() {
    const { value } = this.props;
    return value.length < 1;
  }

  isEmpty() {
    const { value } = this.props;
    return [null, undefined, ''].includes(value);
  }

  isRequired() {
    const { question } = this.props;
    return !question.required;
  }

  isFullName() {
    const { value } = this.props;
    if (!value) return false;
    const names = value.trim().split(' ');
    if (names.length < 2) return false;
    let count = 0;
    for (const n of names) {
      if (n.length >= 2) count += 1;
      if (count >= 2) return true;
    }
    return false;
  }

  validateText(field, value) {
    this.props.onChangeAnswer(field, value).then(() => {
      if (!this.isFullName(value)) value = null;
      this.props.onAnswer(field, value);
    });
  }

  onEndingEdit(e) {
    const { field } = this.props.question;
    const orgVal = e.nativeEvent?.text;
    const newVal = orgVal?.trim();
    if (orgVal !== newVal) this.validateText(field, newVal);
  }

  render() {
    const { question: item, errorMessage } = this.props;

    return (
      <TextInput
        {...this.props}
        key={item.field}
        error={errorMessage || this.errorMessage}
        ref={(input) => this.props.refField(item.field, input)}
        onSubmitEditing={() => this.props.nextField(item.field)}
        placeholder={item.placeholder}
        value={this.props.value}
        label={item.label}
        onChangeText={(value) => this.validateText(item.field, value)}
        placeholderTextColor={{ color: '#d3d5da' }}
        underlineColorAndroid="rgba(0,0,0,0)"
        blurOnSubmit={false}
        autoCapitalize={'words'}
        onEndEditing={(e) => this.onEndingEdit(e)}
        autoFocus={true}
      />
    );
  }
}
