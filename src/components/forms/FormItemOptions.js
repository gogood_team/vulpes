import React, { Component } from 'react';
import { SelectInput, SelectItem } from 'react-native-vulpes';

export class FormItemOptions extends Component {
  constructor(props) {
    super(props);
    const value = props.value;
    this.state = {
      value: value,
    };
  }

  componentDidUpdate(prevProps) {
    const { value } = this.props;
    if (value !== prevProps.value) {
      this.setState({ value: value });
    }
  }

  validateOptions(field, value) {
    this.props.onAnswer(field, value);
    this.setState({ value: value });
  }

  get errorMessage() {
    const { question } = this.props;
    if (!question.required) {
      return undefined;
    }
    if (this.state.value === undefined) {
      return undefined;
    }
    if (this.state.value === null) {
      return 'Campo não preenchido';
    }
    return undefined;
  }

  render() {
    const { question: item, errorMessage } = this.props;
    if (!item) return null;

    return (
      <SelectInput
        key={item.field}
        testID={item.field}
        error={errorMessage || this.errorMessage}
        inputRef={(input) => this.props.refField(item.field, input)}
        placeholder={item.placeholder}
        label={item.label}
        value={this.state.value}
        onChangeValue={(value) => this.validateOptions(item.field, value)}
      >
        {item.options.map((ii) => {
          return (
            <SelectItem
              label={ii.label}
              key={ii.field + '_' + ii.value}
              value={ii.value}
            />
          );
        })}
      </SelectInput>
    );
  }
}
