import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Icon, TextInput } from 'react-native-vulpes';

const tStyle = {
  width: 24,
  height: 24,
  position: 'absolute',
  top: 30,
  right: 0,
  justifyContent: 'center',
  alignItems: 'center',
};

export class FormItemPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: true,
    };
  }

  get errorMessage() {
    const { value } = this.props;
    const { question } = this.props;
    if (!question.required) return undefined;
    if ([null, undefined, ''].includes(value)) return undefined;
    if (value.length < 8) return 'Senha deve ter pelo menos 8 caracteres';
    if (value.length > 100) return 'Senha deve ter no máximo 100 caracteres';
    return undefined;
  }

  validateText(field, value) {
    this.props.onChangeAnswer(field, value).then(() => {
      if (this.errorMessage) value = null;
      this.props.onAnswer(field, value);
    });
  }

  renderPassView(password) {
    return (
      <TouchableOpacity
        style={tStyle}
        onPress={() => this.setState({ password: !password })}
      >
        <Icon name={password ? 'eye_off' : 'eye'} size={16} />
      </TouchableOpacity>
    );
  }

  fixedParameters() {
    return {
      underlineColorAndroid: 'rgba(0,0,0,0)',
      blurOnSubmit: false,
      autoCapitalize: 'none',
      autoCorrect: false,
      autoCompleteType: 'password',
      textContentType: 'password',
      autoComplete: 'password',
    };
  }

  render() {
    const { question: item, errorMessage } = this.props;
    const { password } = this.state;
    return (
      <View>
        <TextInput
          {...this.props}
          key={item.field}
          error={errorMessage || this.errorMessage}
          ref={(input) => this.props.refField(item.field, input)}
          onSubmitEditing={() => this.props.nextField(item.field)}
          placeholder={item.placeholder}
          secureTextEntry={password}
          value={this.props.value}
          label={item.label}
          onChangeText={(value) => this.validateText(item.field, value)}
          placeholderTextColor={{ color: '#d3d5da' }}
          {...this.fixedParameters()}
        />
        {this.renderPassView(password)}
      </View>
    );
  }
}
