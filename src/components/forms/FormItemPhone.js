import React, { Component } from 'react';
import { TextInput } from 'react-native-vulpes';

export class FormItemPhone extends Component {
  validPhone(phone) {
    if (!phone) {
      return false;
    }
    phone = phone
      .replace('(', '')
      .replace('-', '')
      .replace(' ', '')
      .replace(')', '');
    const reMobilePhone = new RegExp(/\d{10,11}/);
    if (!reMobilePhone.test(phone)) {
      return false;
    }
    return true;
  }

  validatePhone(field, value) {
    const phone = this.normalizePhone(value);
    this.props.onChangeAnswer(field, phone).then(() => {
      const valid = this.validPhone(phone);
      if (valid) {
        this.props.onAnswer(field, phone);
      } else {
        this.props.onAnswer(field, null);
      }
    });
  }

  normalizePhone(value) {
    let phone = value.replace(/\D/g, '');
    phone = phone.replace(/^0/, '');
    if (phone.length > 10) {
      phone = phone.replace(/^(\d\d)(\d{5})(\d{4}).*/, '($1) $2-$3');
    } else if (phone.length === 10) {
      phone = phone.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, '($1) $2-$3');
    } else if (phone.length > 2) {
      phone = phone.replace(/^(\d\d)(\d{0,5})/, '($1) $2');
    } else {
      phone = phone.replace(/^(\d*)/, '$1');
    }
    return phone;
  }

  get errorMessage() {
    const { question } = this.props;
    if (!question.required) {
      return undefined;
    }
    if (this.validPhone(this.props.value)) {
      return undefined;
    }
    if ([null, undefined, ''].includes(this.props.value)) {
      return undefined;
    }
    return 'Telefone inválido';
  }

  render() {
    const { question: item, errorMessage } = this.props;

    return (
      <TextInput
        {...this.props}
        error={errorMessage || this.errorMessage}
        key={item.field}
        label={item.label}
        ref={(input) => this.props.refField(item.field, input)}
        onSubmitEditing={() => this.props.nextField(item.field)}
        placeholder={item.placeholder}
        value={this.props.value}
        onChangeText={(value) => this.validatePhone(item.field, value)}
        placeholderTextColor={{ color: '#d3d5da' }}
        keyboardType={'phone-pad'}
        underlineColorAndroid="rgba(0,0,0,0)"
      />
    );
  }
}
