import React, { Component } from 'react';
import { TextInput } from '../text_input';
//

export class FormItemText extends Component {
  constructor(props) {
    super(props);
  }

  get errorMessage() {
    const { value, question, noLimitCharacters } = this.props;
    if (!question.required) return undefined;
    if ([null, undefined, ''].includes(value)) return undefined;
    if (value.length < 1) return 'Texto deve ter pelo menos 1 caracteres';
    if (!noLimitCharacters && value.length > 100)
      return 'Texto deve ter no máximo 100 caracteres';
    return undefined;
  }

  validateText(field, value) {
    return this.props.onChangeAnswer(field, value).then(() => {
      if (value === '') value = null;
      return this.props.onAnswer(field, value);
    });
  }

  onEndingEdit(e) {
    const { field } = this.props.question;
    const orgVal = e.nativeEvent?.text;
    const newVal = orgVal?.trim();
    if (orgVal !== newVal) this.validateText(field, newVal);
  }

  render() {
    const { question: item, errorMessage } = this.props;

    return (
      <TextInput
        {...this.props}
        key={item.field}
        error={errorMessage || this.errorMessage}
        ref={(input) => this.props.refField(item.field, input)}
        onSubmitEditing={() => this.props.nextField(item.field)}
        placeholder={item.placeholder}
        value={this.props.value}
        label={item.label}
        onChangeText={(value) => this.validateText(item.field, value)}
        placeholderTextColor={{ color: '#d3d5da' }}
        underlineColorAndroid="rgba(0,0,0,0)"
        onEndEditing={(e) => this.onEndingEdit(e)}
        blurOnSubmit={false}
      />
    );
  }
}
