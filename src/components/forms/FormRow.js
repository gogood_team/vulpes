import React from 'react';
import { View } from 'react-native';

export const FormRow = ({ index, questions, renderQuestion }) => {
  if (!questions || questions.length === 0) return null;
  const ss = { flexDirection: 'row' };
  return (
    <View style={ss}>
      {questions.map((item, i2) => {
        const indexStyle = { paddingLeft: 8, paddingRight: 8, flex: 1 };
        if (i2 === 0) indexStyle.paddingLeft = 0;

        if (i2 === questions.length - 1) indexStyle.paddingRight = 0;

        return (
          <View style={indexStyle} key={'row_' + index + '_' + i2}>
            {renderQuestion(item, index + '_' + i2)}
          </View>
        );
      })}
    </View>
  );
};
