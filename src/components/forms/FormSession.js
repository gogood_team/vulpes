import React, { Component } from 'react';
import { Linking, View } from 'react-native';
import { H4, Small } from 'react-native-vulpes';
import { HTMLRender } from '../html_render';
import { Questions } from './Questions';

const SessionHeader = (props) => {
  if (!props.title) return null;
  const ss = { marginTop: 24, marginBottom: 24 };
  return (
    <View style={ss}>
      <H4>{props.title}</H4>
    </View>
  );
};

const RequiredSession = (props) => {
  const { hasRequired } = props;
  if (!hasRequired) {
    return null;
  }
  return <Small color="gray.80">{'* Campos obrigatórios'}</Small>;
};

const SessionDescription = (props) => {
  const { description } = props;
  if (!description) return null;
  return (
    <HTMLRender onLink={(href) => Linking.openURL(href)} html={description} />
  );
};

const successStyle = {
  color: 'success.100',
};
const errorStyle = {
  color: 'error.100',
};

const Messages = ({ messages }) => {
  if (!messages) return null;
  const msgs = [messages].flat();

  if (msgs.length === 0) return null;
  const mode = {
    success: successStyle,
    error: errorStyle,
  };

  const msgComp = msgs.map((message, index) => {
    if (!message) return null;

    const type = mode[message.type] || null;
    return (
      <Small key={'_' + index} {...type}>
        {message.text || message}
      </Small>
    );
  });

  const style = { marginTop: 8, marginBottom: 32 };
  return <View style={style}>{msgComp}</View>;
};

export class FormSession extends Component {
  constructor(props) {
    super(props);
  }

  hasRequired() {
    const { form } = this.props;
    if (form.hasRequired !== undefined) {
      return form.hasRequired;
    }
    return true;
  }

  getErrorMessagesFromKeys(currKeys, errKeys) {
    const { messages, errors } = this.props;
    const msgs = [messages].flat();
    for (const k of errKeys) {
      if (currKeys.includes(k)) continue;
      msgs.push({ type: 'error', text: errors[k] });
    }
    return msgs;
  }

  getErrorMessages() {
    const { form, messages, errors } = this.props;
    try {
      const currKeys = form.questions.map((f) => f.field);
      const errKeys = Object.keys(errors);

      return this.getErrorMessagesFromKeys(currKeys, errKeys);
    } catch (error) {
      return messages;
    }
  }

  renderSessionDescription() {
    const { form, description, noDescription } = this.props;
    if (noDescription) return null;
    return <SessionDescription description={description || form.description} />;
  }

  renderSessionHeader() {
    const { form } = this.props;
    return (
      <SessionHeader title={form.title} hasRequired={this.hasRequired()} />
    );
  }

  renderInnerDescription() {
    const { form } = this.props;
    return <SessionDescription description={form.innerDescription} />;
  }

  render() {
    const { form, answers, onAnswer, style, errors, hide, onEndSession } =
      this.props;
    const messages = this.getErrorMessages();
    if (!form || !form.questions) return null;

    return (
      <View style={style}>
        {this.renderSessionDescription()}
        {this.renderSessionHeader()}
        {this.renderInnerDescription()}
        {!hide && <RequiredSession hasRequired={this.hasRequired()} />}
        <Questions
          questions={form.questions}
          answers={answers}
          onAnswer={onAnswer}
          errors={errors}
          onEndSession={onEndSession}
        />
        <Messages messages={messages} />
      </View>
    );
  }
}
