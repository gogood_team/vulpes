import React, { Component } from 'react';
import { Keyboard, View } from 'react-native';
import { FormItem } from './FormItem.js';
import { FormRow } from './FormRow';

export class Questions extends Component {
  constructor(props) {
    super(props);

    this.precedences = [];

    const { questions } = this.props;
    if (questions) {
      questions.forEach((f, i) => {
        if (f.showIf !== undefined) {
          this.precedences.push(f.showIf.field);
        }
      });
    }

    this.orderedFields = [];
    this.referenceField = [];
    this.state = {
      answers: props.answers,
      filledAnswers: props.answers,
    };
  }

  complete() {
    let complete = true;
    const { questions } = this.props;
    if (!questions) {
      return false;
    }
    for (const i in questions) {
      if (!questions[i]) {
        continue;
      }
      const f = questions[i];
      if (f.required && !this.state.filledAnswers[f.field]) {
        complete = false;
        break;
      }
    }
    return complete;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.answers !== this.props.answers) {
      const newAnswers = this.updateAnswersFromProps();
      this.setState({
        answers: newAnswers,
        filledAnswers: this.props.answers,
      });
    }
  }

  updateAnswersFromProps() {
    const newAnswers = { ...this.state.answers };
    for (const field in this.props.answers) {
      if (this.props.answers[field] || this.props.answers[field] === '') {
        newAnswers[field] = this.props.answers[field];
      }
    }
    return newAnswers;
  }

  componentDidMount() {
    const { onAnswer, questions } = this.props;
    onAnswer && onAnswer(this.state.filledAnswers, this.complete());
    try {
      const firstQuestion = questions[0].field;
      this.referenceField[firstQuestion].focus();
      console.log('DID MOUNT QUESTIONS');
      console.log(this.referenceField[firstQuestion]);
    } catch (error) {
      console.log('DID MOUNT QUESTIONS ERROR');
      console.log(error);
    }
  }

  onAnswer(field, value) {
    const { onAnswer } = this.props;
    console.log('ON ANSWER', field);

    this.setState(
      {
        filledAnswers: {
          ...this.state.filledAnswers,
          [field]: value,
        },
      },
      () => {
        // console.log('@@@@@', this.state.filledAnswers);
        onAnswer && onAnswer(this.state.filledAnswers, this.complete(), field);
      }
    );
  }

  onChangeAnswer(field, value) {
    return new Promise((success) => {
      this.setState(
        {
          answers: {
            ...this.state.answers,
            [field]: value,
          },
        },
        () => success()
      );
    });
  }

  refField(field, input) {
    this.referenceField[field] = input;
  }

  getOrderedFields(questions) {
    if (this.orderedFields.length > 0) {
      return this.orderedFields;
    }
    const allFields = [];
    for (const question of questions) {
      if (question.row) {
        this.addRowFields(question, allFields);
      } else {
        allFields.push(question.field);
      }
    }
    this.orderedFields = allFields;
    return this.orderedFields;
  }

  addRowFields(question, allFields) {
    for (const q2 of question.row) {
      allFields.push(q2.field);
    }
  }

  nextField(field) {
    const { questions } = this.props;
    const orderedFields = this.getOrderedFields(questions);
    const index = orderedFields.findIndex((e) => e === field);
    const nField = orderedFields?.[index + 1];
    if (nField && this.referenceField[nField]) {
      this.referenceField[nField].focus();
    } else {
      Keyboard.dismiss();
    }
    if (index === orderedFields.length - 1) this.props.onEndSession?.();
  }

  shouldShow(question) {
    if (question.showIf !== undefined) {
      const { answers } = this.state;
      const show = question.showIf;

      let answer = null;
      if (!answers) return false;
      if (answers[show.field] != null || answers[show.field] !== undefined) {
        answer = answers[show.field];
      }
      if (!show.value.includes(answer)) {
        return false;
      }
    }
    return true;
  }

  render() {
    const { questions } = this.props;
    if (!questions) return null;
    const ss = { marginTop: 24 };

    return <View style={ss}>{this.listQuestions()}</View>;
  }

  listQuestions() {
    const { questions } = this.props;
    return questions.map((question, index) => {
      if (question.row) return this.renderRow(question.row, index);
      return this.renderOneQuestion(question, index);
    });
  }

  renderRow(row, index) {
    return (
      <FormRow
        key={'_' + index}
        index={index}
        questions={row}
        renderQuestion={this.renderOneQuestion.bind(this)}
      />
    );
  }

  errorMessage(field) {
    const { errors } = this.props;

    if (!errors) return null;
    const err = [errors[field]].flat();
    const arr = err.filter((e) => !!e).join('; ');

    if (arr.length === 0) return null;
    return arr;
  }

  paramFromQuestion(question) {
    const { editable, disabled } = question;
    const params = {};
    if (editable !== undefined) params.editable = editable;
    if (disabled === true) {
      params.editable = false;
      params.disabled = true;
    }
    return params;
  }

  renderOneQuestion(question, index) {
    const { answers } = this.state;
    const answer = answers ? answers[question.field] : null;
    const errorMessage = this.errorMessage(question.field);

    const params = this.paramFromQuestion(question);

    return (
      <View key={'qq' + index}>
        <FormItem
          {...params}
          autoFocus={index === 0 || index === '0_0'}
          value={answer}
          shouldShow={this.shouldShow(question)}
          key={'q' + index}
          question={question}
          answer={answer}
          errorMessage={errorMessage}
          onAnswer={this.onAnswer.bind(this)}
          onChangeAnswer={this.onChangeAnswer.bind(this)}
          refField={this.refField.bind(this)}
          nextField={this.nextField.bind(this)}
        />
      </View>
    );
  }
}
