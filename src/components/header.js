import React, { Component } from 'react';
import { ImageBackground, TouchableOpacity, View } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { getColors } from '../colors';
import { getFonts } from '../fonts';
import useVulpes from '../hooks/useVulpes';
import { Button } from './button';
import { Icon } from './icon';
import { NotificationMenu } from './notification_menu';
import { Text } from './text';
import { H2, Regular } from './typos';
import { FillSpace } from './utils';

let header = null;
let headers = null;

// o nome precisa ser diferente pois na compilação dos assets web dá conflito
if (process.env.APP_ALIAS) {
  header = require(`../../assets/images/${process.env.APP_ALIAS}/header_${process.env.APP_ALIAS}.png`);
} else {
  headers = {
    gogood: require('../../assets/images/gogood/header_gogood.png'),
    partners: require('../../assets/images/partners/header_partners.png'),
    dasa: require('../../assets/images/dasa/header_dasa.png'),
    sesi: require('../../assets/images/sesi/header_sesi.png'),
  };
}

const topHeaderContainer = {
  flexDirection: 'row',
  position: 'absolute',
  marginLeft: 8,
  marginRight: 8,
  height: 48,
  flex: 1,
  right: 0,
  left: 0,
  alignItems: 'center',
};

const BackAction = ({ backAction, children }) => {
  if (!backAction) return null;

  const backButtonStyle = {
    height: 36,
    justifyContent: 'center',
    width: 40,
    paddingLeft: 8,
  };
  return (
    <TouchableOpacity onPress={() => backAction()} style={backButtonStyle}>
      <Icon
        name={'long_arrow_left'}
        color={children ? 'singleton.black' : 'singleton.white'}
        size={22}
      />
    </TouchableOpacity>
  );
};

const AdvanceActionButton = ({ advanceAction, advanceText }) => {
  const { theme } = useVulpes();
  const colors = getColors(theme);
  const fonts = getFonts(theme);
  if (!advanceAction) return null;
  return (
    <Button color={'singleton.white'} ghost onPress={advanceAction}>
      <Text style={{ ...fonts.regular, color: colors('singleton.white') }}>
        {advanceText}
      </Text>
    </Button>
  );
};

const helpContainer = { paddingLeft: 7 };
const titleLine = { flexDirection: 'row', alignItems: 'center' };

const HeaderTitleLine = ({ title, helpAction }) => {
  if (!title) return null;

  return (
    <View style={titleLine}>
      <H2 color={'singleton.white'}>{title}</H2>
      {helpAction && (
        <TouchableOpacity onPress={helpAction} style={helpContainer}>
          <Icon name={'help'} color={'singleton.white'} />
        </TouchableOpacity>
      )}
    </View>
  );
};

const HeaderSubtitleLine = ({ subtitle }) => {
  if (!subtitle) return null;

  return <Regular color={'singleton.white'}>{subtitle}</Regular>;
};

const HeaderTitles = ({ title, subtitle, helpAction }) => {
  if (!title && !subtitle) return null;

  const style = { marginTop: 24, marginBottom: 16 };
  return (
    <View style={style}>
      <HeaderTitleLine title={title} helpAction={helpAction} />
      <HeaderSubtitleLine subtitle={subtitle} />
    </View>
  );
};

const DummyHeader = () => {
  const style = { height: 48 };
  return <View style={style} />;
};

const DummyBottom = () => {
  const style = { height: 32 };
  return <View style={style} />;
};

const ContentComponent = ({ component }) => {
  if (!component) return <DummyHeader />;

  return component;
};

class HeaderComplete extends Component {
  imageBackground() {
    if (header) return header;
    const { theme } = this.props;
    return headers[theme];
  }

  render() {
    const {
      backAction,
      menuList,
      advanceAction,
      advanceText,
      contentComponent,
      title,
      subtitle,
      helpAction,
      imageStyle,
      children,
    } = this.props;
    return (
      <View>
        <ImageBackground
          source={children ? null : this.imageBackground()}
          style={this.imageStyle()}
          imageStyle={imageStyle}
        >
          <View style={this.headerStyle()}>
            <ContentComponent component={contentComponent} />
            <HeaderTitles {...{ title, subtitle, helpAction }} />
            {children ? children : <DummyBottom />}
            <View style={topHeaderContainer}>
              <BackAction backAction={backAction} {...{ children }} />
              <FillSpace />
              <NotificationMenu menuList={menuList} />
              <AdvanceActionButton {...{ advanceAction, advanceText }} />
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }

  notchDifference() {
    if (DeviceInfo.hasNotch() || DeviceInfo.hasDynamicIsland()) return 21;
    return 0;
  }

  imageStyle() {
    return {
      minHeight: 90,
    };
  }
  headerStyle() {
    const { children } = this.props;
    return {
      marginTop: 12 + this.notchDifference(),
      ...(children ? {} : { paddingLeft: 16, paddingRight: 16 }),
    };
  }
}

export const Header = (props) => {
  const { theme } = useVulpes();
  return <HeaderComplete {...props} theme={theme} />;
};

Header.displayName = 'Header';
