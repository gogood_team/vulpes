import React from 'react';
import { View } from 'react-native';
import { Thumbnail } from './thumbnail';

const lineStyle = {
  borderBottomWidth: 1,
  borderBottomColor: '#D9D9D9',
  flex: 1,
  margin: 10,
  height: 22,
};
const Line = () => {
  return <View style={lineStyle} />;
};

const rowStyle = { flexDirection: 'row' };
const Row = ({ children }) => {
  return <View style={rowStyle}>{children}</View>;
};

const separatorThumbStyle = { borderWidth: 0, padding: 0 };

export const HeaderSeparator = ({
  cover,
  color,
  source,
  children,
  ...rest
}) => {
  return (
    <Row>
      <Thumbnail source={source} size={'medium'} style={separatorThumbStyle} />
      <Line />
    </Row>
  );
};
