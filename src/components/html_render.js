import React, { Component } from 'react';
import HTML, {
  defaultSystemFonts,
  HTMLContentModel,
  HTMLElementModel,
} from 'react-native-render-html';
import { getColors } from '../colors';
import VulpesContext from '../contexts/VulpesContext';
import { getFonts } from '../fonts';
import { separatorToLong } from '../styles/list';

const baseStyle = (theme) => {
  const colors = getColors(theme);
  const fonts = getFonts(theme);
  return {
    ...fonts.regular,
    color: colors('gray.80'),
    paddingRight: 10,
  };
};

export class HTMLRender extends Component {
  rendererProps() {
    const { onLink } = this.props;

    return {
      a: {
        onPress: onLink ? (_data, href) => onLink(href) : undefined,
      },
    };
  }

  customHTMLElementModels() {
    const { theme } = this.context;
    const fonts = getFonts(theme);
    const colors = getColors(theme);
    const strongModel = HTMLElementModel.fromCustomModel({
      tagName: 'b',
      mixedUAStyles: {
        ...fonts.regularBold,
        color: colors('gray.80'),
      },
      contentModel: HTMLContentModel.textual,
    });
    return {
      a: HTMLElementModel.fromCustomModel({
        tagName: 'a',
        mixedUAStyles: {
          ...fonts.regularBold,
          textDecorationLine: 'underline',
        },
        contentModel: HTMLContentModel.textual,
      }),
      b: strongModel,
      strong: strongModel,
    };
  }

  render() {
    const { html } = this.props;
    const { theme } = this.context;
    return (
      <HTML
        baseStyle={baseStyle(theme)}
        systemFonts={[
          'DasaSans-Regular',
          'DasaSans-Bold',
          ...defaultSystemFonts,
        ]}
        source={{ html: html }}
        style={separatorToLong}
        renderersProps={this.rendererProps()}
        customHTMLElementModels={this.customHTMLElementModels()}
      />
    );
  }
}
HTMLRender.contextType = VulpesContext;
