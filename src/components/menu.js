import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { getFonts } from '../fonts';
import useVulpes from '../hooks/useVulpes';
import getStyle from '../styles/menu';

export const Menu = (props) => {
  const style = getStyle(theme);
  const { theme } = useVulpes();
  return <View style={style.generalMenuStyle}>{props.children}</View>;
};

export const MenuItem = ({ children, selected, onPress, itemFocus }) => {
  const { theme } = useVulpes();
  const fonts = getFonts(theme);
  const style = getStyle(theme);

  return (
    <TouchableOpacity
      style={!itemFocus ? style.generalMenuItemStyle : style.focusedMenuStyle}
      onPress={onPress}
      activeOpacity={0.85}
    >
      {!itemFocus ? (
        <NormalItems children={children} selected={selected} fonts={fonts} />
      ) : (
        <FocusedItems
          children={children}
          selected={selected}
          fonts={fonts}
          theme={theme}
        />
      )}
    </TouchableOpacity>
  );
};

const NormalItems = ({ children, selected, fonts }) => {
  return React.Children.map(children, (child, i) => {
    if (React.isValidElement(child)) {
      if (
        child.type.name === 'NotificationIcon' ||
        child.type.name === 'Icon'
      ) {
        return React.cloneElement(child, {
          color: selected ? 'tabBottom.active' : 'tabBottom.inactive',
          fontStyle: selected ? fonts.menuTextBold : fonts.menuText,
          size: 18,
          style: {
            alignSelf: 'center',
            ...child.props.style,
          },
        });
      }
      return React.cloneElement(child, {
        color: selected ? 'tabBottom.active' : 'tabBottom.inactive',
        fontStyle: selected ? fonts.menuTextBold : fonts.menuText,
        size: 18,
        style: {
          alignSelf: 'center',
          ...child.props.style,
        },
      });
    }
    return child;
  });
};

const FocusedItems = ({ children, selected, fonts, theme }) => {
  const style = getStyle(theme);
  return (
    <View style={style.circularTopBorder}>
      <View style={style.adjustCircularBorder}>
        <View style={style.circularButton}>
          {React.Children.map(children, (child, i) => {
            if (React.isValidElement(child)) {
              const { componentName, displayName, name } = child.type;
              const cName = componentName || displayName || name;

              if (['Icon', 'NotificationIcon'].includes(cName)) {
                return React.cloneElement(child, {
                  color: 'singleton.white',
                  size: 18,
                  style: {
                    top: 6,
                    paddingTop: 21,
                    alignSelf: 'center',
                    ...child.props.style,
                  },
                });
              } else if (cName === 'Text') {
                return React.cloneElement(child, {
                  color: selected ? 'tabBottom.active' : 'tabBottom.inactive',
                  fontStyle: selected ? fonts.menuTextBold : fonts.menuText,
                  size: 18,
                  style: {
                    paddingTop: 22,
                    alignSelf: 'center',
                    ...child.props.style,
                  },
                });
              } else {
                return React.cloneElement(child);
              }
            }
          })}
        </View>
      </View>
    </View>
  );
};
