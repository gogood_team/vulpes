import React, { Component } from 'react';
import { SafeAreaView, View } from 'react-native';
import { DEVICE_HEIGHT } from '../utils/metrics';
import { ModalContainer } from './modal_container';
import { Regular, RegularBold } from './typos';
import VulpesContext from '../contexts/VulpesContext';
import { getColors } from '../colors';
import { Button } from './button';
import { Icon } from './icon';

const containerStyle = {
  padding: 8,
  paddingBottom: 16,
};

const seletorStyle = {
  borderRadius: 8,
  backgroundColor: 'white',
  alignItems: 'center',
};

const headerContainerStyle = (theme = 'gogood') => ({
  padding: 10,
  width: '100%',
  alignItems: 'center',
  borderBottomWidth: 0.25,
  borderBottomColor: getColors(theme)('main.regular'),
});

const cancelBtnStyle = {
  alignSelf: 'flex-end',
};

const optionsStyle = { maxHeight: DEVICE_HEIGHT * 0.4, width: '100%' };

export class ModalPicker extends Component {
  render() {
    const { children, onClose, headerText } = this.props;
    return (
      <ModalContainer {...this.props} style={[containerStyle]}>
        <SafeAreaView>
          <Button
            onPress={onClose}
            color={'singleton.white'}
            ghost
            style={cancelBtnStyle}
          >
            <Regular>Fechar</Regular>
            <Icon name="close" size={14} />
          </Button>

          <View style={seletorStyle}>
            <View style={headerContainerStyle(this.context.theme)}>
              <RegularBold>{headerText}</RegularBold>
            </View>
            <View style={optionsStyle}>{children}</View>
          </View>
        </SafeAreaView>
      </ModalContainer>
    );
  }
}
ModalPicker.contextType = VulpesContext;
