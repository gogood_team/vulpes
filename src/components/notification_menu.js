import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { iconList } from 'react-native-icomoon';
import json from '../../assets/icons/selection.json';
import { NotificationIcon } from './notification_icon';

const notificationMenuSeparationSpace = {
  padding: 8,
  justifyContent: 'center',
};
export const MenuItem = ({ item, color, markerColor, size }) => {
  return (
    <TouchableOpacity
      onPress={() => item.action()}
      style={notificationMenuSeparationSpace}
    >
      {item.component ? (
        item.component
      ) : (
        <NotificationIcon
          name={item.icon}
          showMarker={item.showMarker}
          color={color}
          size={size}
          markerColor={markerColor}
        />
      )}
    </TouchableOpacity>
  );
};

export const NotificationMenu = ({
  menuList,
  size,
  color,
  markerColor,
  style,
  ...restProps
}) => {
  const notificationMenuCompleteStyle = {
    ...style,
    flexDirection: 'row',
    gap: 8,
    justifyContent: 'center',
  };
  if (!menuList) return null;
  return (
    <View {...restProps} style={notificationMenuCompleteStyle}>
      {menuList.map((item, index) => {
        return (
          <MenuItem
            key={'mItem' + index}
            item={item}
            size={size}
            color={color}
            markerColor={markerColor}
          />
        );
      })}
    </View>
  );
};

export const listOfIcons = () => {
  return iconList(json);
};
