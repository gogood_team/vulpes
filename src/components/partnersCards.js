import React from 'react';
import { Image, TouchableOpacity, View, FlatList } from 'react-native';
import { getColors } from '../colors';
import useVulpes from '../hooks/useVulpes';
import getStyle from '../styles/card';
import { Icon } from './icon';
import { Thumbnail } from './thumbnail';
import { Regular, RegularBold, H3, H2, H4 } from './typos';
import {
  CardTag,
  Visitor,
  qrCodeCardPartner,
  VerticalCardSeparator,
} from '../utils/cardElements';
import { Card } from './card';
//import { Button } from './button';

export const PartnersMiniGymCard = ({
  tagTextColor,
  tagColor = 'primary.100',
  tagText = null,
  tagIcon = null,
  props,
  source,
  children,
  onPress = null,
}) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  return (
    <PartnersCard
      cardContainer={style.PartnersMiniGymCardContainer}
      {...props}
      onPress={onPress}
    >
      <View style={style.outerMiniCardStyle}>
        <Thumbnail source={source} size={'small'} />
        <View style={style.PartnersMiniGymCardContentStyle}>
          {children}
          {(tagText || tagIcon) && (
            <View style={style.PartnersMiniGymCardTag}>
              <CardTag
                textColor={tagTextColor}
                color={tagColor}
                text={tagText}
                icon={tagIcon}
              />
            </View>
          )}
        </View>
      </View>
    </PartnersCard>
  );
};

export const PartnersVisitorInfoCard = ({
  tagTextColor,
  tagText = null,
  tagText2 = null,
  tagIcon = null,
  tagIcon2 = null,
  props,
  source,
  titleName,
  datetime,
  value,
  onPress = null,
}) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  const valueStyle = { marginLeft: 8 };
  return (
    <PartnersCard
      cardContainer={style.PartnersVisitorInfoCardContainer}
      onPress={onPress}
      {...props}
    >
      <View style={style.PartnersOuterMiniReportStyle}>
        <Thumbnail source={source} size={'small'} />
        <View style={style.PartnersMiniReportContentStyle}>
          <View>
            <Regular>{titleName}</Regular>
            <Regular>{datetime}</Regular>
          </View>
          <H3 style={valueStyle}>R${value}</H3>
        </View>
      </View>
      {tagText || tagIcon ? (
        <View style={style.PartnersMiniReportTagStyle}>
          <CardTag
            textColor={tagTextColor}
            color={'gray.60'}
            text={tagText}
            icon={tagIcon}
          />
          {tagText2 || tagIcon2 ? (
            <View style={style.PartnersSecondTagView}>
              <CardTag
                textColor={tagTextColor}
                color={'gray.100'}
                text={tagText2}
                icon={tagIcon2}
              />
            </View>
          ) : null}
        </View>
      ) : null}
    </PartnersCard>
  );
};
export const PartnersChatCard = ({
  props,
  source,
  titleName,
  datetime,
  lastMessage,
  onPress = null,
}) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  const textWidth = { width: '90%' };
  const partnersThumbTitleStyle = { flexDirection: 'row' };
  if (!lastMessage) return null;
  return (
    <PartnersCard
      cardContainer={style.PartnersChatCardContainer}
      {...props}
      onPress={onPress}
    >
      <View style={partnersThumbTitleStyle}>
        <Thumbnail source={source} size={'small'} />
        <View style={style.PartnersMiniReportContentStyle}>
          <View>
            <RegularBold>{titleName}</RegularBold>
            <Regular>{datetime}</Regular>
          </View>
        </View>
      </View>
      <View style={style.PartnersChatTextStyle}>
        <Regular numberOfLines={1} style={textWidth}>
          {lastMessage}
        </Regular>
        <View style={style.PartnersChatCardAlertCircle} />
      </View>
    </PartnersCard>
  );
};

export const PartnersAgenda = ({ props, dayMonth, dayName, timeArray }) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);

  return (
    <View style={style.PartnersAgendaContentStyle} {...props}>
      <View style={style.PartnersAgendaFirstCardStyle}>
        <RegularBold>{dayMonth}</RegularBold>
        <Regular>{dayName}</Regular>
      </View>
      <VerticalCardSeparator />
      <AgendaTimeDetails timeArray={timeArray} />
    </View>
  );
};

const renderAgendaItem = ({ item }) => {
  const marginTimeCards = { marginBottom: 8 };
  const maxDescriptionToWrap = { maxWidth: 100 };
  return (
    <PartnersCard style={marginTimeCards} onPress={item.onPress}>
      <RegularBold>{item.time}</RegularBold>
      <Regular style={maxDescriptionToWrap}>{item.description}</Regular>
    </PartnersCard>
  );
};

const AgendaTimeDetails = ({
  timeArray,
  noEventText = 'Agendamento indisponível',
}) => {
  const secondCardsColumn = {
    flexDirection: 'column',
  };

  if (!timeArray) return <AgendaTimeUnavailable noEventText={noEventText} />;
  return (
    <View style={secondCardsColumn}>
      <FlatList
        data={timeArray}
        renderItem={renderAgendaItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
};

const AgendaTimeUnavailable = ({ noEventText }) => {
  const marginTimeCards = { marginBottom: 8 };
  return (
    <View>
      <PartnersCard style={marginTimeCards}>
        <Regular>{noEventText}</Regular>
      </PartnersCard>
    </View>
  );
};

export const PartnersMiniReportCard = ({
  tagTextColor,
  tagColor = 'primary.100',
  tagText = null,
  tagIcon = null,
  props,
  source,
  titleName,
  datetime,
  value,
  onPress = null,
  children = null,
}) => {
  const { theme } = useVulpes();
  const styles = getStyle(theme);
  const tagProps = { tagTextColor, tagColor, tagText, tagIcon };
  return (
    <PartnersCard
      cardContainer={styles.PartnersMiniReportCardContainer}
      onPress={onPress}
      {...props}
    >
      <View style={styles.PartnersOuterMiniReportStyle}>
        <Thumbnail reportCard size={'small'} />
        <View style={styles.PartnersMiniReportContentStyle}>
          <View style={styles.PartnersMiniReportTextStyle}>
            <Regular>{titleName}</Regular>
            <Regular>{datetime}</Regular>
          </View>
          <MiniReportValueAndTag
            tagProps={tagProps}
            value={value}
            styles={styles}
          />
        </View>
      </View>
      {children}
    </PartnersCard>
  );
};
const MiniReportValueAndTag = ({ tagProps, styles, value }) => {
  const { tagTextColor, tagColor, tagText, tagIcon } = tagProps;
  const toCurrency = (param) => `R$ ${param.toFixed(2).replace('.', ',')}`;
  return (
    <View style={styles.PartnersMiniReportCard}>
      {value ? <H3>{toCurrency(value)}</H3> : null}
      {tagText || tagIcon ? (
        <View style={styles.PartnersMiniReportTagStyle}>
          <CardTag
            textColor={tagTextColor}
            color={tagColor}
            text={tagText}
            icon={tagIcon}
          />
        </View>
      ) : null}
    </View>
  );
};

export const PartnersMiniRecentVisitorsCard = ({ data, style, ...rest }) => {
  const { theme } = useVulpes();
  const themeStyle = getStyle(theme);
  const onRow = { flexDirection: 'row' };

  return (
    <PartnersCard
      cardContainer={{
        ...themeStyle.PartnersMiniRecentVisitorsContainer,
        ...style,
      }}
      {...rest}
    >
      <View style={onRow}>
        {(!data || data.length === 0) && rest.ListEmptyComponent}
        {data &&
          data.map((item, index) => {
            const margin = index < data.length - 1 ? 8 : 0;
            const svContainer = { flex: 1, marginRight: margin };
            return (
              <View style={svContainer} key={'v' + index}>
                <Visitor source={item.source} visitorName={item.visitorName} />
              </View>
            );
          })}
      </View>
    </PartnersCard>
  );
};

export const PartnersStatsMiniCard = ({
  data1,
  data2,
  props,
  color = 'primary.100',
  onPress = null,
}) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  const colors = getColors(theme);
  return (
    <Card
      cardContainer={style.PartnersStatsCardContainer}
      color={'singleton.transparent'}
      onPress={onPress}
      {...props}
    >
      <View style={style.outerMiniCardStyle}>
        <StatsExhibit
          style={{
            ...style.PartnersStatsLeftSide,
            backgroundColor: colors(color),
          }}
          data={data1}
          side={'left'}
        />
        <StatsExhibit
          style={style.PartnersStatsRightSide}
          data={data2}
          side={'right'}
        />
      </View>
    </Card>
  );
};

const StatsExhibit = ({ style, data, side = 'left' }) => {
  const { theme } = useVulpes();
  if (!data) return null;
  const styles = getStyle(theme);
  const percentageArrow = data.percentage > 0 ? 'arrow_up' : 'arrow_down';
  const percentageColor = data.percentage > 0 ? 'success.100' : 'error.100';
  const fontColor = side === 'left' ? 'singleton.white' : 'gray.100';
  return (
    <View style={style}>
      <H4 color={fontColor}>{data.title}</H4>
      <H2 color={fontColor}>{data.value}</H2>
      {data.percentage ? (
        <View style={styles.percentageStyle}>
          <RegularBold color={fontColor}>{data.percentage}%</RegularBold>
          <Icon
            name={percentageArrow}
            color={side === 'left' ? fontColor : percentageColor}
            size={14}
            style={styles.arrowIconStyle}
          />
        </View>
      ) : null}
    </View>
  );
};

export const PartnersFixedNotificationCard = ({
  tagTextColor,
  tagColor,
  tagText,
  tagText2 = null,
  tagIcon = null,
  tagIcon2 = null,
  props,
  children,
  onPress = null,
}) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  return (
    <PartnersCard
      cardContainer={style.PartnersFixedNotificationCardContainer}
      onPress={onPress}
      {...props}
    >
      {(tagText || tagIcon) && (
        <View style={style.PartnersTagsView}>
          {tagText ? (
            <CardTag
              textColor={tagTextColor}
              color={tagColor}
              text={tagText}
              icon={tagIcon}
              marginBottom={true}
            />
          ) : null}
          {tagText2 ? (
            <View style={style.PartnersSecondTagView}>
              <CardTag
                textColor={tagTextColor}
                text={tagText2}
                icon={tagIcon2}
                marginBottom={true}
              />
            </View>
          ) : null}
        </View>
      )}
      <View style={style.outerMiniCardStyle}>{children}</View>
    </PartnersCard>
  );
};

export const PartnersCard = ({ children, innerMargin, ...props }) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  return (
    <Card
      cardContainer={{ ...style.PartnersCardContainer, overflow: 'visible' }}
      color={'singleton.transparent'}
      {...props}
    >
      <View
        style={{
          ...style.PartnersCardOuterStyle,
          marginLeft: innerMargin,
          marginRight: innerMargin,
        }}
      >
        {children}
      </View>
    </Card>
  );
};

export const PartnersQrCodeCard = ({
  color = 'primary.100',
  onPress,
  style: bStyle,
  title,
  source = null,
}) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  const colors = getColors(theme);

  const OuterComp = onPress ? TouchableOpacity : View;

  const cardHeight = 252;
  let width = 272;

  let imageStyle = {
    height: cardHeight * 0.6,
    width: width * 0.6,
    margin: 20,
    marginRight: 24,
    resizeMode: 'contain',
  };
  return (
    <OuterComp onPress={onPress} style={bStyle}>
      <View
        style={[
          style.bannerCardGradient,
          { height: cardHeight, width, backgroundColor: colors(color) },
        ]}
      >
        <View style={style.outerViewQrCodeCard}>
          <View style={style.imageInQrCodeCard}>
            <Image
              source={source ? source : qrCodeCardPartner}
              style={imageStyle}
            />
          </View>

          <View style={style.textsViewQrCodeCard}>
            <H3 color="singleton.white" style={style.titleTextQrCodeCard}>
              {title}
            </H3>
          </View>
        </View>
      </View>
    </OuterComp>
  );
};
