import React from 'react';
import { View } from 'react-native';
import { PartnersLineChartStyles as styles } from './styles';

export const GraphView = (props) => (
  <View style={{ ...styles.graphView(props.width) }}>{props.children}</View>
);
