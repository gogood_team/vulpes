import React from 'react';
import { PartnersLineChartStyles as styles } from './styles';
import styled from 'styled-components';

const GraphViewDiv = styled.div`
  svg {
    height: 100%;
  }
`;

export const GraphView = (props) => {
  return (
    <GraphViewDiv style={{ ...styles.graphView(props.width) }}>
      {props.children}
    </GraphViewDiv>
  );
};
