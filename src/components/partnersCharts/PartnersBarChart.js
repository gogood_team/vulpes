import React from 'react';
import { Icon } from './../icon';
import { View } from 'react-native';
import { BarChart } from 'react-native-gifted-charts';
import { getColors } from '../../colors';
import { Small } from '../typos';
import VulpesContext from '../../contexts/VulpesContext';
import { PartnersBarChartStyles } from './styles';
import { getFonts } from '../../fonts';

const InfoRectangle = ({ item, barw, colors }) => {
  const currencyValue = `R$ ${item.value.toFixed(2).replace('.', ',')}`;
  const { outerStyle, valueStyle, circleStyle, textIconStyle, iconStyle } =
    PartnersBarChartStyles;

  return (
    <View style={outerStyle(currencyValue.length, barw)}>
      <View style={valueStyle(colors)}>
        <View style={textIconStyle}>
          <Small style={{ color: colors('primary.100') }}>
            {currencyValue}
          </Small>
          <Icon
            name="arrow_up"
            color={'primary.100'}
            size={16}
            style={iconStyle}
          />
        </View>
      </View>
      <View style={circleStyle(colors)} />
    </View>
  );
};

const PartnersBarChartWrapper = ({ theme, props }) => {
  const colors = getColors(theme);
  let { data, width } = props;
  const { barChartViewStyle } = PartnersBarChartStyles;
  //  const firstCurrencyValue = `R$ ${data[0].value.toFixed(2).replace('.', ',')}`;

  let maxValue = 0;
  for (const item of data)
    maxValue = item.value > maxValue ? item.value : maxValue;

  // const [initialSpacing, setinitialSpacing] = useState(
  //   firstCurrencyValue.length * 4
  // );
  const fonts = getFonts(theme);

  width = width - 20;

  const barw = (width - 55) / (data.length * 2 - 1);

  return (
    <View style={barChartViewStyle}>
      <BarChart
        maxValue={maxValue}
        height={135}
        yAxisLabelWidth={26}
        noOfSections={5}
        data={data}
        barWidth={barw}
        width={props.width - 55}
        yAxisTextStyle={fonts.menuText}
        xAxisLabelTextStyle={xAxisLabelTextStyleConst(fonts)}
        spacing={barw}
        initialSpacing={6}
        rulesType="solid"
        rulesThickness={1}
        yAxisThickness={0}
        frontColor={colors('primary.100')}
        renderTooltip={(item, index) => {
          const last = index === data.length - 1;
          return (
            <InfoRectangle
              last={last}
              barw={barw}
              item={item}
              colors={colors}
            />
          );
        }}
      />
    </View>
  );
};

export const PartnersBarChart = (props) => {
  return (
    <VulpesContext.Consumer>
      {({ theme }) => <PartnersBarChartWrapper theme={theme} props={props} />}
    </VulpesContext.Consumer>
  );
};
function xAxisLabelTextStyleConst(fonts) {
  return { ...fonts.menuText, marginTop: -15 };
}
