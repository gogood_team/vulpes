import React, { Component } from 'react';
import { Platform, View } from 'react-native';
import { LineChart as LChart } from 'react-native-gifted-charts';
import { Circle, ForeignObject, Rect } from 'react-native-svg';
import { getColors } from '../../colors';
import VulpesContext from '../../contexts/VulpesContext';
import { Icon } from './../icon';
import { H3, Regular, RegularBold } from './../typos';
import { GraphView } from './GraphView';
import { PartnersLineChartStyles as styles } from './styles';

const shouldShowLabel = (index, size, label) => {
  //  console.log(size,index);
  if (label.length === 1) return true;
  if (size <= 7) return true;
  if (index === size - 1) return false;
  const div = Math.floor(size / 4);
  if (index % div === 1) return true;
  return false;
};

const xAxisStyle = {
  width: 50,
  textAlign: 'center',
  color: 'rgba(96, 96, 96, 1)',
  fontFamily: 'Open Sans',
  fontWeight: '400',
  fontSize: 12,
};

function labelTooltipStyle(colors) {
  return {
    color: colors('primary.100'),
    width: 136,
    textAlign: 'center',
    paddingTop: 2,
    display: 'block',
  };
}

export class PartnersLineChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.valuesToSet(props.data.length - 1),
      notSet: true,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data) {
      this.setState(this.valuesToSet(this.props.data.length - 1));
    }
  }

  valuesToSet(index) {
    const { data } = this.props;
    if (index <= 0)
      return {
        currentValue: data[index],
        currentIncreaseState: '',
        icon: 'less',
        index: index,
        notSet: false,
      };
    let increaseState;
    let icon;
    if (data[index] < data[index - 1]) {
      increaseState = 'Queda';
      icon = 'arrow_down';
    } else if (data[index] > data[index - 1]) {
      increaseState = 'Crescimento';
      icon = 'arrow_up';
    } else {
      increaseState = 'Estável';
      icon = 'less';
    }
    return {
      index: index,
      currentValue: data[index],
      currentIncreaseState: increaseState,
      icon: icon,
      notSet: false,
    };
  }

  changeValue = (_item, index) => {
    this.setState(this.valuesToSet(index));
  };

  get width() {
    if (this.props.width < 30) return 40;
    return this.props.width + 10;
  }

  bottomLabel() {
    const { description, currencyData } = this.props;
    const currency = currencyData ? 'R$ ' : '';
    return (
      <View style={styles.bottomLabelView(this.width)}>
        <View style={styles.valueView}>
          <H3>
            {currency + this.state.currentValue.toFixed(2).replace('.', ',')}
          </H3>
          <Regular>{description}</Regular>
        </View>
        <View style={styles.increaseView}>
          <RegularBold color={'primary.100'}>
            {this.state.currentIncreaseState}
          </RegularBold>
          {this.state.icon && (
            <Icon
              name={this.state.icon}
              color={'primary.100'}
              size={16}
              style={styles.icon}
            />
          )}
        </View>
      </View>
    );
  }

  parseData() {
    const { data, labels } = this.props;
    const maxValue = Math.max.apply(null, data);
    const minValue = Math.min.apply(null, data);
    const dataParsed = [];
    for (let index in data)
      dataParsed.push({
        value: data[index],
        label: shouldShowLabel(index, data.length, labels[index])
          ? labels[index]
          : null,
        dataPointRadius: -1,
      });

    return { maxValue, minValue, dataParsed };
  }

  xLabelPosition() {
    const { data } = this.props;
    const posdiff = this.state.index / (data.length - 1);
    if (posdiff <= 0.15) return -10;
    if (posdiff >= 0.85) return -120;
    return -68;
  }

  yLabelPosition(maxValue, minValue) {
    const diff = maxValue - minValue;
    const diffCurrent = this.state.currentValue - minValue;
    if (diffCurrent / diff >= 0.6) return 20;
    return -24;
  }

  render() {
    const { data, longLabels, labels, height, style } = this.props;

    console.log(labels);

    const { width } = this;
    if (!data || data.length === 0) {
      console.error('PartnersLineChart:: No data defined');
      return null;
    }
    const { maxValue, minValue, dataParsed } = this.parseData();
    const { theme } = this.context;
    const colors = getColors(theme);

    const diff = Math.max((maxValue - minValue) / 2, 2);
    const lowerBound = minValue - diff;
    const upperBound = maxValue + diff;

    return (
      <View
        style={{
          ...styles.chartView(width),
          height: height,
          ...style,
        }}
      >
        <GraphView width={width}>
          <LChart
            data={dataParsed}
            pressEnabled
            initialIndex={data.length - 1}
            width={width || 300}
            height={55}
            focusEnabled={true}
            disableScroll={true}
            areaChart
            hideRules
            hideYAxisText
            hideXAxisText
            curved
            adjustToWidth
            color={colors('primary.100')}
            thickness={3.5}
            startFillColor={colors('primary.100')}
            endFillColor={colors('singleton.white')}
            startOpacity={0.4}
            endOpacity={0}
            unFocusOnPressOut={false}
            initialSpacing={Platform.OS === 'web' ? 16 : 32}
            yAxisThickness={0}
            yAxisLabelWidth={1}
            xAxisLabelTextStyle={xAxisStyle}
            xAxisThickness={0}
            xAxisColor={'rgba(232, 232, 232, 1)'}
            focusedDataPointWidth={3}
            focusedDataPointRadius={6}
            focusedDataPointHeight={3}
            labelsExtraHeight={40}
            maxValue={upperBound}
            minValue={lowerBound}
            xAxisLength={width - (Platform.OS === 'web' ? 16 : 32) || 300}
            spacing={(width - 30) / (data.length - 1)}
            onFocus={(item, index) => {
              this.changeValue(item, index);
            }}
            focusedCustomDataPoint={() => (
              <>
                <Circle
                  r={6}
                  cx={3}
                  cy={10}
                  stroke={'#FFFFFF'}
                  strokeWidth={2.7}
                  fill={colors('primary.100')}
                />
                {!this.state.notSet && (
                  <Rect
                    rx={12}
                    ry={12}
                    width={136}
                    height={24}
                    fill={colors('primary.40')}
                    x={this.xLabelPosition()}
                    y={this.yLabelPosition(maxValue, minValue)}
                  />
                )}
                {!this.state.notSet && (
                  <ForeignObject
                    x={this.xLabelPosition()}
                    y={this.yLabelPosition(maxValue, minValue)}
                    width={136}
                    height={24}
                  >
                    <RegularBold style={labelTooltipStyle(colors)}>
                      {longLabels[this.state.index]}
                    </RegularBold>
                  </ForeignObject>
                )}
              </>
            )}
          />
        </GraphView>
        {this.bottomLabel()}
      </View>
    );
  }
}

PartnersLineChart.contextType = VulpesContext;
