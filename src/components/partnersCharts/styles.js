import { getColors } from '../../colors';

export const PartnersLineChartStyles = {
  valueView: {
    flexDirection: 'column',
    marginLeft: 6,
    marginTop: 4,
  },

  increaseView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 0,
    marginTop: 6,
  },

  icon: { marginLeft: 2 },

  graphView: (width) => {
    return {
      width: width,
      alignItems: 'center',
      overflow: 'hidden',
    };
  },

  bottomLabelView: (widthProps) => {
    return {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'flex-start',
      width: widthProps * 0.95,
      transform: [{ translateY: -40 }],
      marginBottom: -30,
    };
  },

  chartView: (widthProps) => {
    return {
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: -16,
      marginLeft: -16,
      width: widthProps || 300,
      overflow: 'visible',
    };
  },

  focusedDataPoint: (theme) => {
    const colors = getColors(theme);
    return {
      width: 20,
      height: 20,
      marginTop: 20,
      backgroundColor: colors('primary.100'),
      borderWidth: 3,
      borderRadius: 20,
      borderColor: colors('singleton.white'),
      elevation: 4,
      shadowColor: colors('singleton.default'),
    };
  },
};

export const PartnersBarChartStyles = {
  iconStyle: { marginLeft: 4 },

  textIconStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  barChartViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'visible',
    height: 160,
  },

  outerStyle: (stringLength, barw) => {
    const translate = -55.5 + barw / 2.5;
    return {
      flexDirection: 'column',
      width: 100,
      alignItems: 'center',
      transform: [{ translateX: translate }],
    };
  },

  valueStyle: (colors) => {
    return {
      justifyContent: 'center',
      transform: [{ translateY: 12 }],
      backgroundColor: colors('singleton.white'),
      borderColor: colors('primary.100'),
      borderWidth: 2,
      borderRadius: 4,
      padding: 4,
    };
  },
  circleStyle: (colors) => {
    return {
      marginLeft: 5,
      width: 16,
      height: 16,
      bottom: -8,
      backgroundColor: colors('primary.100'),
      borderWidth: 3,
      borderRadius: 50,
      borderColor: colors('singleton.white'),
      elevation: 4,
      shadowColor: colors('singleton.default'),
    };
  },
};
