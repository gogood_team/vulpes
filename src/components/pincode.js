import React, { Component } from 'react';
import { TextInput, View } from 'react-native';
import { getColors } from 'react-native-vulpes';
import VulpesContext from '../contexts/VulpesContext';

const MAX_CELLS = 6;
export class PinCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: new Array(MAX_CELLS).fill(''),
    };
    this.inputsRef = [];
  }

  focusNext(index) {
    if (index < 5) {
      this.inputsRef[index + 1].focus();
    }
  }

  focusPrev(index) {
    if (index > 0) {
      this.inputsRef[index - 1].focus();
    }
  }

  handleMultiInput(text, index) {
    const newCode = [...this.state.code];

    const pasteCode = text.slice(0, MAX_CELLS);

    pasteCode.split('').forEach((char, i) => {
      newCode[index + i] = char;
    });

    this.setState({ code: newCode }, () => {
      const nextIndex = index + pasteCode.length - 1;
      if (nextIndex < MAX_CELLS) {
        this.inputsRef[nextIndex].focus();
      }
      this.checkCodeComplete();
    });
    return;
  }

  handleSingleInput(text, index) {
    const newCode = [...this.state.code];
    newCode[index] = text;
    this.setState({ code: newCode }, () => {
      if (text.length === 1 && index < 5) {
        this.focusNext(index);
      }
      this.checkCodeComplete();
    });
  }

  handleInput(text, index) {
    if (text.length > 1) {
      return this.handleMultiInput(text, index);
    }
    return this.handleSingleInput(text, index);
  }

  checkCodeComplete() {
    const { code } = this.state;
    const isComplete = code.every((digit) => digit !== '');
    const { onChange } = this.props;
    return onChange && onChange(code, isComplete);
  }

  onKeyPress(index, { nativeEvent }) {
    if (nativeEvent.key === 'Backspace') {
      const newCode = [...this.state.code];
      if (newCode[index] === '') {
        this.focusPrev(index);
        return;
      }
      newCode[index] = '';
      this.setState({ code: newCode }, () => {
        this.focusPrev(index);
      });
    }
  }

  inputStyle(digit) {
    const { theme } = this.context;
    const colors = getColors(theme);
    const borderColor = colors(digit === '' ? 'main.regular' : 'gray.40');
    return {
      width: 42,
      height: 48,
      borderBottomWidth: 2,
      borderBottomColor: borderColor,
      fontSize: 26,
      textAlign: 'center',
      paddingHorizontal: 10,
      fontWeight: 'bold',
    };
  }

  containerStyle() {
    return {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginLeft: 26,
      marginRight: 26,
    };
  }

  render() {
    return (
      <View style={this.containerStyle()}>
        {this.state.code.map((digit, index) => (
          <TextInput
            key={index}
            style={this.inputStyle(digit)}
            value={digit}
            onChangeText={(text) => this.handleInput(text, index)}
            onKeyPress={(e) => this.onKeyPress(index, e)}
            ref={(ref) => {
              this.inputsRef[index] = ref;
            }}
            maxLength={MAX_CELLS - index}
            keyboardType={'number-pad'}
            returnKeyType={'done'}
          />
        ))}
      </View>
    );
  }
}
PinCode.contextType = VulpesContext;
