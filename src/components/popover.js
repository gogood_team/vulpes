import React, { useState } from 'react';
import { View } from 'react-native';
import { getColors } from '../colors';
import { Icon } from './icon';
import { Button } from './button';
import { RegularBold } from './typos';
import useVulpes from '../hooks/useVulpes';
import { IDGenerator } from '../utils/IDGenerator';

const outerContainerStyle = (active) => ({
  zIndex: active ? 100 : 10,
});

const containerStyle = (theme) => {
  const colors = getColors(theme);
  return {
    width: 264,
    minHeight: 74,
    borderRadius: 10,
    backgroundColor: colors('singleton.white'),
    borderColor: colors('gray.40'),
    borderWidth: 1,
    position: 'absolute',
    left: 0,
    top: 0,
  };
};

const contentStyle = () => ({
  paddingLeft: 17,
  paddingRight: 17,
  paddingBottom: 17,
  flexDirection: 'column',
});

export const Popover = ({ label, color, children, style }) => {
  const [id] = useState(IDGenerator.getInstance().getId());

  const { theme, activePopover, setActivePopover } = useVulpes();

  if (!children) return null;

  const isActive = id === activePopover;

  return (
    <View style={outerContainerStyle(isActive)}>
      <RegularBold color={color} onPress={() => setActivePopover(id)}>
        {label}
      </RegularBold>
      {isActive ? (
        <View style={{ ...containerStyle(theme), ...(style || {}) }}>
          <View>
            <Button
              ghost
              color={'gray.100'}
              onPress={() => {
                setActivePopover(null);
              }}
            >
              <Icon size={14} name={'close'} />
            </Button>
          </View>
          <View style={contentStyle()}>{children}</View>
        </View>
      ) : null}
    </View>
  );
};
