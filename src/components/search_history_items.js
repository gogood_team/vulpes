import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { getColors } from '../colors';
import { Icon } from './icon';
import useVulpes from '../hooks/useVulpes';
import { H3, Regular } from './typos';
import { Button } from './button';

const Separator = () => {
  const { theme } = useVulpes();
  const colors = getColors(theme);
  const separatorStyle = {
    marginRight: 5,
    marginTop: 8,
    borderBottomWidth: 1,
    borderBottomColor: colors('gray.40'),
    flexDirection: 'row',
  };
  return <View style={separatorStyle} />;
};

const HistoryItem = ({ searchItems, onPress }) => {
  const { theme } = useVulpes();
  const colors = getColors(theme);
  const itemStyle = { flexDirection: 'row', marginTop: 10 };
  const iconStyle = { paddingTop: 3, marginRight: 10 };
  return (
    <View>
      {searchItems
        .slice(-3)
        .reverse()
        .map((item, index) => (
          <View key={`${item}${index}`}>
            <TouchableOpacity style={itemStyle} onPress={() => onPress(item)}>
              <Icon
                name={'clock'}
                size={14}
                color={colors('gray.100')}
                style={iconStyle}
              />
              <Regular>{item}</Regular>
            </TouchableOpacity>
            <Separator />
          </View>
        ))}
    </View>
  );
};

export const SearchHistoryItems = ({
  title,
  searchItems,
  onPress = null,
  cleanHistoryFunction = null,
  ...props
}) => {
  const { theme } = useVulpes();
  const colors = getColors(theme);
  if (
    !searchItems ||
    searchItems.length < 1 ||
    !onPress ||
    !cleanHistoryFunction
  )
    return null;
  const container = { marginTop: 32 };
  const titleContainer = {
    flexDirection: 'row',
    justifyContent: 'space-between',
  };
  const cleanStyle = {
    textDecorationLine: 'underline',
    color: colors('gray.60'),
    paddingBottom: 7,
  };
  const buttonStyle = { paddingBottom: 15 };
  return (
    <View style={container} {...props}>
      <View style={titleContainer}>
        <H3>{title}</H3>
        <Button ghost style={buttonStyle} onPress={cleanHistoryFunction}>
          <Regular style={cleanStyle}>Limpar</Regular>
        </Button>
      </View>
      <HistoryItem searchItems={searchItems} onPress={onPress} />
    </View>
  );
};
