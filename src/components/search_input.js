import React, { Component } from 'react';
import {
  Platform,
  TextInput as Input,
  TouchableOpacity,
  View,
} from 'react-native';
import { getColors } from '../colors';
import VulpesContext from '../contexts/VulpesContext';
import { getFonts } from '../fonts';
import styles from '../styles/search_input';
import { Icon } from './icon';
import { Small } from './typos';

export class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placeholder: !props.value || props.value.length === 0,
      focused: false,
      iconColor: 'searchInput.text',
    };
    this.field = null;
  }

  focus() {
    this.field && this.field.focus();
  }

  handleReference(field) {
    this.field = field;
  }

  handleChange(text) {
    this.setState({
      placeholder: text.length === 0,
    });
    this.setIconColor(text.length > 0);
    this.props.onChangeText && this.props.onChangeText(text);
  }

  handleFocus() {
    this.setState({ focused: true });
    this.props.onFocus && this.props.onFocus();
  }

  handleBlur() {
    this.setState({ focused: false });
    this.props.onBlur && this.props.onBlur();
  }

  render() {
    const {
      error,
      placeholder,
      onChangeText,
      inputStyle,
      onPress,
      onBlur,
      onFocus,
      allowSend,
      value,
      iconName = 'search',
      disableIcon = false,
      ...rest
    } = this.props;

    const touchStyle = { justifyContent: 'center' };
    return (
      <View>
        <View style={this.completeOuterStyle()}>
          <Input
            allowFontScaling={false}
            ref={this.handleReference.bind(this)}
            placeholder={placeholder}
            placeholderTextColor={this.fontStyle().color}
            onFocus={this.handleFocus.bind(this)}
            onBlur={this.handleBlur.bind(this)}
            {...rest}
            style={this.completeStyle()}
            value={this.props.value}
            onSubmitEditing={this.onSend.bind(this)}
            onChangeText={this.handleChange.bind(this)}
          />
          <TouchableOpacity
            style={touchStyle}
            disabled={disableIcon}
            onPress={allowSend ? () => this.onSend() : null}
          >
            <Icon
              name={iconName}
              color={this.state.iconColor}
              size={16}
              style={this.iconStyle()}
            />
          </TouchableOpacity>
        </View>
        {error && (
          <Small style={this.errorStyle()} color={'error.100'}>
            {error}
          </Small>
        )}
      </View>
    );
  }

  setIconColor(allowSend) {
    const { theme } = this.context;
    allowSend
      ? this.setState({ iconColor: styles(theme).iconColor })
      : this.setState({ iconColor: 'gray.100' });
  }

  shouldShowTouchableOpacity() {
    const { theme } = this.context;
    return styles(theme).showTouchableOpacity;
  }

  onSend() {
    const { allowSend, onFinish } = this.props;
    allowSend && onFinish && onFinish();
  }

  okStyle() {
    return { marginTop: 2 };
  }

  errorStyle() {
    const { theme } = this.context;
    const style = styles(theme);
    return {
      paddingLeft: style.view.paddingLeft,
      marginTop: 8,
    };
  }

  systemOutline() {
    if (Platform.OS !== 'web') return {};
    return {
      outline: 'none',
    };
  }
  showIconPosition() {
    const { theme } = this.context;
    const style = styles(theme);
    return style.iconPositionLeft;
  }
  iconStyle() {
    return {
      marginLeft: 8,
      marginRight: 8,
    };
  }

  completeOuterStyle() {
    const { style } = this.props;
    const { theme } = this.context;
    const viewStyle = styles(theme);
    return {
      ...this.colorOutline(),
      borderWidth: this.widthOutline(),
      height: 44,
      marginTop: 2,
      paddingRight: 16,
      flexDirection: 'row',
      ...style,
      ...viewStyle.view,
    };
  }

  completeStyle() {
    const { inputStyle } = this.props;

    return {
      ...this.fontStyle(),
      ...this.systemOutline(),
      flex: 1,
      ...inputStyle,
    };
  }

  fontStyle() {
    const { theme } = this.context;
    const colors = getColors(theme);
    const fonts = getFonts(theme);
    let font = fonts.regular;

    if (this.state.placeholder && !this.props.value) font = fonts.regular;
    if (this.props.error) font = { ...font, color: colors('error.100') };
    if (this.props.noResult) font = { ...font, color: colors('error.100') };
    return {
      ...font,
      fontWeight: 'normal',
      color: colors('searchInput.text'),
    };
  }

  colorOutline() {
    const { theme } = this.context;
    const colors = getColors(theme);
    const style = styles(theme);
    if (this.props.error) return { borderColor: colors('error.100') };
    if (this.state.focused) return { ...style.borderInput.selected };
    return { ...style.borderInput.default };
  }

  widthOutline() {
    const { theme } = this.context;
    const viewStyle = styles(theme);
    if (this.props.error) return viewStyle.view.widthOutline;
    if (this.state.focused) return viewStyle.view.widthOutline;
    return viewStyle.view.widthOutline;
  }
}

SearchInput.contextType = VulpesContext;
