import React, { Component, useState } from 'react';
import { ScrollView, TouchableOpacity, View } from 'react-native';
import { getColors } from '../colors';
import VulpesContext from '../contexts/VulpesContext';
import { getFonts } from '../fonts';
import { Icon } from './icon';
import { ModalPicker } from './modal_picker';
import { Text } from './text';
import { Regular, RegularBold, Small } from './typos';
import styles from '../styles/select_inputs';
import useVulpes from '../hooks/useVulpes';

const itemStyleContainer = {
  width: '100%',
  flex: 1,
  borderBottomColor: '#D9D9D9',
  borderBottomWidth: 0.25,
  alignItems: 'center',
  alignContent: 'center',
};

const itemStyle = {
  flex: 1,
  height: 44,
  lineHeight: 44,
  textAlign: 'center',
};

const itemTouchStyle = (isPressed, theme = 'gogood') => {
  const colors = getColors(theme);
  return {
    flex: 1,
    width: '100%',
    alignContent: 'center',
    backgroundColor: isPressed ? colors('selectInput.pressed') : {},
  };
};

export const SelectItem = (props) => {
  let lastStyle = props.last ? { borderBottomWidth: 0 } : {};
  let TextComponent =
    props.currentValue === props.value ? RegularBold : Regular;
  const [isPressed, setIsPressed] = useState(false);
  const { theme } = useVulpes();

  return (
    <View style={[itemStyleContainer, lastStyle]}>
      <TouchableOpacity
        onPressOut={() => {
          setIsPressed(false);
        }}
        onPress={() => {
          setIsPressed(true);
          return props.onSelect(props.value, props.label);
        }}
        activeOpacity={1}
        style={itemTouchStyle(isPressed, theme)}
      >
        <TextComponent style={itemStyle}>{props.label}</TextComponent>
      </TouchableOpacity>
    </View>
  );
};

export class SelectInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      placeholder: !props.value || props.value.length === 0,
      modalShow: false,
      selectedLabel: undefined,
      selectedValue: undefined,
      itens: this.loadItens(),
      focused: false,
    };
    const selected = this.loadSelected();
    if (selected) {
      this.state.selectedLabel = selected.label;
      this.state.selectedValue = selected.value;
    }

    this.field = null;
  }

  static getDerivedStateFromProps(props, state) {
    if (props.value !== state.selectedValue) {
      const selected = state.itens.find((i) => i.value === props.value);
      if (selected) {
        return {
          selectedLabel: selected.label,
          selectedValue: selected.value,
        };
      }
    }
    return null;
  }

  loadItens() {
    return React.Children.toArray(this.props.children).map((item) => {
      return item.props;
    });
  }

  loadSelected() {
    const mapping = this.loadItens();
    if (!mapping || mapping.length === 0) return null;

    return mapping.find((item) => item.value === this.props.value);
  }

  handleFocus() {
    this.setState({ focused: true });
    this.field && this.field.focus();
  }

  handleBlur() {
    this.setState({ focused: false });
    this.props.onBlur && this.props.handleBlur();
  }

  handleReference(field) {
    this.field = field;
  }

  viewBorders() {
    const { theme } = this.context;
    const style = styles(theme);
    return {
      ...this.colorOutline(),
      borderBottomWidth: style.view.borderBottomWidth,
    };
  }

  inputBorders() {
    const { theme } = this.context;
    const { button } = this.props;
    const style = styles(theme);
    const border = button ? style.buttonBorderInput : style.borderInput;

    if (!border) return null;
    return {
      ...border,
      ...this.colorOutline(),
    };
  }

  onSelect(value, label) {
    this.setState({
      selectedLabel: label,
      selectedValue: value,
      placeholder: !value || value.length === 0,
      modalShow: false,
    });

    this.props.onChangeValue && this.props.onChangeValue(value);
  }

  iconStyle() {
    return {
      marginTop: 13,
      position: 'absolute',
      right: 0,
    };
  }

  loadModalPicker() {
    this.setState({ modalShow: true });
  }

  closeModal() {
    this.setState({ modalShow: false });
  }

  iconErrorStyle() {
    const { theme } = this.context;
    const style = styles(theme);
    return {
      ...style.iconErrorStyle,
    };
  }

  showIcon() {
    const { theme } = this.context;
    const style = styles(theme);
    return style.showIcon;
  }

  drawPicker() {
    const { children } = this.props;
    const itemCount = (children && children.length) || 0;

    return (
      <ScrollView style={this.scrollViewContainer()}>
        {React.Children.map(children, (child, i) => {
          const last = i === itemCount - 1;
          return React.cloneElement(child, {
            onSelect: this.onSelect.bind(this),
            last: last,
            currentValue: this.state.selectedValue,
          });
        })}
      </ScrollView>
    );
  }

  errorStyle() {
    const { theme } = this.context;
    const style = styles(theme);
    return {
      paddingLeft: style.view.paddingLeft,
      marginTop: 10,
      flexDirection: 'row',
    };
  }

  selectStyle() {
    const { theme } = this.context;
    const { style, button } = this.props;

    const mainStyle = styles(theme);
    const viewBorder = this.viewBorders();

    if (button) {
      return {
        style,
        ...mainStyle.view,
        ...mainStyle.buttonView,
        ...this.colorOutline(),
        borderBottomWidth: 1,
      };
    }

    return {
      ...mainStyle.view,
      ...viewBorder,
      style,
    };
  }

  titleLabelStyle() {
    const { labelStyle, error } = this.props;
    const { theme } = this.context;
    const style = styles(theme);
    const label = this.state.selectedLabel;
    if (error) return { ...style.label.error };
    if (label) return { ...style.label.selected };
    return {
      ...labelStyle,
      color: style.label.default,
    };
  }

  selectedLabel() {
    const label = this.state.selectedLabel;
    if (!label) return this.props.placeholder;
    return label;
  }

  scrollViewContainer() {
    return { width: '100%', flexGrow: 0 };
  }

  textStyle() {
    const { theme } = this.context;
    const colors = getColors(theme);

    return {
      ...this.fontStyle(),
      backgroundColor: 'transparent',
      height: 37,
      lineHeight: 37,
      marginTop: 2,
      ...(this.props.button
        ? { color: colors('selectInput.button_text') }
        : {}),
    };
  }

  fontStyle() {
    const { theme } = this.context;
    const fonts = getFonts(theme);
    const style = styles(theme);
    let font = style.selectedItemIsBold ? fonts.regularBold : fonts.regular;
    if (this.state.placeholder && !this.props.value) font = fonts.placeholder;
    if (this.props.error) font = { ...font, ...style.placeholderError };
    return font;
  }

  colorOutline() {
    const { theme } = this.context;
    const colors = getColors(theme);
    const style = styles(theme);
    if (this.props.error) return style.borberBottomError;
    if (this.state.focused) {
      return { borderBottomColor: colors('main.regular') };
    }
    return style.borderOutline;
  }

  render() {
    const { error, label, testID } = this.props;

    return (
      <>
        <View testID={testID} style={this.selectStyle()}>
          {label && <Regular style={this.titleLabelStyle()}>{label}</Regular>}
          <TouchableOpacity
            style={this.inputBorders()}
            onPress={this.loadModalPicker.bind(this)}
            ref={this.handleReference.bind(this)}
            onFocus={this.handleFocus.bind(this)}
            onBlur={this.handleBlur.bind(this)}
          >
            <View>
              <Icon
                size={16}
                name={'chevron_down'}
                style={this.iconStyle()}
                color={
                  this.props.button ? 'selectInput.button_text' : 'dark_gray'
                }
              />
              <Text style={this.textStyle()}>{this.selectedLabel()}</Text>
            </View>
          </TouchableOpacity>
          <ModalPicker
            visible={this.state.modalShow}
            onClose={this.closeModal.bind(this)}
            headerText={'Selecione'}
          >
            {this.drawPicker()}
          </ModalPicker>
        </View>
        {error && (
          <View style={this.errorStyle()}>
            {this.showIcon() && (
              <Icon
                style={this.iconErrorStyle()}
                size={12}
                name={'info'}
                color={'error.100'}
              />
            )}
            <Small color={'error.100'}>{error}</Small>
          </View>
        )}
      </>
    );
  }
}

SelectInput.contextType = VulpesContext;
