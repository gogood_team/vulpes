import React from 'react';
import { Linking, View } from 'react-native';
import { Button, H4, Icon, Text } from 'react-native-vulpes';

const SocialMediaSmallButtonsList = ({ keys, media }) => {
  const socialMediaSmallButtonsListStyle = {
    justifyContent: 'center',
    width: 40,
    height: 40,
    borderRadius: 22,
    marginRight: 8,
  };
  return keys.map((key) => {
    return (
      <Button
        outline
        color={'gray.110'}
        style={socialMediaSmallButtonsListStyle}
        onPress={() => Linking.openURL(media[key])}
      >
        <Icon name={'social_' + key} />
      </Button>
    );
  });
};

const SocialMediaSmallButtons = ({ ...props }) => {
  const socialMediaSmallButtonsStyle = {
    flexDirection: 'row',
    marginTop: 16,
  };
  return (
    <View style={socialMediaSmallButtonsStyle}>
      <SocialMediaSmallButtonsList {...props} />
    </View>
  );
};

const SocialMediaLargeButtons = ({ keys, media }) => {
  const socialMediaLargeButtonsListStyle = {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 0,
    marginRight: 0,
    marginTop: 16,
  };
  const socialMediaLargeButtonsStyle = {
    justifyContent: 'center',
    width: '100%',
    height: 40,
    borderRadius: 22,
    marginBottom: 16,
  };
  return (
    <View style={socialMediaLargeButtonsListStyle}>
      {keys.map((key) => {
        return (
          <Button
            outline
            color={'gray.110'}
            style={socialMediaLargeButtonsStyle}
            onPress={() => Linking.openURL(media[key])}
          >
            <Icon name={'social_' + key} />
            <Text>{mapping[key]}</Text>
          </Button>
        );
      })}
    </View>
  );
};

const mapping = {
  facebook: 'Facebook',
  instagram: 'Instagram',
  whatsapp: 'Whatsapp',
  tiktok: 'TikTok',
  twitter: 'Twitter',
  website: 'Site',
};

const SocialMediaButtons = ({ media }) => {
  let keys = Object.keys(media);
  if (keys.length > 2) {
    return <SocialMediaSmallButtons keys={keys} media={media} />;
  } else {
    return <SocialMediaLargeButtons keys={keys} media={media} />;
  }
};

export const SocialMedia = ({ social_media }) => {
  if (Object.keys(social_media).length <= 0) return null;
  const socialMediaMarginBottom = 60;
  const titleMarginBottom = 8;
  return (
    <View style={{ marginBottom: socialMediaMarginBottom }}>
      <H4 style={{ marginBottom: titleMarginBottom }}>Redes Sociais</H4>
      <View>
        <SocialMediaButtons media={social_media} />
      </View>
    </View>
  );
};
