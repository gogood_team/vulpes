import React, { Component, useMemo, useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Button, Icon, Small } from 'react-native-vulpes';
import { getColors } from '../colors';
import useVulpes from '../hooks/useVulpes';

import {
  cellStyle,
  cellToggleContent,
  cellToggleStyle,
  cellWrapperStyle,
  containerUnset,
  headerStyle,
  listContainer,
  listItem,
  titleStyle,
} from '../styles/table';
import { filterUndefined } from '../utils/filterUndefined';
import { IDGenerator } from '../utils/IDGenerator';
import { ModalHelper } from './modal_helper';
import { Popover } from './popover';
import { H3, Regular, RegularBold } from './typos';

const RESPONSIVE_BREAKPOINT = 1200;

const Title = (props) => {
  const { title, helper } = props;
  if (!title) return null;
  return (
    <H3 style={titleStyle}>
      {title}
      <ModalHelper title={title} helper={helper} show={helper} />
    </H3>
  );
};

const cellByType = (cellData, props) => {
  const { type = null, value = null } = cellData;
  const { dictionary } = props;

  const fromDict = (key) => {
    if (dictionary && dictionary[key]) {
      return dictionary[key];
    }
    return key;
  };

  switch (type) {
    case 'composite':
      return (
        <CompositeCell
          title={fromDict(value.title)}
          description={fromDict(value.description)}
        />
      );
    case 'composite_popover':
      return (
        <CompositePopoverCell
          title={fromDict(value.title)}
          description={fromDict(value.description)}
          setActivePopover={props.setActivePopover}
          popoverStyle={value.popoverStyle}
        >
          {value.content}
        </CompositePopoverCell>
      );
    case 'component':
      return value;
    default:
      return <SingleCell {...props} text={fromDict(value)} />;
  }
};

const getRowStyle = (props, theme) => {
  const colors = getColors(theme);
  const isEven = (number) => number % 2 === 0;
  const baseColor = props.headerColor || 'table.header';
  const style = {
    ...listItem,
    ...props.style,
    ...(props.isChildRow
      ? { borderBottomColor: colors(baseColor) + '80', borderBottomWidth: 1 }
      : {}),
  };

  const rowParams = {
    baseRow: props.head,
    baseColor: props.headerColor || 'table.header',
    evenColor: props.evenColor || 'table.even',
    oddColor: props.oddColor || 'table.odd',
  };

  const responsiveRowParams = {
    baseRow: props.isChildRow,
    baseColor: props.childColor || 'table.child',
    evenColor: props.mobileEvenColor || 'table.mobileEven',
    oddColor: props.mobileOddColor || 'table.mobileOdd',
  };

  const params = props.responsive ? responsiveRowParams : rowParams;

  if (params.baseRow) {
    style.backgroundColor = colors(params.baseColor);
  } else if (!isEven(props.line)) {
    style.backgroundColor = colors(params.evenColor);
  } else {
    style.backgroundColor = colors(params.oddColor);
  }

  return style;
};

const cellDataArray = (data) => {
  return data
    .map((item) => {
      if (React.isValidElement(item)) return item;
      if (typeof item !== 'object') {
        return { type: 'text', value: item };
      }
      return Object.keys(item).map((key) => item[key]);
    })
    .flat();
};

const EmptyState = ({ emptyState, data }) => {
  if (!emptyState) return null;
  if (data && data.length > 0) return null;
  return emptyState;
};

const Pagination = ({ onChangePage, data, pageSize }) => {
  const [currentPage, setPage] = useState(0);

  if (!onChangePage) return null;
  if (!data || (data.length === 0 && currentPage === 0)) return null;

  const change = (v) => {
    const next = currentPage + v;
    setPage(next);
    onChangePage(next);
  };
  const up = () => change(1);
  const down = () => change(-1);

  const sm = {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  };
  const sb = { marginLeft: 5, marginRight: 5 };

  if (pageSize && currentPage === 0 && data.length < pageSize) return null;

  return (
    <View style={sm}>
      {currentPage > 0 && (
        <Button onPress={down} text={'Página anterior'} style={sb} />
      )}
      {<Button onPress={up} text={'Próxima página'} style={sb} />}
    </View>
  );
};

const TableWrapper = (props) => {
  const { containerStyle, title, helper, style, setWidth } = props;
  const onTableLayout = ({ nativeEvent }) => {
    const { width } = nativeEvent.layout;
    setWidth(width);
  };
  return (
    <View style={[containerStyle, containerUnset]}>
      <Title title={title} helper={helper} />
      <View
        onLayout={onTableLayout}
        style={{
          ...listContainer,
          ...style,
          ...containerUnset,
        }}
      >
        <EmptyState {...props} />
        {props.children}
      </View>
      <Pagination {...props} />
    </View>
  );
};

class Rows extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columnWidth: { ...props.columnWidth } || {},
    };
  }

  onCellLayout({ nativeEvent }, index, lastCellRender) {
    const { width } = nativeEvent.layout;
    const minCellWidth = 5;
    if (this.props.columnWidth !== undefined)
      if (this.props.columnWidth[index] !== undefined) return;

    const columnWidth = this.state.columnWidth;

    const cellWidth = Math.max(
      columnWidth[index] || 0,
      Math.round(width),
      minCellWidth
    );

    columnWidth[index] = cellWidth;

    if (!this.props.data || !this.props.data[0]) return;

    if (lastCellRender) {
      const columnWidthArray = Object.values(columnWidth);
      if (this.props.width) {
        for (let i = 0; i < columnWidthArray.length; i++) {
          const newColumnWidth = Math.round(
            this.props.width / columnWidthArray.length
          );
          columnWidth[i] = Math.round(newColumnWidth);
        }
      }
      this.setState({ columnWidth: columnWidth });
    }
  }

  listRows() {
    const { data, ...rest } = this.props;
    const redirectProps = {
      ...rest,
      columnWidth: this.state.columnWidth,
      onCellLayout: this.onCellLayout.bind(this),
    };

    if (!data) return null;
    const rowData = [data].flat();
    if (rowData.length === 0) return null;
    const _rows = rowData.map((d, i) => {
      return <Row key={'r' + i} data={d} line={i} {...redirectProps} />;
    });

    if (!this.props.responsive) {
      _rows.unshift(<Row key={'head-row'} {...redirectProps} head={true} />);
    }

    return _rows;
  }

  componentDidUpdate(prevProps) {
    if (this.props.responsive) return;
    if (this.props.width === prevProps.width) return;
    if (!this.props.data || !this.props.data[0]) return;
    const rowCellsQuantity = Object.keys(this.props.data[0]).length;
    for (let i = 0; i < rowCellsQuantity; i++) {
      this.onCellLayout({ nativeEvent: { layout: { width: 10 } } }, i, true);
    }
  }

  render() {
    const rows = this.listRows();
    if (!rows) return null;

    return rows.map((child, i) => {
      const last = i === rows.length - 1;
      if (!child) return child;
      return React.cloneElement(child, {
        last: last,
        key: 'c' + i,
        line: i,
      });
    });
  }
}

export class Table extends Component {
  constructor(props) {
    super(props);

    this.state = {
      responsive: false,
      width: null,
    };
    this.delta = 8;
  }

  setWidth(width) {
    let responsive = width < RESPONSIVE_BREAKPOINT;
    if (this.props.noResponsive) responsive = false;
    this.setState({
      responsive: responsive,
      width: width,
    });
  }

  render() {
    const { style, ...props } = this.props;

    return (
      <TableWrapper {...props} setWidth={this.setWidth.bind(this)}>
        {this.state.width ? (
          <Rows
            {...props}
            responsive={this.state.responsive}
            width={this.state.width}
          />
        ) : null}
      </TableWrapper>
    );
  }
}

const useIdGeneratorList = (data) => {
  return useMemo(
    () =>
      Array.from({ length: data.length }).map(() =>
        IDGenerator.getInstance().getId()
      ),
    [data.length]
  );
};

export const Row = (props) => {
  const { head, data, responsive } = props;
  const [toggle, setToggle] = useState(true);

  const rawData = filterUndefined([data].flat());
  const cellData = cellDataArray(rawData);

  const responsiveCellData = responsive
    ? filterUndefined([
        cellData.find((v) => v.responsiveParent),
        ...cellData.filter((v) => !v.responsiveParent),
      ])
    : cellData;

  const rows = toggle ? [responsiveCellData[0]] : responsiveCellData;
  const rowIds = useIdGeneratorList(rows);

  if (head) {
    const { header = null } = props;
    if (!header) return null;
    return (
      <RowContent
        {...props}
        head={true}
        data={cellDataArray(header)}
        columnWidth={props.columnWidth || {}}
        last={props.last}
        key={'base-2'}
      />
    );
  }

  if (props.responsive) {
    const canRender = (row) => {
      if (!row) return false;
      if (row.type === 'composite_popover' && !row.value.content) return false;
      if (row.type === 'component' && !row.value) return false;
      if (row.type === 'composite') {
        const values = Object.values(row.value);
        const cantRender = values.every((value) =>
          [undefined, null].includes(value)
        );

        if (cantRender) return false;
      }

      return true;
    };

    return rows.map((row, index) => {
      if (!canRender(row)) return;

      return (
        <RowContent
          {...props}
          canRenderToggle={index === 0 && cellData.length > 1}
          toggle={toggle}
          data={[row]}
          index={index}
          line={props.line}
          onPress={() => setToggle(!toggle)}
          key={rowIds[index]}
        />
      );
    });
  }

  return (
    <RowContent
      {...props}
      data={cellData}
      columnWidth={props.columnWidth || {}}
      last={props.last}
      key={'base'}
    />
  );
};

const ToggleIcon = ({ toggle }) => {
  return (
    <View style={{ ...{ padding: 8 } }}>
      <Icon name={toggle ? 'chevron_down' : 'chevron_up'} />
    </View>
  );
};

export const RowContent = (props) => {
  const {
    data,
    columnWidth,
    last,
    onCellLayout,
    toggle,
    canRenderToggle,
    onPress,
    responsive,
    index,
    line,
    head,
  } = props;

  const { theme } = useVulpes();
  const _style = getRowStyle(
    { ...props, isChildRow: responsive && !canRenderToggle, head: head },
    theme
  );
  const ContentWrapper = canRenderToggle ? TouchableOpacity : View;
  const cellIds = useIdGeneratorList(data);

  const mapRowData = () => {
    return data.map((cellData, i) => {
      const lastCellIndex = data.length - 1;
      const isLastCell = i === lastCellIndex;
      const width = columnWidth[i] || null;
      const widthStyle = width ? { width: width } : {};
      return (
        <View
          style={[responsive ? cellToggleContent : {}, containerUnset]}
          key={cellIds[i]}
        >
          <CellWrapper
            style={[cellStyle, responsive ? {} : widthStyle]}
            onLayout={(e) => onCellLayout(e, i, last && isLastCell)}
          >
            {React.isValidElement(cellData)
              ? cellData
              : cellByType(cellData, props)}
          </CellWrapper>
          {canRenderToggle ? <ToggleIcon toggle={toggle} /> : null}
        </View>
      );
    });
  };

  return (
    <ContentWrapper
      onPress={canRenderToggle ? onPress : null}
      style={[
        head ? headerStyle : {},
        _style,
        containerUnset,
        responsive ? cellToggleStyle : {},
        index === 0 && line === 0 ? headerStyle : {},
      ]}
    >
      {mapRowData()}
    </ContentWrapper>
  );
};

const CellWrapper = (props) => {
  const { style, onLayout, children } = props;
  return (
    <View style={style} onLayout={onLayout}>
      <View style={[cellWrapperStyle, containerUnset]}>{children}</View>
    </View>
  );
};

const SingleCell = (props) => {
  const { text, head } = props;

  if (head) {
    return <RegularBold color={'singleton.white'}>{text}</RegularBold>;
  }

  return <Small numberOfLines={1}>{text}</Small>;
};

const CompositeCell = ({ title = ' ', description = ' ' }) => {
  return (
    <View style={{ ...containerUnset }}>
      <RegularBold>{title}</RegularBold>
      <Regular>{description}</Regular>
    </View>
  );
};

const CompositePopoverCell = ({
  title = ' ',
  description = ' ',
  popoverStyle,
  children,
}) => {
  return (
    <View style={{ ...containerUnset }}>
      <RegularBold>{title}</RegularBold>
      <Popover
        color={'popover.regular'}
        label={description}
        style={popoverStyle}
      >
        {children}
      </Popover>
    </View>
  );
};
