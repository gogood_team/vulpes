import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { getColors } from '../colors';
import useVulpes from '../hooks/useVulpes';
import { Regular, RegularBold } from './typos';
import { Spinner } from './spinner';

const TabButton = ({ focus, title, onSelect }) => {
  const { theme } = useVulpes();
  const colors = getColors(theme);
  const TextComponent = focus ? RegularBold : Regular;
  const tabsStyle = {
    paddingBottom: 10,
    paddingTop: 10,
    marginRight: 32,
    borderBottomWidth: 4,
    borderBottomColor: focus ? colors('tab.underline') : colors('gray.40'),
    marginBottom: -1,
  };

  return (
    <TouchableOpacity onPress={onSelect} style={tabsStyle}>
      <TextComponent color={focus ? 'tab.activeText' : 'tab.inactiveText'}>
        {title}
      </TextComponent>
    </TouchableOpacity>
  );
};

const tabsContainerStyle = (theme) => {
  const colors = getColors(theme);
  return {
    marginRight: 5,
    borderBottomWidth: 1,
    borderBottomColor: colors('gray.40'),
    flexDirection: 'row',
  };
};

const ListOfTabs = ({ titles, selected, onChange }) => {
  const { theme } = useVulpes();
  return (
    <View style={tabsContainerStyle(theme)}>
      {titles.map((title, i) => {
        if (!title) return;
        return (
          <TabButton
            key={'tb' + i}
            title={title}
            focus={selected === i}
            onSelect={() => onChange(i)}
          />
        );
      })}
    </View>
  );
};

export class Tabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.current ? props.current : 0,
      loading: false,
    };
    this.cache = {};
    this.countClick = 0;
  }

  componentDidUpdate(prevProps) {
    const { children, visible } = this.props;

    if (children === prevProps.children && visible === prevProps.visible)
      return;

    const { selected } = this.state;

    if (selected !== undefined) {
      this.cache[selected] = null;
    }

    this.setState({});
  }

  handleChange(newIndex) {
    const cache = this.cache[newIndex];
    const now = new Date().getTime();
    let load = false;
    const tabCache = this.props.tabCache || Infinity;

    const { selected } = this.state;

    if (selected !== newIndex) this.countClick = 0;
    this.countClick += 1;

    if (this.countClick >= 3 || (cache && cache.timestamp + tabCache < now)) {
      cache.timestamp = now;
      load = true;
      this.countClick = 0;
    }

    this.setState({ selected: newIndex, loading: load }, () => {
      this.props.onChangeTab && this.props.onChangeTab(newIndex);
      if (load) this.setState({ loading: false });
    });
  }

  tabTitles(children) {
    try {
      if (!children) return [];
      return React.Children.toArray(children).map((child, i) => {
        if (!child) return null;
        let { title, visible } = child.props;
        if (!title || visible === false) return null;
        return title;
      });
    } catch (error) {
      return [];
    }
  }

  renderLoading() {
    const st = { padding: 32 };
    return <Spinner style={st} />;
  }

  render() {
    const { children, current, ...rest } = this.props;
    const { selected, loading } = this.state;

    let currentTab = current === undefined ? selected : current;

    if (!children || children.length === 0) return null;
    const titles = this.tabTitles(children);

    const cachedTab = this.cache[currentTab];
    if (!cachedTab) {
      this.cache[currentTab] = {
        tab: children[currentTab],
        timestamp: new Date().getTime(),
      };
    }

    return (
      <View {...rest}>
        <ListOfTabs
          titles={titles}
          selected={currentTab}
          onChange={this.handleChange.bind(this)}
        />
        {React.Children.map(children, (child, index) => {
          if (!child) return null;

          const isCurrentTab = index === currentTab;
          const displayStyle = isCurrentTab ? 'flex' : 'none';

          return (
            <View key={index} style={{ display: displayStyle }}>
              {isCurrentTab && loading
                ? this.renderLoading()
                : this.cache[index]?.tab || null}
            </View>
          );
        })}
      </View>
    );
  }
}

export const Tab = (props) => {
  return <View>{props.children}</View>;
};
