import React, { Component } from 'react';
import { Platform, TextInput as Input, View } from 'react-native';
import VulpesContext from '../contexts/VulpesContext';
import { getFonts } from '../fonts';
import { Regular, Small } from './typos';
import styles from '../styles/text_input';
import { Icon } from './icon';

export class TextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placeholder: !props.value || props.value.length === 0,
      focused: false,
      loading: true,
    };
    this.field = null;
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({ loading: false });
    }, 10);
  }

  focus() {
    this.field && this.field.focus();
  }

  handleReference(field) {
    this.field = field;
  }

  handleChange(text) {
    this.setState({
      placeholder: !text || text.length === 0,
    });
    this.props.onChangeText && this.props.onChangeText(text);
  }

  handleFocus() {
    this.setState({ focused: true });
    this.props.onFocus && this.props.onFocus();
  }

  handleBlur() {
    this.setState({ focused: false });
    this.props.onBlur && this.props.onBlur();
  }

  handlePointer() {
    if (Platform.OS !== 'ios') return {};
    if (this.props.editable === false) return { pointerEvents: 'none' };
    return {};
  }

  render() {
    const {
      error,
      style,
      label,
      placeholder,
      onChangeText,
      labelStyle,
      inputStyle,
      onPress,
      onBlur,
      onFocus,
      value,
      ...rest
    } = this.props;
    if (this.state.loading) return null;
    return (
      <View>
        <View style={this.viewStyle()} {...this.handlePointer()}>
          {label && this.titleLabel(label)}
          <Input
            allowFontScaling={false}
            placeholder={placeholder}
            placeholderTextColor={this.fontStyle().color}
            onFocus={this.handleFocus.bind(this)}
            onBlur={this.handleBlur.bind(this)}
            {...rest}
            ref={this.handleReference.bind(this)}
            style={this.completeStyle()}
            value={this.props.value}
            onChangeText={this.handleChange.bind(this)}
          />
        </View>
        {error && (
          <View style={this.errorStyle()}>
            {this.showIcon() && (
              <Icon
                style={this.iconErrorStyle()}
                size={12}
                name={'info'}
                color={'error.100'}
              />
            )}
            <Small color={'error.100'}>{error}</Small>
          </View>
        )}
      </View>
    );
  }

  errorStyle() {
    const { theme } = this.context;
    const style = styles(theme);
    return {
      paddingLeft: style.view.paddingLeft,
      marginTop: 10,
      flexDirection: 'row',
    };
  }
  iconErrorStyle() {
    const { theme } = this.context;
    const style = styles(theme);
    return {
      ...style.iconErrorStyle,
    };
  }
  showIcon() {
    const { theme } = this.context;
    const style = styles(theme);
    return style.showIcon;
  }
  systemOutline() {
    if (Platform.OS !== 'web') return {};
    return {
      outline: 'none',
    };
  }
  titleLabelStyle() {
    const { labelStyle, error } = this.props;
    const { theme } = this.context;
    const style = styles(theme);

    if (error && this.state.placeholder)
      return {
        ...style.label.default,
        ...labelStyle,
      };
    if (error) return { ...style.label.error };
    if (this.state.focused || !this.state.placeholder)
      return { ...style.label.selected };
    return {
      ...style.label.default,
      ...labelStyle,
    };
  }
  titleLabel(label) {
    const { theme } = this.context;
    const style = styles(theme);
    if (style.isLabelSmall) {
      return <Small style={this.titleLabelStyle()}>{label}</Small>;
    } else {
      return <Regular style={this.titleLabelStyle()}>{label}</Regular>;
    }
  }
  viewStyle() {
    const { style } = this.props;
    const { theme } = this.context;
    const viewStyle = styles(theme);
    return {
      ...style,
      ...viewStyle.view,
      ...this.colorOutline(),
    };
  }
  completeStyle() {
    const { inputStyle } = this.props;
    const { theme } = this.context;
    const style = styles(theme);
    return {
      ...this.fontStyle(),
      ...this.colorOutline(),
      ...style.placeholderStyle,
      marginTop: 2,
      ...this.systemOutline(),
      ...inputStyle,
    };
  }

  fontStyle() {
    const { theme } = this.context;
    const fonts = getFonts(theme);
    const style = styles(theme);
    let font = style.font.defaultWeight;
    const { editable, disabled } = this.props;

    if (this.props.error) return { ...font, ...style.font.error };
    if (this.state.placeholder && !this.props.value) font = fonts.placeholder;
    if (this.state.placeholder && this.props.value) font = style.font.weight;
    if (editable === false || disabled)
      font = { ...font, ...(style.font.disabled || {}) };
    return { ...font };
  }

  colorOutline() {
    const { theme } = this.context;
    const style = styles(theme);
    if (this.props.error) return style.borderBottom.error;
    if (this.state.focused) return style.borderBottom.selected;
    return style.borderBottom.default;
  }
}

TextInput.contextType = VulpesContext;
