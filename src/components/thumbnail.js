import React, { useState, useMemo, useCallback } from 'react';
import { Image, View } from 'react-native';
import PropTypes from 'prop-types';
import { getColors } from '../colors';
import useVulpes from '../hooks/useVulpes';

// Tamanhos disponíveis
const sizesStyle = (theme, size) => {
  const colors = getColors(theme);
  const dimensions = {
    smaller: 32,
    small: 48,
    medium: 64,
    large: 80,
  };
  const sizeValue = dimensions[size] || dimensions.medium;

  return {
    width: sizeValue,
    height: sizeValue,
    borderRadius: sizeValue / 2,
    borderWidth: 1,
    borderColor: colors('gray.40'),
    backgroundColor: colors('singleton.white'),
    padding: sizeValue / 2 - 11,
  };
};

// Estilo transparente
const transparentStyle = (source) => {
  if (source && source.length) return {};
  return {
    backgroundColor: 'transparent',
    borderWidth: 0,
  };
};

// Normaliza fontes
const normalizeSource = (source) => {
  if (typeof source === 'string') {
    return source
      .split(';')
      .map((uri) => ({ uri }))
      .filter(Boolean);
  }
  return [source].flat().filter(Boolean);
};

// Define estilo
const defineStyleFromParams = (theme, size, source, style, square) => {
  const baseStyle = sizesStyle(theme, size);
  if (square) {
    return {
      ...baseStyle,
      borderRadius: 10,
      width: baseStyle.width + 2,
      height: baseStyle.height + 2,
      borderWidth: 0,
    };
  }
  return { ...baseStyle, ...transparentStyle(source), ...style };
};

// Componente principal
export const Thumbnail = ({
  square,
  source,
  size,
  style,
  empty,
  reportCard,
  logoDistance,
  autoDistance,
  ...restProps
}) => {
  const { theme } = useVulpes();
  const listSource = useMemo(() => normalizeSource(source), [source]);
  const [rowWidth, setRowWidth] = useState(0);

  const completeStyle = useMemo(
    () => defineStyleFromParams(theme, size, listSource, style, square),
    [theme, size, listSource, style, square]
  );

  const thumbSize = completeStyle.width;
  const countThumbs = listSource.length;

  const deltaLogo = useMemo(() => {
    if (!rowWidth || !autoDistance) return logoDistance ?? -12;

    const spacing =
      (rowWidth - 32 - countThumbs * thumbSize) / (countThumbs - 1);
    return Math.max(Math.min(spacing, logoDistance ?? -12), -thumbSize / 2);
  }, [rowWidth, autoDistance, thumbSize, countThumbs, logoDistance]);

  const displayedThumbs = listSource.slice(0, countThumbs);

  const handleLayout = useCallback(
    (event) => {
      if (countThumbs > 1) {
        const { width } = event.nativeEvent.layout;
        setRowWidth(width);
      }
    },
    [countThumbs]
  );

  if (!listSource.length) return null;

  const vStyle = {
    flexDirection: 'row',
    alignItems: 'center',
    height: completeStyle.height,
  };
  return (
    <View style={vStyle} onLayout={handleLayout}>
      {displayedThumbs.map((s, i) => (
        <Image
          key={`thumb-${i}`}
          source={s}
          style={[completeStyle, i > 0 && { marginLeft: deltaLogo }]}
        />
      ))}
    </View>
  );
};

Thumbnail.propTypes = {
  square: PropTypes.bool,
  source: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
    PropTypes.object,
  ]),
  size: PropTypes.oneOf(['smaller', 'small', 'medium', 'large']),
  style: PropTypes.object,
  empty: PropTypes.bool,
  reportCard: PropTypes.bool,
  logoDistance: PropTypes.number,
  autoDistance: PropTypes.bool,
};

Thumbnail.defaultProps = {
  square: false,
  size: 'medium',
  logoDistance: -12,
  autoDistance: false,
};
