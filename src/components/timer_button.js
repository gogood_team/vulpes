import React, { Component } from 'react';
import { Button, Regular, getColors } from 'react-native-vulpes';
import moment from 'moment';

const SECOND_IN_MS = 1000;
const MINUTE_IN_MS = SECOND_IN_MS * 60;

export class TimerButton extends Component {
  constructor(props) {
    super(props);
    this.intervalId = null;
    this.state = {
      remainingTimeInMs: null,
    };
  }

  componentDidMount() {
    if (this.props.startActive) return;
    this.startTimer();
  }

  componentWillUnmount() {
    this.stopTimer();
  }

  tick() {
    const { onTimerEnd } = this.props;
    const newRemainingTime = this.calculateRemainingTime();
    this.setState({ remainingTimeInMs: newRemainingTime }, () => {
      if (newRemainingTime <= 0) {
        this.stopTimer();
        onTimerEnd && onTimerEnd();
      }
    });
  }

  startTimer() {
    this.stopTimer();
    const remainingTime = this.calculateRemainingTime();
    this.setState({ remainingTimeInMs: remainingTime }, () => {
      const { remainingTimeInMs } = this.state;
      if (!remainingTimeInMs) return null;
      this.intervalId = setInterval(this.tick.bind(this), SECOND_IN_MS);
    });
  }

  stopTimer() {
    if (!this.intervalId) return;
    clearInterval(this.intervalId);
    this.intervalId = null;
  }

  resetTimer() {
    this.stopTimer();
    this.startTimer();
  }

  style() {
    const { theme } = this.context;
    const colors = getColors(theme);

    return {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: colors('alert.20'),
      borderWidth: 1,
      borderColor: colors('alert.110'),
      paddingLeft: 16,
      paddingRight: 16,
      paddingTop: 4,
      paddingBottom: 4,
      borderRadius: 22,
      marginBottom: 16,
      marginTop: 16,
    };
  }

  formattedTime() {
    const { remainingTimeInMs } = this.state;
    if (remainingTimeInMs < SECOND_IN_MS) return '';
    const minutes = Math.floor(remainingTimeInMs / MINUTE_IN_MS)
      .toString()
      .padStart(2, '0');
    const seconds = Math.floor(
      (remainingTimeInMs % MINUTE_IN_MS) / SECOND_IN_MS
    )
      .toString()
      .padStart(2, '0');
    return `${minutes}:${seconds}`;
  }

  calculateRemainingTime() {
    try {
      const { token } = this.props;
      const serverNow = moment(token.now);

      const tokenExpires = moment(token.expires_at);
      const delta = moment().diff(serverNow, 'seconds');
      const currentDateTime = serverNow.add(delta, 'seconds');
      const remainingTimeInMs = tokenExpires.diff(
        currentDateTime,
        'milliseconds'
      );
      return remainingTimeInMs > 0 ? remainingTimeInMs : null;
    } catch (e) {
      return null;
    }
  }

  onPress() {
    const { onPress = null } = this.props;
    onPress && onPress();
    return this.resetTimer();
  }

  textInfo() {
    const { text, textEnabled } = this.props;
    const { remainingTimeInMs } = this.state;
    if (remainingTimeInMs <= 0) return textEnabled || text;
    return text;
  }

  render() {
    const formattedTime = this.formattedTime();
    const { remainingTimeInMs } = this.state;
    return (
      <Button
        disabled={remainingTimeInMs > 0}
        style={this.props.style}
        onPress={this.onPress.bind(this)}
        ghost={this.props.ghost}
        outline={this.props.outline}
      >
        <Regular>
          {this.textInfo()} {formattedTime}
        </Regular>
      </Button>
    );
  }
}
