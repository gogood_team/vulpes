import React from 'react';
import { Animated, TouchableOpacity } from 'react-native';
import { getColors } from '../colors';
import VulpesContext from '../contexts/VulpesContext';
import { Regular, RegularBold } from '../index';

const defaultConfig = {
  duration: 3000,
  onPress: () => null,
  type: 'default',
  title: null,
  text: null,
  position: 0,
};

class ToastRoot extends React.Component {
  constructor(props) {
    super(props);
    this.props.reference.current = this;

    this.state = {
      visible: false,
      fadeAnim: new Animated.Value(0),
      posAnim: new Animated.Value(0),
    };

    const { reference, ...rest } = props;

    this.initialConfig = {
      ...defaultConfig,
      ...rest,
    };
    this.config = {
      ...this.initialConfig,
    };
  }

  fadeIn = () => {
    Animated.parallel([
      Animated.timing(this.state.fadeAnim, {
        toValue: 1,
        duration: 200,
        useNativeDriver: true,
      }),
      Animated.timing(this.state.posAnim, {
        toValue: -16,
        duration: 200,
        useNativeDriver: true,
      }),
    ]).start(() => {
      this.fadeOut();
    });
  };

  fadeOut = () => {
    setTimeout(() => {
      Animated.parallel([
        Animated.timing(this.state.fadeAnim, {
          toValue: 0,
          duration: 200,
          useNativeDriver: true,
        }),
        Animated.timing(this.state.posAnim, {
          toValue: 0,
          duration: 200,
          useNativeDriver: true,
        }),
      ]).start(() => {
        this.setState({ visible: false });
      });
    }, this.config.duration);
  };

  show({ ...config }) {
    this.config = {
      ...this.initialConfig,
      ...config,
      duration: config.duration || this.initialConfig.duration,
    };

    this.setState({ visible: true }, () => this.fadeIn());
  }

  hide() {
    if (this.state.visible) {
      this.config.duration = 1;
      this.fadeOut();
    }
  }

  viewStyle = () => {
    const { fadeAnim, posAnim } = this.state;
    const { position } = this.config;
    const { theme } = this.context;
    const colors = getColors(theme);

    return {
      opacity: fadeAnim,
      position: 'absolute',
      backgroundColor: colors('gray.40'),
      left: 16,
      right: 16,
      bottom: position,
      transform: [{ translateY: posAnim }],
      zIndex: 1000,
      borderRadius: 10,
      borderTopWidth: 5,
      boxShadow: '0px 0px 4px 0px rgba(0, 0, 0, 0.25)',
      ...this.backgroundColorStyle(),
      borderTopColor: 'transparent',
    };
  };

  backgroundColorStyle = (grade = '100') => {
    const { theme } = this.context;
    const colors = getColors(theme);

    const typeToColorFamily = {
      default: 'gray',
      success: 'success',
      error: 'error',
      warning: 'alert',
      alert: 'alert',
    };

    return {
      backgroundColor: colors(
        `${typeToColorFamily[this.config.type ?? 'default']}.${grade}`
      ),
    };
  };

  touchStyle = () => {
    return {
      flex: 1,
      padding: 10,
      justifyContent: 'center',
      borderRadius: 10,
      ...this.backgroundColorStyle('20'),
    };
  };

  titleStyle = () => {
    return {
      marginBottom: 8,
    };
  };

  render() {
    const { visible } = this.state;
    const { title, text } = this.config;
    if (!visible) return null;

    return (
      <Animated.View style={this.viewStyle()}>
        <TouchableOpacity
          style={this.touchStyle()}
          onPress={() => this.config.onPress()}
        >
          {title ? (
            <RegularBold style={this.titleStyle()} color={'gray.100'}>
              {title}
            </RegularBold>
          ) : null}
          {text ? <Regular color={'gray.100'}>{text}</Regular> : null}
        </TouchableOpacity>
      </Animated.View>
    );
  }
}

ToastRoot.contextType = VulpesContext;

const toastRef = React.createRef();

export function Toast(props) {
  return <ToastRoot reference={toastRef} {...props} />;
}

Toast.show = (params) => {
  toastRef.current?.show(params);
};

Toast.hide = (params) => {
  toastRef.current?.hide(params);
};
