import React from 'react';
import { Component } from 'react';
import { View, Animated, TouchableOpacity } from 'react-native';
import { getColors } from '../colors/index';
import { getFonts } from '../fonts';
import { Regular } from './typos';
import VulpesContext from '../contexts/VulpesContext';

const getStyles = (theme) => {
  const colors = getColors(theme);
  return {
    backgroundActive: colors('main.regular'),
    backgroundInactive: colors('gray.60'),

    containerStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      width: 50.29,
      height: 32,
      borderRadius: 50,
      padding: 4,
    },
    circleStyle: {
      width: 24,
      height: 24,
      borderRadius: 100,
      backgroundColor: colors('singleton.white'),
    },
    floatContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
    },
    container: {
      flexDirection: 'column',
    },
  };
};

export class Toggle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animationValue: new Animated.Value(this.props.active ? 1 : 0),
    };
  }

  async componentDidUpdate() {
    const { active } = this.props;
    const { animationValue } = this.state;
    const animationConfig = {
      fromValue: active ? 0 : 1,
      toValue: active ? 1 : 0,
      duration: 100,
      useNativeDriver: true,
    };

    return Animated.timing(animationValue, animationConfig).start();
  }

  fontStyle() {
    const { theme } = this.context;
    const fonts = getFonts(theme);
    const colors = getColors(theme);
    let font = fonts.regular;
    if (this.props.error) font = { ...font, color: colors('error.100') };
    return font;
  }

  render() {
    const { theme } = this.context;
    const _style = getStyles(theme);
    const circlePosXEnd =
      _style.containerStyle.width -
      (_style.circleStyle.width + _style.containerStyle.padding * 2);

    const { active, onChange, floatRight, text } = this.props;

    return (
      <TouchableOpacity onPress={() => onChange && onChange(!active)}>
        <View style={floatRight ? _style.floatContainer : _style.container}>
          {text && <Regular style={this.fontStyle()}>{text}</Regular>}
          <Animated.View
            style={[
              _style.containerStyle,
              {
                backgroundColor: this.state.animationValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: [
                    _style.backgroundInactive,
                    _style.backgroundActive,
                  ],
                }),
              },
            ]}
          >
            <Animated.View
              style={[
                _style.circleStyle,
                {
                  transform: [
                    {
                      translateX: this.state.animationValue.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, circlePosXEnd],
                      }),
                    },
                  ],
                },
              ]}
            />
          </Animated.View>
        </View>
      </TouchableOpacity>
    );
  }
}
Toggle.defaultProps = {
  active: false,
  text: null,
  onChange: null,
  floatRight: false,
};
Toggle.contextType = VulpesContext;
