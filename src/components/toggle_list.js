import { View, FlatList } from 'react-native';
import React from 'react';
import { Toggle } from 'react-native-vulpes';

const containerStyle = {
  margin: 12,
};

export const ToggleList = ({ values, onChange, ...props }) => {
  const _handleChange = async (_values, _index, _onChange) => {
    const newState = _values.map((item, index) => {
      if (index === _index) item.active = !item.active;
      return item;
    });
    onChange(newState);
  };

  return (
    <View {...props}>
      <FlatList
        data={values}
        keyExtractor={(_, index) => `tg-${index}`}
        renderItem={({ item, index }) => (
          <View style={containerStyle}>
            <Toggle
              active={item.active}
              text={item.text}
              floatRight={true}
              onChange={() => _handleChange(values, index, onChange)}
            />
          </View>
        )}
      />
    </View>
  );
};
