import { createContext } from 'react';

const VulpesContext = createContext({
  theme: 'gogood',
  setTheme: () => {},
  activePopover: null,
  setActivePopover: () => {},
});

export default VulpesContext;
