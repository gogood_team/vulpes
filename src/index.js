import { NativeModules } from 'react-native';
import { Colors, colorList, getColors } from './colors';
import { Accordion, AccordionItem } from './components/accordion';
import { ActionSheet, openActionSheet } from './components/action_sheet';
import { Button, ToggleButton } from './components/button';
import { TimerButton } from './components/timer_button';
import {
  BannerCard,
  Card,
  IllustrationMiniCard,
  UploadCard,
  MiniProfileCard,
  ProfileCard,
  TicketCard,
  TicketCheckinCard,
  TicketProfileCard,
} from './components/card';
import { HeaderSeparator } from './components/header_separator';
import {
  PartnersCard,
  PartnersAgenda,
  PartnersChatCard,
  PartnersStatsMiniCard,
  PartnersMiniGymCard,
  PartnersVisitorInfoCard,
  PartnersMiniReportCard,
  PartnersMiniRecentVisitorsCard,
  PartnersQrCodeCard,
  PartnersFixedNotificationCard,
} from './components/partnersCards';
import { CardActions } from './utils/cardElements';
import { Carousel, ImagesCarousel } from './components/carousel';
import {
  BarChart,
  EmptyChart,
  PieChart,
  StackChart,
  PartnersLineChart,
  PartnersBarChart,
} from './components/charts';
import { CheckboxInput } from './components/checkbox_input';
import { Checkbox } from './components/checkbox';
import { CheckboxList } from './components/checkbox_list';
import { CheckboxInputList } from './components/checkbox_input_list';
import { CircularProgress } from './components/circular_progress';
import { Divider } from './components/divider';
import { Form } from './components/form';
import { GradientView } from './components/gradient_view';
import { Header } from './components/header';
import { Icon } from './components/icon';
import { LeftMenu, LeftMenuItem } from './components/left_menu';
import { List, ListItem } from './components/list';
import { Menu, MenuItem } from './components/menu';
import { Modal } from './components/modal';
import { ModalHelper } from './components/modal_helper';
import { ModalContainer } from './components/modal_container';
import { NotificationIcon } from './components/notification_icon';
import { NotificationMenu } from './components/notification_menu';
import {
  BackgroundPage,
  Content,
  ContentList,
  ContentView,
  ContentPage,
  Page,
} from './components/page_content';
import { PaginationBullets } from './components/pagination_bullets';
import { QRCodeShow } from './components/qr_code';
import { SearchInput } from './components/search_input';
import { SearchFilter } from './components/filter';
import { SearchHistoryItems } from './components/search_history_items';
import { SelectInput, SelectItem } from './components/select_input';
import { SnapCarousel } from './components/snap_carousel';
import { SocialMedia } from './components/social_media';
import { Spinner } from './components/spinner';
import { Row, Table } from './components/table';
import { Tab, Tabs } from './components/tabs';
import { Tag } from './components/tag';
import { Text } from './components/text';
import { TextInput } from './components/text_input';
import { Thumbnail } from './components/thumbnail';
import { Toast } from './components/toast';
import { Popover } from './components/popover';
import { Toggle } from './components/toggle';
import { ToggleList } from './components/toggle_list';
import { Slider } from './components/slider';
import {
  BodyLarge,
  BodyLargeBold,
  BodyText,
  H1,
  H2,
  H3,
  H4,
  Placeholder,
  PlaceholderBold,
  Regular,
  RegularBold,
  Small,
  Small2,
  SmallBold,
  Subtitle,
} from './components/typos';
import { FillSpace, PaddedView } from './components/utils';
import { Fonts } from './fonts';
import { PinCode } from './components/pincode';
import VulpesContext from './contexts/VulpesContext';
import VulpesProvider from './providers/VulpesProvider';
import useVulpes from './hooks/useVulpes';

export {
  PaginationBullets,
  Spinner,
  Menu,
  MenuItem,
  LeftMenu,
  LeftMenuItem,
  BannerCard,
  TimerButton,
  ActionSheet,
  openActionSheet,
  Colors,
  colorList,
  getColors,
  Text,
  Fonts,
  GradientView,
  FillSpace,
  H1,
  H2,
  H3,
  H4,
  Divider,
  PaddedView,
  BodyLarge,
  BodyText,
  BodyLargeBold,
  Placeholder,
  PlaceholderBold,
  Regular,
  RegularBold,
  Small,
  Small2,
  SmallBold,
  QRCodeShow,
  PartnersQrCodeCard,
  Icon,
  Button,
  Subtitle,
  TicketProfileCard,
  TicketCheckinCard,
  NotificationMenu,
  Tabs,
  Tab,
  TextInput,
  CheckboxInput,
  Checkbox,
  CheckboxList,
  CheckboxInputList,
  SelectInput,
  BackgroundPage,
  SearchInput,
  SearchFilter,
  SearchHistoryItems,
  SelectItem,
  Header,
  Page,
  ContentPage,
  Content,
  MiniProfileCard,
  PartnersMiniGymCard,
  PartnersAgenda,
  PartnersChatCard,
  PartnersVisitorInfoCard,
  PartnersMiniReportCard,
  PartnersMiniRecentVisitorsCard,
  PinCode,
  ContentList,
  ContentView,
  Card,
  CardActions,
  Thumbnail,
  NotificationIcon,
  TicketCard,
  BarChart,
  PieChart,
  StackChart,
  PartnersLineChart,
  PartnersBarChart,
  EmptyChart,
  ProfileCard,
  Carousel,
  SnapCarousel,
  ImagesCarousel,
  SocialMedia,
  Modal,
  ModalHelper,
  ModalContainer,
  IllustrationMiniCard,
  PartnersCard,
  PartnersStatsMiniCard,
  PartnersFixedNotificationCard,
  UploadCard,
  List,
  ListItem,
  Accordion,
  AccordionItem,
  Tag,
  Toast,
  Popover,
  Toggle,
  ToggleList,
  HeaderSeparator,
  Slider,
  ToggleButton,
  CircularProgress,
  Table,
  Row,
  VulpesContext,
  VulpesProvider,
  useVulpes,
  Form,
};

const { Vulpes } = NativeModules;

export default Vulpes;
