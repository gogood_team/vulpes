import React, { useState, useEffect } from 'react';
import VulpesContext from '../contexts/VulpesContext';

export default function VulpesProvider({ children, theme = 'gogood' }) {
  const [_theme, setTheme] = useState(null);
  const [activePopover, setActivePopover] = useState(null);
  useEffect(() => {
    setTheme(theme);
  }, [theme]);
  return (
    <VulpesContext.Provider
      value={{ theme: _theme, setTheme, activePopover, setActivePopover }}
    >
      {children}
    </VulpesContext.Provider>
  );
}
