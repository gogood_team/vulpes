import { getColors } from '../colors';

const radius = 10;
const borderTopHeight = 4;
const remainingBorderHeight = 1;
const cardPadding = 16;
const additionalTopBottomPadding = 4;
export default (theme) => {
  const colors = getColors(theme);
  const gray = colors('gray.40');
  const white = colors('singleton.white');
  return {
    outerCardBorder: {
      position: 'absolute',
      height: borderTopHeight,
      overflow: 'hidden',
      left: 0,
      right: 0,
    },
    checkinImageOuter: { marginTop: 11, marginBottom: 11 },
    checkinImage: { width: 40, height: 42 },
    cardTopBorder: {
      borderTopLeftRadius: radius,
      borderTopRightRadius: radius,
      backgroundColor: colors('primary.80'),
      height: radius,
    },
    cardSeparator: {
      marginTop: 15,
      marginBottom: 15,
      flexDirection: 'row',
      height: 24,
    },
    verticalSeparator: {
      height: '100%',
      width: 1,
      backgroundColor: colors('primary.100'),
    },
    _cardSeparator: {
      flex: 1,
      height: 1,
      marginTop: 12,
      borderWidth: 0,
      overflow: 'hidden',
      backgroundColor: gray,
    },
    cardSeparatorLeft: {
      borderRightColor: gray,
      borderRightWidth: 1,
      borderTopColor: gray,
      borderTopWidth: 1,
      borderBottomColor: gray,
      borderBottomWidth: 1,
      marginLeft: -17,
      height: 24, // change these values according to your requirement.
      width: 16,
      borderTopRightRadius: 12,
      borderBottomRightRadius: 12,
      backgroundColor: white,
    },
    cardSeparatorRight: {
      borderLeftColor: gray,
      borderLeftWidth: 1,
      borderTopColor: gray,
      borderTopWidth: 1,
      borderBottomColor: gray,
      borderBottomWidth: 1,
      marginRight: -17,
      height: 24, // change these values according to your requirement.
      width: 16,
      borderTopLeftRadius: 12,
      borderBottomLeftRadius: 12,
      backgroundColor: white,
    },
    cardContainerZeroPadding: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
    },
    cardContainer: {
      borderRadius: radius,
      borderColor: gray,
      borderWidth: remainingBorderHeight,
      paddingTop: additionalTopBottomPadding + cardPadding,
      paddingLeft: cardPadding,
      paddingRight: cardPadding,
      paddingBottom: additionalTopBottomPadding + cardPadding,
      backgroundColor: white,
      overflow: 'hidden',
    },
    illustrationOnCard: {
      width: 80,
      height: 91,
      margin: -1,
      borderTopLeftRadius: radius,
      borderBottomLeftRadius: radius,
    },
    // Upload card
    uploadCardContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
      height: 51,
      flexDirection: 'row',
      alignItems: 'center',
    },

    uploadCardIconContainer: {
      width: 80,
      height: 91,

      margin: -1,
      borderTopLeftRadius: radius,
      borderBottomLeftRadius: radius,
      backgroundColor: colors('uploadCard.background'),
    },

    uploadCardIcon: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      position: 'relative',
      left: theme !== 'dasa' ? 3 : 0,
    },

    uploadCardContentContainer: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexGrow: 1,
    },

    uploadCardLabelContainer: {
      flex: 1,
      marginLeft: 16,
    },

    // IllustrationCard
    illustrationCardContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
    },

    illustrationCardOuterStyle: {
      margin: 16,
      flex: 1,
    },

    //Partners
    PartnersStatsCardContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
      minHeight: 141,
      minWidth: 260,
    },
    PartnersCardContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
    },
    PartnersVisitorInfoCardContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
    },
    PartnersChatCardContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
    },

    PartnersMiniReportCardContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
    },
    PartnersMiniRecentVisitorsContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
    },
    PartnersMiniGymCardContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
      minWidth: 250,
    },
    PartnersFixedNotificationCardContainer: {
      paddingTop: 0,
      paddingBottom: 0,
      paddingLeft: 0,
      paddingRight: 0,
      minWidth: 250,
    },

    PartnersCardOuterStyle: {
      margin: 23,
      flex: 1,
    },

    PartnersStatsLeftSide: {
      justifyContent: 'space-evenly',
      width: '33%',
      height: 141,
      margin: -1,
      paddingLeft: 16,
      borderTopLeftRadius: 10,
      borderBottomLeftRadius: 10,
    },

    PartnersStatsRightSide: {
      justifyContent: 'space-evenly',
      marginLeft: 16,
      flex: 1,
      backgroundColor: colors('singleton.white'),
    },
    PartnersTagsView: { flexDirection: 'row', marginBottom: 8 },

    PartnersSecondTagView: { marginLeft: 8 },

    PartnersOuterMiniReportStyle: { flexDirection: 'row' },
    PartnersOuterMiniRecentVisitorStyle: {
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    PartnersMiniReportTextStyle: {
      flex: 1,
    },
    PartnersMiniReportContentStyle: {
      flex: 1,
      paddingLeft: 8,
      paddingTop: 4,
      flexDirection: 'row',
      width: '85%',
      justifyContent: 'space-between',
    },
    PartnersAgendaContentStyle: {
      flex: 1,
      justifyContent: 'center',
      paddingLeft: 4,
      paddingTop: 4,
      flexDirection: 'row',
    },
    PartnersAgendaFirstCardStyle: {
      borderWidth: 1,
      height: 72,
      width: 83,
      padding: 16,
      borderRadius: 12,
      borderColor: colors('primary.100'),
    },

    PartnersMiniReportTagStyle: {
      flexDirection: 'row',
      marginTop: 4,
    },
    PartnersChatTextStyle: {
      flexDirection: 'row',
      marginTop: 8,
      justifyContent: 'space-between',
    },
    PartnersMiniGymCardContentStyle: {
      flex: 1,
      paddingLeft: 8,
    },
    PartnersMiniGymCardTag: { paddingTop: 4 },
    PartnersMiniReportCard: {
      flexDirection: 'column',
      alignItems: 'flex-end',
      marginLeft: 8,
      marginTop: -4,
    },
    PartnersChatCardAlertCircle: {
      height: 10,
      width: 10,
      backgroundColor: colors('alert.110'),
      borderRadius: 100,
      alignSelf: 'center',
    },
    arrowIconStyle: { marginLeft: 3, marginTop: 2 },

    outerMiniCardStyle: { flexDirection: 'row', flexWrap: 'wrap' },

    percentageStyle: { flexDirection: 'row' },

    miniCardContainer: {
      paddingTop: cardPadding,
      paddingBottom: cardPadding,
    },

    profileCardDivider: {
      borderBottomWidth: 1,
      borderBottomColor: gray,
    },
    profileCardDividerContainer: {
      marginLeft: -cardPadding,
      marginRight: -cardPadding,
      marginBottom: 32,
    },

    ticketProfileCardDividerContainer: {
      marginLeft: 0,
      marginRight: 0,
      marginBottom: 10,
    },

    profileCardDividerContent: {
      position: 'absolute',
      height: '100%',
      width: '100%',
      justifyContent: 'center',
    },

    profileCardImgContent: {
      paddingLeft: cardPadding,
      paddingRight: cardPadding,
      position: 'absolute',
      marginTop: -32,
    },

    cardActionsContainer: {
      flexDirection: 'row',
      alignSelf: 'flex-end',
      marginTop: 16,
      position: 'absolute',
      zIndex: 99,
      elevation: 99,
    },

    ticketProfileCardImgContent: {
      paddingLeft: cardPadding - 10,
      paddingRight: cardPadding - 10,
    },

    miniCardContentStyle: {
      flex: 1,
      paddingLeft: 8,
      paddingTop: 4,
    },
    bannerCardGradient: { height: 162, borderRadius: 10, overflow: 'hidden' },
    bannerCard: { flex: 1, borderRadius: 10 },
    outerViewBannerCard: { flexDirection: 'row', height: '100%' },
    outerViewQrCodeCard: { flexDirection: 'row', height: '100%' },
    textsViewBannerCard: {
      flex: 1,
      flexDirection: 'column',
      padding: 16,
      paddingRight: 90,
      marginRight: -6,
    },
    textsViewQrCodeCard: {
      flex: 1,
      flexDirection: 'column',
      padding: 24,
      paddingRight: 45,
    },

    cardCoverContainer: {
      flex: 1,
      flexDirection: 'row',
      height: 130,
      marginLeft: -17,
      marginRight: -17,
      marginTop: -21,
    },
    cardContainerCoverBackground: {
      flex: 1,
      height: 130,
      padding: 16,
    },
    cardContainerCoverImage: {
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
    },

    titleTextBannerCard: { marginBottom: 8 },
    buttonTextBannerCard: { paddingLeft: 0, marginTop: 12 },
    imageInBannerCard: { position: 'absolute', right: 0 },

    titleTextQrCodeCard: { marginBottom: 8, marginRight: 8 },
    imageInQrCodeCard: { position: 'absolute', right: 0, bottom: 0 },
  };
};
