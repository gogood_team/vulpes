import { getColors } from '../colors';

export default (theme) => {
  const colors = getColors(theme);
  const gray = colors('gray.40');
  const white = colors('singleton.white');
  return {
    generalMenuStyle: {
      flexDirection: 'row',
      borderTopWidth: 1,
      backgroundColor: white,
      borderTopColor: gray,
    },
    focusedMenuStyle: {
      flexDirection: 'row',
      flex: 1,
      paddingBottom: 14,
      paddingLeft: 10,
      paddingRight: 10,
      alignItems: 'flex-start',
      justifyContent: 'center',
      top: 1,
      borderColor: gray,
      marginBottom: -24,
    },

    generalMenuItemStyle: {
      flex: 1,
      padding: 14,
      paddingLeft: 10,
      paddingRight: 10,
      alignItems: 'center',
    },

    circularTopBorder: {
      width: 56,
      height: 56,
      borderRadius: 50,
      borderWidth: 1,
      borderColor: gray,
      backgroundColor: white,
      justifyContent: 'center',
      alignItems: 'center',
      top: -22,
    },

    adjustCircularBorder: {
      width: 56,
      height: 42,
      backgroundColor: white,
      top: 14,
      justifyContent: 'center',
      alignItems: 'center',
    },

    circularButton: {
      width: 48,
      height: 48,
      borderRadius: 50,
      backgroundColor: colors('primary.100'),
      justifyContent: 'center',
      top: -14,
    },
  };
};
