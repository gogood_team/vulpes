import { getColors } from '../colors';

const textInputStyle = (theme) => {
  const colors = getColors(theme);
  const styles = {
    gogood: {
      view: {
        borderBottomWidth: 1,
        borderRadius: 22,
        widthOutline: 1,
        paddingLeft: 16,
      },
      borderInput: {
        default: { borderBottomColor: colors('gray.110') },
        selected: null,
      },
      iconColor: 'primary.100',
    },
    partners: {
      view: {
        borderBottomWidth: 1,
        borderRadius: 22,
        widthOutline: 1,
        paddingLeft: 16,
      },
      borderInput: {
        default: { borderBottomColor: colors('gray.110') },
        selected: null,
      },
      iconPositionLeft: true,
    },
    sesi: {
      view: {
        borderBottomWidth: 1,
        borderRadius: 22,
        widthOutline: 1,
        paddingLeft: 16,
      },
      borderInput: {
        default: { borderColor: colors('gray.110') },
        selected: { borderColor: colors('gray.110') },
      },
      iconColor: 'purple.80',
    },
    dasa: {
      view: {
        backgroundColor: colors('gray.20'),
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        paddingLeft: 16,
        paddingTop: 8,
        paddingRight: 16,
        borderBottomWidth: 1,
        widthOutline: 0,
      },
      iconPositionStyle: {
        position: 'relative',
        bottom: -2,
        marginRight: 8,
        marginTop: 3,
      },
      borderInput: {
        default: { borderBottomColor: colors('gray.60') },
        selected: { borderBottomColor: colors('primary.60') },
      },
      iconColor: 'primary.60',
    },
  };
  return styles[theme || 'gogood'];
};

export default (theme) => {
  return {
    ...textInputStyle(theme),
  };
};
