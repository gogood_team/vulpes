import { getColors } from '../colors';

const selectInputStyle = (theme) => {
  const colors = getColors(theme);
  const styles = {
    gogood: {
      view: {
        backgroundColor: null,
        borderBottomWidth: 0,
        paddingLeft: 0,
      },
      buttonView: {
        backgroundColor: colors('selectInput.button_bg'),
        borderRadius: 50,
        borderWidth: 1,
        borderColor: colors('selectInput.button_outline'),
      },
      borderInput: {
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 1,
      },
      buttonBorderInput: {
        marginVertical: 4,
        marginHorizontal: 12,
        justifyContent: 'center',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 0,
      },
      label: {
        default: { color: null },
        selected: { color: null },
        error: { color: colors('gray.100') },
      },
      iconErrorStyle: {
        marginRight: 8,
        position: 'relative',
        bottom: -2,
      },
      selectedItemIsBold: true,
      showIcon: false,
      placeholderError: { color: colors('error.100') },
      borberBottomError: { borderBottomColor: colors('error.100') },
      borderOutline: { borderBottomColor: colors('gray.40') },
    },
    partners: {
      view: {
        backgroundColor: null,
        borderBottomWidth: 0,
        paddingLeft: 0,
      },
      buttonView: {
        backgroundColor: colors('alert.100'),
        borderRadius: 8,
        borderWidth: 1,
        borderColor: colors('main.regular'),
        padding: 8,
      },
      borderInput: {
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 1,
      },
      buttonBorderInput: {
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 0,
      },
      label: {
        default: { color: null },
        selected: { color: null },
        error: { color: colors('gray.100') },
      },
      iconErrorStyle: {
        marginRight: 8,
        position: 'relative',
        bottom: -2,
      },
      selectedItemIsBold: true,
      showIcon: false,
      placeholderError: { color: colors('error.100') },
      borberBottomError: { borderBottomColor: colors('error.100') },
      borderOutline: { borderBottomColor: colors('gray.40') },
    },
    sesi: {
      view: {
        backgroundColor: null,
        borderBottomWidth: 0,
        paddingLeft: 0,
      },
      buttonView: {
        backgroundColor: colors('selectInput.button_bg'),
        borderRadius: 50,
        borderWidth: 1,
        borderColor: colors('selectInput.button_outline'),
      },
      borderInput: {
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 1,
      },
      buttonBorderInput: {
        marginVertical: 4,
        marginHorizontal: 12,
        justifyContent: 'center',
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 0,
      },
      label: {
        default: { color: null },
        selected: { color: null },
        error: { color: colors('gray.100') },
      },
      iconErrorStyle: {
        marginRight: 8,
        position: 'relative',
        bottom: -2,
      },
      selectedItemIsBold: true,
      showIcon: false,
      placeholderError: { color: colors('error.100') },
      borberBottomError: { borderBottomColor: colors('error.100') },
      borderOutline: { borderBottomColor: colors('gray.40') },
    },
    dasa: {
      view: {
        backgroundColor: colors('gray.20'),
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        paddingLeft: 16,
        paddingTop: 8,
        paddingRight: 16,
        borderBottomWidth: 2,
        borderBottomColor: colors('gray.60'),
      },
      buttonView: {
        backgroundColor: colors('alert.100'),
        borderRadius: 8,
        borderWidth: 1,
        borderColor: colors('main.regular'),
        padding: 8,
      },
      borderInput: {
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 0,
      },
      buttonBorderInput: {
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 0,
      },
      label: {
        default: { color: colors('gray.100') },
        selected: { color: colors('primary.60') },
        error: { color: colors('error.100') },
      },
      iconErrorStyle: {
        marginRight: 8,
        position: 'relative',
        bottom: -2,
      },
      selectedItemIsBold: false,
      showIcon: true,
      placeholderError: { color: colors('gray.110') },
      borberBottomError: { borderBottomColor: colors('error.100') },
      borderOutline: { borderBottomColor: colors('gray.60') },
    },
  };
  return styles[theme || 'gogood'];
};

export default (theme) => {
  return {
    ...selectInputStyle(theme),
  };
};
