import { Colors } from '../colors';

export const listContainer = {
  marginTop: 8,
  marginBottom: 8,
  minWidth: '100%',
  flex: 1,
  borderRadius: 10,
};

export const listItem = {
  paddingTop: 4,
  paddingBottom: 4,
  paddingLeft: 0,
  paddingRight: 0,
  borderBottomColor: Colors.light_gray,
  borderBottomWidth: 0,
  flexDirection: 'row',
  alignItems: 'center',
  height: 72,
};

export const outerTableOverflowStyle = {};

export const headerStyle = {
  borderTopLeftRadius: 10,
  borderTopRightRadius: 10,
};

export const titleStyle = {
  marginTop: 16,
  marginBottom: 8,
};

export const cellStyle = {
  paddingLeft: 8,
  paddingRight: 8,
  minWidth: 2,
  zIndex: 'unset',
};

export const cellWrapperStyle = {
  minWidth: 2,
  zIndex: 'unset',
};

export const cellToggleStyle = {
  paddingLeft: 16,
  paddingRight: 16,
  justifyContent: 'space-between',
};

export const cellToggleContent = {
  width: '100%',
  flexDirection: 'row',
  justifyContent: 'space-between',
};

export const containerUnset = {
  zIndex: 'unset',
};
