import { getColors } from '../colors';
import { getFonts } from '../fonts';

const textInputStyle = (theme) => {
  const colors = getColors(theme);
  const fonts = getFonts(theme);
  const styles = {
    gogood: {
      view: {
        backgroundColor: null,
        borderBottomWidth: 0,
        paddingLeft: 0,
      },
      placeholderStyle: {
        borderBottomWidth: 1,
        paddingTop: 8,
        paddingBottom: 8,
      },
      iconErrorStyle: {
        marginRight: 8,
        position: 'relative',
        bottom: -2,
      },
      label: {
        default: { color: colors('gray.100') },
        selected: { color: null },
        error: { color: colors('gray.100') },
      },
      borderBottom: {
        default: { borderBottomColor: colors('gray.40') },
        selected: { borderBottomColor: colors('textInput.outline') },
        error: { borderBottomColor: colors('error.100') },
      },
      font: {
        error: { color: colors('error.100') },
        default: { color: colors('gray.100') },
        weight: fonts.placeholderBold,
        defaultWeight: fonts.regularBold,
        disabled: { color: colors('gray.40') },
      },
      borderOutline: { borderBottomColor: colors('gray.40') },
      showIcon: false,
      isLabelSmall: false,
    },
    partners: {
      view: {
        backgroundColor: null,
        borderBottomWidth: 0,
        paddingLeft: 0,
      },
      placeholderStyle: {
        borderBottomWidth: 1,
        paddingTop: 8,
        paddingBottom: 8,
      },
      iconErrorStyle: {
        marginRight: 8,
        position: 'relative',
        bottom: -2,
      },
      label: {
        default: { color: colors('gray.100') },
        selected: { color: null },
        error: { color: colors('gray.100') },
      },
      borderBottom: {
        default: { borderBottomColor: colors('gray.40') },
        selected: { borderBottomColor: colors('textInput.outline') },
        error: { borderBottomColor: colors('error.100') },
      },
      font: {
        error: { color: colors('error.100') },
        default: { color: colors('gray.100') },
        weight: fonts.placeholderBold,
        defaultWeight: fonts.regularBold,
        disabled: { color: colors('gray.40') },
      },
      borderOutline: { borderBottomColor: colors('gray.40') },
      showIcon: false,
      isLabelSmall: false,
    },
    sesi: {
      view: {
        backgroundColor: null,
        borderBottomWidth: 0,
        paddingLeft: 0,
      },
      placeholderStyle: {
        borderBottomWidth: 1,
        paddingTop: 8,
        paddingBottom: 8,
      },
      iconErrorStyle: {
        marginRight: 8,
        position: 'relative',
        bottom: -2,
      },
      label: {
        default: { color: colors('gray.100') },
        selected: { color: null },
        error: { color: colors('gray.100') },
      },
      borderBottom: {
        default: { borderBottomColor: colors('gray.40') },
        selected: { borderBottomColor: colors('textInput.outline') },
        error: { borderBottomColor: colors('error.100') },
      },
      font: {
        error: { color: colors('error.100') },
        default: { color: colors('gray.100') },
        weight: fonts.placeholderBold,
        defaultWeight: fonts.regularBold,
        disabled: { color: colors('gray.40') },
      },
      borderOutline: { borderBottomColor: colors('gray.40') },
      showIcon: false,
      isLabelSmall: false,
    },
    dasa: {
      view: {
        backgroundColor: colors('gray.20'),
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        paddingLeft: 16,
        paddingTop: 8,
        paddingRight: 16,
        borderBottomWidth: 2,
        borderBottomColor: colors('gray.60'),
      },
      placeholderStyle: {
        marginTop: 3,
        marginBottom: 5,
        borderBottomWidth: 0,
      },
      iconErrorStyle: {
        marginRight: 8,
        position: 'relative',
        bottom: -2,
      },
      label: {
        default: { color: colors('gray.100') },
        selected: { color: colors('primary.60') },
        error: { color: colors('error.100') },
      },
      borderBottom: {
        default: { borderBottomColor: colors('gray.60') },
        selected: { borderBottomColor: colors('textInput.outline') },
        error: { borderBottomColor: colors('gray.60') },
      },
      font: {
        error: { color: colors('gray.110') },
        default: { color: colors('gray.110') },
        weight: fonts.placeholder,
        defaultWeight: fonts.regular,
        disabled: { color: colors('gray.40') },
      },
      borderOutline: { borderBottomColor: colors('gray.60') },
      showIcon: true,
      isLabelSmall: true,
    },
  };
  return styles[theme || 'gogood'];
};

export default (theme) => {
  return {
    ...textInputStyle(theme),
  };
};
