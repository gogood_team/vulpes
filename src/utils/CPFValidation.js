const invalidCPF = [
  '00000000000',
  '11111111111',
  '22222222222',
  '33333333333',
  '44444444444',
  '55555555555',
  '66666666666',
  '77777777777',
  '88888888888',
  '99999999999',
];

export class CPFValidation {
  constructor(cpf) {
    this.cpf = cpf;
  }

  clean() {
    if (!this.cpf) {
      return false;
    }
    this.cpf = this.cpf.replace(/\./g, '').replace('-', '');
    return true;
  }

  validSize() {
    if (this.cpf.length !== 11) {
      return false;
    }
    if (invalidCPF.includes(this.cpf)) {
      return false;
    }
    return true;
  }

  validFirstSum() {
    let sum = 0;

    for (let i = 1; i <= 9; i++) {
      sum = sum + parseInt(this.cpf.substring(i - 1, i), 10) * (11 - i);
    }
    let rest = (sum * 10) % 11;
    if (rest === 10 || rest === 11) {
      rest = 0;
    }
    if (rest !== parseInt(this.cpf.substring(9, 10), 10)) {
      return false;
    }
    return true;
  }

  validSecondSum() {
    let sum = 0;
    for (let i = 1; i <= 10; i++) {
      sum = sum + parseInt(this.cpf.substring(i - 1, i), 10) * (12 - i);
    }
    let rest = (sum * 10) % 11;
    if (rest === 10 || rest === 11) {
      rest = 0;
    }
    if (rest !== parseInt(this.cpf.substring(10, 11), 10)) {
      return false;
    }
    return true;
  }

  valid() {
    if (!this.clean()) {
      return false;
    }
    if (!this.validSize()) {
      return false;
    }
    if (!this.validFirstSum()) {
      return false;
    }
    if (!this.validSecondSum()) {
      return false;
    }
    return true;
  }
}

export function isValidCPF(cpf) {
  const validation = new CPFValidation(cpf);
  return validation.valid();
}
