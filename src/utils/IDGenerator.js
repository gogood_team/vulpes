export class IDGenerator {
  constructor() {
    if (!IDGenerator._instance) {
      IDGenerator._instance = this;
    }
    this.current_id = 0;
    return IDGenerator._instance;
  }

  static getInstance() {
    return this._instance;
  }

  getId() {
    const id = 'rc_' + this.current_id;
    this.current_id++;
    return id;
  }
}

const _idGenerator = new IDGenerator();
_idGenerator;
