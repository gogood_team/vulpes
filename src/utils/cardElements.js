import React from 'react';
import { Image, ImageBackground, View } from 'react-native';
import { Icon } from '../components/icon';
import { Tag } from '../components/tag';
import { Text } from '../components/text';
import { Thumbnail } from '../components/thumbnail';
import { Regular } from '../components/typos';
import { FillSpace } from '../components/utils';
import useVulpes from '../hooks/useVulpes';
import getStyle from '../styles/card';

const uncheckedImage = require('../../assets/images/unchecked.png');
const checkedImage = require('../../assets/images/checked.png');
export const qrCodeCardPartner = require('../../assets/images/qr_code_partners.png');

const CardSeparator = () => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  return <View style={style._cardSeparator} />;
};

export const VerticalCardSeparator = () => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  const horizontalPadding = { paddingHorizontal: 10 };
  return (
    <View style={horizontalPadding}>
      <View style={style.verticalSeparator} />
    </View>
  );
};

const CheckinImage = (props) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  return (
    <View style={style.checkinImageOuter}>
      {props.checked ? (
        <Image source={checkedImage} style={style.checkinImage} />
      ) : (
        <Image source={uncheckedImage} style={style.checkinImage} />
      )}
    </View>
  );
};

export const TicketCardSeparator = () => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  return (
    <View style={style.cardSeparator}>
      <View style={style.cardSeparatorLeft} />
      <CardSeparator />
      <View style={style.cardSeparatorRight} />
    </View>
  );
};

export const TicketCheckinCardSeparator = (props) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);

  return (
    <View style={style.ticketProfileCardDividerContainer}>
      <View style={style.profileCardDividerContent}>
        <View style={style.cardSeparator}>
          <View style={style.cardSeparatorLeft} />
          <CardSeparator />
          <View style={style.cardSeparatorRight} />
        </View>
      </View>
      <View style={style.ticketProfileCardImgContent}>
        <CheckinImage checked={props.checked} />
      </View>
    </View>
  );
};

export const TicketProfileCardSeparator = (props) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  const cStyle = {
    marginLeft: 0,
    marginRight: 0,
    minHeight: 64,
    marginBottom: 10,
  };
  return (
    <View style={cStyle}>
      <View style={style.profileCardDividerContent}>
        <View style={style.cardSeparator}>
          <View style={style.cardSeparatorLeft} />
          <CardSeparator />
          <View style={style.cardSeparatorRight} />
        </View>
      </View>
      <View style={style.ticketProfileCardImgContent}>
        <Thumbnail source={props.source} autoDistance={true} />
      </View>
    </View>
  );
};

export const ProfileCardSeparator = (props) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  if (!props.source) {
    const sMargin = { marginBottom: 16 };
    return (
      <View style={[style.profileCardDividerContainer, sMargin]}>
        <View style={style.profileCardDivider} />
      </View>
    );
  }

  return (
    <View style={style.profileCardDividerContainer}>
      <View style={style.profileCardDivider} />
      <View style={style.profileCardImgContent}>
        <Thumbnail size="medium" source={props.source} />
      </View>
    </View>
  );
};

export const CardTag = ({ icon, color, text, textColor, marginBottom }) => {
  const mgBottom = marginBottom ? { marginBottom: 8 } : {};
  return (
    <Tag
      textColor={textColor || 'singleton.white'}
      color={color || 'gray.100'}
      style={mgBottom}
    >
      {icon && <Icon name={icon} size={12} />}
      <Text>{text}</Text>
    </Tag>
  );
};

export const CardCover = ({
  tagText,
  tagIcon,
  tagColor,
  tagTextColor,
  source,
}) => {
  const { theme } = useVulpes();
  if (!source) {
    const dummyStyle = { height: 32 };
    return <View style={dummyStyle} />;
  }
  const style = getStyle(theme);
  return (
    <View style={style.cardCoverContainer}>
      <ImageBackground
        source={source}
        style={style.cardContainerCoverBackground}
        imageStyle={style.cardContainerCoverImage}
      >
        {(tagText || tagIcon) && (
          <CardTag
            textColor={tagTextColor}
            color={tagColor}
            text={tagText}
            icon={tagIcon}
          />
        )}
      </ImageBackground>
    </View>
  );
};

export const IllustrationOnCard = (props) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  return <Image source={props.source} style={style.illustrationOnCard} />;
};

export const CardActions = ({ children, style: pStyle }) => {
  const { theme } = useVulpes();
  const style = getStyle(theme);
  return (
    <View style={[style.cardActionsContainer, pStyle]}>
      <FillSpace />
      <View>{children}</View>
    </View>
  );
};
export const Visitor = ({ source, visitorName }) => {
  const visitorView = {
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
  };
  const visitorNameStyle = { width: '100%', textAlign: 'center' };
  const thumbnailStyle = { marginBottom: 8 };
  return (
    <View style={visitorView}>
      <Thumbnail style={thumbnailStyle} source={source} size={'small'} />
      <Regular numberOfLines={1} style={visitorNameStyle}>
        {visitorName}
      </Regular>
    </View>
  );
};
