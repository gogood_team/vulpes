export const getSafe = function (obj, defaultValue = undefined) {
  try {
    const value = obj();
    if (value === false) return false;
    if (value === null) return null;
    if (value === 0) return 0;
    if (!value) return defaultValue;
    return value;
  } catch (error) {
    return defaultValue;
  }
};
