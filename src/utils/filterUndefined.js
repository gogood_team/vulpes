export const filterUndefined = (array = []) => {
  if (!array || array.length === 0) return [];
  return array.filter((value) => ![undefined, null].includes(value));
};
